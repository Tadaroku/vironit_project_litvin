--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

-- Started on 2019-07-09 19:02:09

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 18063)
-- Name: airline_to_airport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.airline_to_airport (
    airport_id integer NOT NULL,
    airline_id integer NOT NULL
);


ALTER TABLE public.airline_to_airport OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 18068)
-- Name: airlines; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.airlines (
    id integer NOT NULL,
    airline_name character varying(255)
);


ALTER TABLE public.airlines OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 18073)
-- Name: airports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.airports (
    id integer NOT NULL,
    code character varying(3) NOT NULL,
    location character varying(255) NOT NULL,
    airport_name character varying(255) NOT NULL
);


ALTER TABLE public.airports OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 18081)
-- Name: flights; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.flights (
    id integer NOT NULL,
    plane_name character varying(255),
    arrival_time timestamp without time zone,
    departure_time timestamp without time zone,
    flight_number character varying(255),
    airport_from_id integer,
    airport_to_id integer
);


ALTER TABLE public.flights OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 18109)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 18089)
-- Name: reservations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservations (
    id integer NOT NULL,
    luggage boolean,
    price double precision NOT NULL,
    reservation_date date NOT NULL,
    status character varying(255) NOT NULL,
    seat character varying(255) NOT NULL,
    airline_id integer,
    flight_id integer,
    user_id bigint
);


ALTER TABLE public.reservations OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 18097)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying(255) NOT NULL,
    name character varying(20) NOT NULL,
    password character varying(255) NOT NULL,
    phone character varying(20),
    role character varying(255) NOT NULL,
    surname character varying(20) NOT NULL,
    username character varying(20)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 2710 (class 2606 OID 18067)
-- Name: airline_to_airport airline_to_airport_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.airline_to_airport
    ADD CONSTRAINT airline_to_airport_pkey PRIMARY KEY (airport_id, airline_id);


--
-- TOC entry 2712 (class 2606 OID 18072)
-- Name: airlines airlines_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.airlines
    ADD CONSTRAINT airlines_pkey PRIMARY KEY (id);


--
-- TOC entry 2714 (class 2606 OID 18080)
-- Name: airports airports_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.airports
    ADD CONSTRAINT airports_pkey PRIMARY KEY (id);


--
-- TOC entry 2716 (class 2606 OID 18088)
-- Name: flights flights_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flights
    ADD CONSTRAINT flights_pkey PRIMARY KEY (id);


--
-- TOC entry 2718 (class 2606 OID 18096)
-- Name: reservations reservations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);


--
-- TOC entry 2720 (class 2606 OID 18106)
-- Name: users uk_6dotkott2kjsp8vw4d0m25fb7; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email);


--
-- TOC entry 2722 (class 2606 OID 18108)
-- Name: users uk_r43af9ap4edm43mmtq01oddj6; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_r43af9ap4edm43mmtq01oddj6 UNIQUE (username);


--
-- TOC entry 2724 (class 2606 OID 18104)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2726 (class 2606 OID 18116)
-- Name: airline_to_airport fka1pkoqyqvkp06o3bbg2l9drbl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.airline_to_airport
    ADD CONSTRAINT fka1pkoqyqvkp06o3bbg2l9drbl FOREIGN KEY (airport_id) REFERENCES public.airports(id);


--
-- TOC entry 2731 (class 2606 OID 18141)
-- Name: reservations fkb5g9io5h54iwl2inkno50ppln; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT fkb5g9io5h54iwl2inkno50ppln FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 2730 (class 2606 OID 18136)
-- Name: reservations fkix9mwp337byu4ve2jqtjurjy6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT fkix9mwp337byu4ve2jqtjurjy6 FOREIGN KEY (flight_id) REFERENCES public.flights(id);


--
-- TOC entry 2729 (class 2606 OID 18131)
-- Name: reservations fkjisn7vmlp4c140qg6616rmtrm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT fkjisn7vmlp4c140qg6616rmtrm FOREIGN KEY (airline_id) REFERENCES public.airlines(id);


--
-- TOC entry 2727 (class 2606 OID 18121)
-- Name: flights fkllk6c56ht8ce7gdftenpgm0jg; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flights
    ADD CONSTRAINT fkllk6c56ht8ce7gdftenpgm0jg FOREIGN KEY (airport_from_id) REFERENCES public.airports(id);


--
-- TOC entry 2725 (class 2606 OID 18111)
-- Name: airline_to_airport fkmq5ei4k5bul5uou7art4bracm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.airline_to_airport
    ADD CONSTRAINT fkmq5ei4k5bul5uou7art4bracm FOREIGN KEY (airline_id) REFERENCES public.airlines(id);


--
-- TOC entry 2728 (class 2606 OID 18126)
-- Name: flights fkowas9bqerru39so0ace1slspr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flights
    ADD CONSTRAINT fkowas9bqerru39so0ace1slspr FOREIGN KEY (airport_to_id) REFERENCES public.airports(id);


-- Completed on 2019-07-09 19:02:09

--
-- PostgreSQL database dump complete
--

