package org.vironit.edu.backend.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.vironit.edu.backend.entity.Flight;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaRepositories(basePackageClasses = FlightRepository.class)
@EntityScan(basePackageClasses = Flight.class)
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource("/test.properties")
public class FlightRepositoryTest {

    @Autowired
    private FlightRepository flightRepository;

    @Test
    public void testSave() throws ParseException {
        Flight flight = new Flight();
        flight.setId(1);
        flight.setAirplane("Boeng");
        flight.setFlightNumber("123");
        String pattern = "yyyy-MM-dd HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date1 = simpleDateFormat.parse("2018-09-09 14:30");
        Date date2 = simpleDateFormat.parse("2018-09-09 16:30");
        flight.setDepartureTime(date1);
        flight.setArrivalTime(date2);
        flightRepository.save(flight);

    }

    @Test
    public void testFindAll(){
        List<Flight> findAll = flightRepository.findAll();
        Assertions.assertThat(findAll).isNotNull();
    }
}
