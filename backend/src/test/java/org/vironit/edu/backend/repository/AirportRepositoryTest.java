package org.vironit.edu.backend.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.vironit.edu.backend.entity.Airport;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaRepositories(basePackageClasses = AirportRepository.class)
@EntityScan(basePackageClasses = Airport.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource("/test.properties")
public class AirportRepositoryTest {

    @Autowired
    AirportRepository airportRepository;

    @Autowired
    private TestEntityManager entityManager;


    @Test
    public void testFindAll() {
        List<Airport> findAll = airportRepository.findAll();
        Assertions.assertThat(findAll).hasSize(0);
    }

    @Test
    public void testFindByName() {
        Airport airport = new Airport();
        airport.setLocation("Minsk");
        airport.setName("Airport");
        airport.setCode("AAA");
        airportRepository.save(airport);
        airport = entityManager.persistAndFlush(airport);
        assertThat(airportRepository.findByName(airport.getName()).orElse(null)).isEqualTo(airport);
    }
}
