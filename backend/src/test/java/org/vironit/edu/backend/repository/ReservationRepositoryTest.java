package org.vironit.edu.backend.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.vironit.edu.backend.entity.Reservation;
import org.vironit.edu.backend.entity.enums.ReservationStatus;
import org.vironit.edu.backend.entity.enums.TicketClass;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaRepositories(basePackageClasses = ReservationRepository.class)
@EntityScan(basePackageClasses = Reservation.class)
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource("/test.properties")
public class ReservationRepositoryTest {

    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    public void testSaveAndFindByPriceBetween(){
        Reservation reservation= new Reservation();
        reservation.setId(1);
        reservation.setLuggage(true);
        reservation.setTicketClass(TicketClass.BUSINESS);
        reservation.setSeatNumber(21);
        reservation.setPrice(123.00);
        reservation.setReservationStatus(ReservationStatus.FREE);
        reservation.setReservationDate(LocalDate.ofEpochDay(12-11-2019));
        reservationRepository.save(reservation);

        List<?> queryResult = reservationRepository.findByPriceBetween(100.00,130.00);
        assertFalse(queryResult.isEmpty());
        assertNotNull(queryResult.get(0));

    }

    @Test
    public void testFindAll(){
        List<Reservation> findAll = reservationRepository.findAll();
        Assertions.assertThat(findAll).isNotNull();
    }
}
