package org.vironit.edu.backend.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.vironit.edu.backend.entity.User;
import org.vironit.edu.backend.entity.enums.RoleName;


import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;


import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
@EntityScan(basePackageClasses = User.class)
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource("/test.properties")
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager entityManager;


    @Test
    public void testSaveAndFindByNameAndSurname(){
        User user = new User();
        user.setId((long) 1);
        user.setName("TestName");
        user.setSurname("TestSurname");
        user.setUsername("TestUsername");
        user.setEmail("Test@mail.ru");
        user.setPassword("TestPassword");
        user.setRole(RoleName.USER);
        userRepository.save(user);

        List<?> queryResult = userRepository.findByNameAndSurname("TestName","TestSurname");

        assertFalse(queryResult.isEmpty());
        assertNotNull(queryResult.get(0));
    }

    @Test
    public void testFindAll(){
        List<User> findAll = userRepository.findAll();
        Assertions.assertThat(findAll).isNotNull();
    }

    @Test
    public void testFindByEmail(){
        User user = new User();
        user.setUsername("TestUsername");
        user.setName("Name");
        user.setSurname("Surname");
        user.setPassword("Password");
        user.setEmail("Email@email.ru");
        user.setRole(RoleName.USER);
        userRepository.save(user);
        user =entityManager.persistAndFlush(user);
        assertThat(userRepository.findByEmail(user.getEmail()).orElse(null)).isEqualTo(user);}

    }




