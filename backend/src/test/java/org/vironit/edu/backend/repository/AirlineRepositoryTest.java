package org.vironit.edu.backend.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.vironit.edu.backend.entity.Airline;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaRepositories(basePackageClasses = AirlineRepository.class)
@EntityScan(basePackageClasses = Airline.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource("/test.properties")
public class AirlineRepositoryTest {

    @Autowired
    AirlineRepository airlineRepository;

    @Autowired
    private TestEntityManager entityManager;


    @Test
    public void testFindAll() {
        List<Airline> findAll = airlineRepository.findAll();
        Assertions.assertThat(findAll).hasSize(0);
    }

    @Test
    public void testFindByName() {
        Airline airline = new Airline();
        airline.setName("BELAVIA");
        airlineRepository.save(airline);
        airline = entityManager.persistAndFlush(airline);
        assertThat(airlineRepository.findByName(airline.getName()).orElse(null)).isEqualTo(airline);
    }
}
