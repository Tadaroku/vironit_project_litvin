package org.vironit.edu.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.backend.dto.AirportDto;
import org.vironit.edu.backend.dto.ConverterDto;
import org.vironit.edu.backend.entity.Airport;
import org.vironit.edu.backend.service.implementation.AirportServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/airports")
public class AirportController {

    private final AirportServiceImpl airportService;

    private final ConverterDto converterDto;


    @Autowired
    public AirportController(ConverterDto converterDto, AirportServiceImpl airportService) {
        this.converterDto = converterDto;
        this.airportService = airportService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AirportDto> getAirportById(@PathVariable Integer id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Optional<Airport> result = airportService.getAirportById(id);
        return result.map(airport -> new ResponseEntity<>(converterDto.fromAirport(airport), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteAirportById(@PathVariable Integer id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        airportService.deleteAirport(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createAirport(@RequestBody AirportDto airportDto) {

        if (airportDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            airportService.saveAirport(converterDto.toAirport(airportDto));
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateAirport(@PathVariable Integer id, @RequestBody AirportDto airportDto) {

        if (airportDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        airportService.saveAirport(converterDto.toAirport(airportDto));

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<AirportDto> getAirportByName(@PathVariable String name) {

        if (name == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Optional<Airport> result = airportService.findByName(name);


        return result.map(airport -> new ResponseEntity<>(converterDto.fromAirport(airport), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping
    public List<AirportDto> getAllAirports(
            @RequestParam(value = "page") int page) {
        List<Airport> airports = airportService.getAllAirports(page);
        List<AirportDto> airportDto = new ArrayList<>();
        for (Airport airport : airports) {
            airportDto.add(converterDto.fromAirport(airport));
        }


        return airportDto;
    }

}
