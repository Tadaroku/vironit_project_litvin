package org.vironit.edu.backend.dto;

public class AirlineDto {

    private Integer id;
    private String name;
    private String reservations;
    private String airports;

    public String getAirports() {
        return airports;
    }

    public void setAirports(String airport) {
        this.airports = airport;
    }

    public String getReservations() {
        return reservations;
    }

    public void setReservations(String reservations) {
        this.reservations = reservations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
