package org.vironit.edu.backend.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.vironit.edu.backend.entity.Reservation;
import org.vironit.edu.backend.repository.FlightRepository;
import org.vironit.edu.backend.repository.ReservationRepository;
import org.vironit.edu.backend.repository.UserRepository;
import org.vironit.edu.backend.service.interfaces.ReservationService;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    private final UserRepository userRepository;

    private final FlightRepository flightRepository;

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository, UserRepository userRepository, FlightRepository flightRepository) {
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.flightRepository = flightRepository;
    }

    @Override
    public Optional<Reservation> getReservationById(Integer id) {
        return reservationRepository.findById(id);
    }

    @Override
    public List<Reservation> getAllReservations(int page) {
        return reservationRepository.findAll(PageRequest.of(page, 3, Sort.by("price"))).getContent();
    }

    @Override
    public Reservation saveReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    @Override
    public void deleteReservation(Integer id) {
        reservationRepository.deleteById(id);
    }

//    @Override
//    public List<Ticket> findTicketsByFlightId(Flight flightId) {
//       return ticketRepository.findAllByFlightId(flightId);
//    }
//
//    @Override
//    public List<Ticket> findTicketsByUsername(User username) {
//        return ticketRepository.findTicketsByUsername(username);
//    }

    @Override
    public List<Reservation> findByPriceBetween(Double startPrice, Double finalPrice) {
        return reservationRepository.findByPriceBetween(startPrice, finalPrice);
    }


}
