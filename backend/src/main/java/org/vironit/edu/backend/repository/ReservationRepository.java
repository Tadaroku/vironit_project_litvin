package org.vironit.edu.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.vironit.edu.backend.entity.Reservation;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    List<Reservation> findByPriceBetween(Double startPrice, Double finalPrice);

//    List<Ticket> findAllByFlightId(Flight flightId);
//
//    List<Ticket> findTicketsByUsername(User username);

}
