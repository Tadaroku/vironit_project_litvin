package org.vironit.edu.backend.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.vironit.edu.backend.entity.Flight;
import org.vironit.edu.backend.repository.FlightRepository;
import org.vironit.edu.backend.service.interfaces.FlightService;

import java.util.List;
import java.util.Optional;

@Service
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;

    @Autowired
    public FlightServiceImpl(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    @Override
    public Optional<Flight> getFlightById(Integer id) {
        return flightRepository.findById(id);
    }

    @Override
    public Flight saveFlight(Flight flight) {
        return flightRepository.save(flight);
    }

    @Override
    public void deleteFlight(Integer id) {
        flightRepository.deleteById(id);
    }

    @Override
    public List<Flight> getAllFlights(int page) {
        return flightRepository.findAll(PageRequest.of(page, 3, Sort.by("departureTime"))).getContent();
    }


}
