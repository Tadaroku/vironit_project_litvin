package org.vironit.edu.backend.service.interfaces;

import org.vironit.edu.backend.entity.Airline;

import java.util.List;
import java.util.Optional;

public interface AirlineService {

    Optional<Airline> getAirlineById(Integer id);

    List<Airline> getAllAirlines(int page);

    Airline saveAirline(Airline airline);

    void deleteAirline(Integer id);

    Optional<Airline> findByName(String name);
}
