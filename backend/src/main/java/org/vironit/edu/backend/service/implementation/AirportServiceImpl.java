package org.vironit.edu.backend.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.vironit.edu.backend.entity.Airport;
import org.vironit.edu.backend.repository.AirportRepository;
import org.vironit.edu.backend.service.interfaces.AirportService;

import java.util.List;
import java.util.Optional;

@Service
public class AirportServiceImpl implements AirportService {

    private final AirportRepository airportRepository;

    @Autowired
    public AirportServiceImpl(AirportRepository airportRepository) {
        this.airportRepository = airportRepository;
    }

    @Override
    public Optional<Airport> getAirportById(Integer id) {
        return airportRepository.findById(id);
    }

    @Override
    public List<Airport> getAllAirports(int page) {
        return airportRepository.findAll(PageRequest.of(page, 3, Sort.by("name"))).getContent();
    }

    @Override
    public Airport saveAirport(Airport airport) {
        return airportRepository.save(airport);
    }

    @Override
    public void deleteAirport(Integer id) {
        airportRepository.deleteById(id);
    }

    @Override
    public Optional<Airport> findByName(String name) {
        return airportRepository.findByName(name);
    }
}
