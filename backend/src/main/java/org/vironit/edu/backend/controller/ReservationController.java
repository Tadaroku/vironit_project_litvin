package org.vironit.edu.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.backend.dto.ConverterDto;
import org.vironit.edu.backend.dto.ReservationDto;
import org.vironit.edu.backend.entity.Reservation;
import org.vironit.edu.backend.service.implementation.ReservationServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/reservations")
public class ReservationController {

    private final ReservationServiceImpl reservationService;

    private final ConverterDto converterDto;

    @Autowired
    public ReservationController(ConverterDto converterDto, ReservationServiceImpl reservationService) {
        this.converterDto = converterDto;
        this.reservationService = reservationService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Reservation> getRentById(@PathVariable(name = "id") Integer id) {
        Optional<Reservation> rent = reservationService.getReservationById(id);
        if (!rent.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(rent.get());
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public ResponseEntity<List<Reservation>> getReservationByPrice(@RequestParam(name = "before") double startingPrice, @RequestParam(name = "after") double finalPrice) {
        List<Reservation> reservations = reservationService.findByPriceBetween(startingPrice, finalPrice);
        return new ResponseEntity<>(reservations, HttpStatus.ACCEPTED);
    }

    @GetMapping
    public List<ReservationDto> getAllReservations(
            @RequestParam(value = "page") int page) {
        List<Reservation> reservations = reservationService.getAllReservations(page);
        List<ReservationDto> reservationDto = new ArrayList<>();
        for (Reservation reservation : reservations) {
            reservationDto.add(converterDto.fromReservation(reservation));
        }
        return reservationDto;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteReservationById(@PathVariable Integer id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        reservationService.deleteReservation(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateReservation(@PathVariable Integer id, @RequestBody ReservationDto reservationDto) {

        if (reservationDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        reservationService.saveReservation(converterDto.toReservation(reservationDto));

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createReservation(@RequestBody ReservationDto reservationDto) {
        if (reservationDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            reservationService.saveReservation(converterDto.toReservation(reservationDto));
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }


}
