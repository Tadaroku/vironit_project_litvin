package org.vironit.edu.backend.service.interfaces;

import org.vironit.edu.backend.entity.Airport;

import java.util.List;
import java.util.Optional;

public interface AirportService {

    Optional<Airport> getAirportById(Integer id);


    Airport saveAirport(Airport airport);

    void deleteAirport(Integer id);

    Optional<Airport> findByName(String name);

    List<Airport> getAllAirports(int page);
}
