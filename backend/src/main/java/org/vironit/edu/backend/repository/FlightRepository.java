package org.vironit.edu.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.vironit.edu.backend.entity.Flight;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Integer> {


}
