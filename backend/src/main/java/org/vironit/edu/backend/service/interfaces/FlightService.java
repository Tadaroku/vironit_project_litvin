package org.vironit.edu.backend.service.interfaces;

import org.vironit.edu.backend.entity.Flight;

import java.util.List;
import java.util.Optional;


public interface FlightService {

    Optional<Flight> getFlightById(Integer id);

    Flight saveFlight(Flight flight);

    void deleteFlight(Integer id);

    List<Flight> getAllFlights(int page);


}
