package org.vironit.edu.backend.entity.enums;

public enum ReservationStatus {
    FREE,
    RESERVED
}
