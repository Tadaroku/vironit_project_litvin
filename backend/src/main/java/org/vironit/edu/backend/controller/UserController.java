package org.vironit.edu.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.backend.dto.ConverterDto;
import org.vironit.edu.backend.dto.UserDto;
import org.vironit.edu.backend.entity.User;
import org.vironit.edu.backend.service.implementation.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserServiceImpl userService;

    private final ConverterDto converterDto;


    @Autowired
    public UserController(UserServiceImpl userService, ConverterDto converterDto) {
        this.userService = userService;
        this.converterDto = converterDto;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Long id) {

        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<User> result = this.userService.getUserById(id);

        return result.map(user -> new ResponseEntity<>(converterDto.fromUser(user), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping(value = "/email/{email}")
    public ResponseEntity<UserDto> getUserByEmail(@PathVariable String email) {

        if (email == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<User> result = this.userService.getUserByEmail(email);


        return result.map(user -> new ResponseEntity<>(converterDto.fromUser(user), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping(value = "/username/{username}")
    public ResponseEntity<UserDto> getUserByUsername(@PathVariable String username) {

        if (username == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<User> result = this.userService.getUserByUsername(username);


        return result.map(user -> new ResponseEntity<>(converterDto.fromUser(user), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping
    public List<UserDto> getAllUsers(
            @RequestParam(value = "page") int page) {

        List<User> users = userService.getAllUsers(page);
        List<UserDto> usersDto = new ArrayList<>();
        for (User user : users) {
            usersDto.add(converterDto.fromUser(user));
        }

        return usersDto;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUserById(@PathVariable Long id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        userService.deleteUserById(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateUser(@PathVariable Long id, @RequestBody UserDto userDto) {

        if (userDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        userService.saveUser(converterDto.toUser(userDto));

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody UserDto userDto) {

        if (userDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            userService.saveUser(converterDto.toUser(userDto));
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/phone/{phone}")
    public ResponseEntity<UserDto> getUserByPhone(@PathVariable String phone) {

        if (phone == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<User> result = this.userService.getUserByPhone(phone);


        return result.map(user -> new ResponseEntity<>(converterDto.fromUser(user), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUserByNameAndSurname(@RequestParam(name = "name") String name, @RequestParam(name = "surname") String surname) {
        List<User> users = userService.getUserByNameAndSurname(name, surname);
        return new ResponseEntity<>(users, HttpStatus.ACCEPTED);
    }

}
