package org.vironit.edu.backend.dto;

public class AirportDto {

    private Integer id;
    private String name;
    private String code;
    private String location;
    private String airlines;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlineName) {
        this.airlines = airlineName;
    }
}
