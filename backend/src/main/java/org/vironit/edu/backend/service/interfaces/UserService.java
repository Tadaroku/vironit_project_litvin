package org.vironit.edu.backend.service.interfaces;

import org.vironit.edu.backend.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> getUserById(Long id);

    Optional<User> getUserByEmail(String email);

    Optional<User> getUserByUsername(String username);

    List<User> getAllUsers(int page);

    User saveUser(User user);

    void deleteUserById(Long id);

    Optional<User> getUserByPhone(String phone);

    List<User> getUserByNameAndSurname(String name, String surname);
}
