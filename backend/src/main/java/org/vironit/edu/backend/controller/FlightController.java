package org.vironit.edu.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.backend.dto.ConverterDto;
import org.vironit.edu.backend.dto.FlightDto;
import org.vironit.edu.backend.entity.Flight;
import org.vironit.edu.backend.service.implementation.FlightServiceImpl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/flights")
public class FlightController {

    private final FlightServiceImpl flightService;

    private final ConverterDto converterDto;

    @Autowired
    public FlightController(ConverterDto converterDto, FlightServiceImpl flightService) {
        this.converterDto = converterDto;
        this.flightService = flightService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<FlightDto> getFlightById(@PathVariable Integer id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<Flight> result = flightService.getFlightById(id);
        return result.map(flight -> new ResponseEntity<>(converterDto.fromFlight(flight), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping
    public List<FlightDto> getAllFlights(
            @RequestParam(value = "page") int page) {
        List<Flight> flights = flightService.getAllFlights(page);
        List<FlightDto> flightDto = new ArrayList<>();
        for (Flight flight : flights) {
            flightDto.add(converterDto.fromFlight(flight));
        }
        return flightDto;

    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteFlightById(@PathVariable Integer id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        flightService.deleteFlight(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createFlight(@RequestBody FlightDto flightDto) {
        if (flightDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            flightService.saveFlight(converterDto.toFlight(flightDto));
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateFlight(@PathVariable Integer id, @RequestBody FlightDto flightDto) {

        if (flightDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            flightService.saveFlight(converterDto.toFlight(flightDto));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
