package org.vironit.edu.backend.entity.enums;

public enum TicketClass {
    GENERAL,
    BUSINESS,
    ECONOM
}
