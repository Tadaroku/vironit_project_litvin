package org.vironit.edu.backend.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.vironit.edu.backend.entity.Airline;
import org.vironit.edu.backend.repository.AirlineRepository;
import org.vironit.edu.backend.service.interfaces.AirlineService;

import java.util.List;
import java.util.Optional;

@Service
public class AirlineServiceImpl implements AirlineService {

    private final AirlineRepository airlineRepository;

    @Autowired
    public AirlineServiceImpl(AirlineRepository airlineRepository) {
        this.airlineRepository = airlineRepository;
    }

    @Override
    public Optional<Airline> getAirlineById(Integer id) {
        return airlineRepository.findById(id);
    }

    @Override
    public List<Airline> getAllAirlines(int page) {
        return airlineRepository.findAll(PageRequest.of(page, 3, Sort.by("name"))).getContent();
    }

    @Override
    public Airline saveAirline(Airline airline) {
        return airlineRepository.save(airline);
    }

    @Override
    public void deleteAirline(Integer id) {
        airlineRepository.deleteById(id);
    }

    @Override
    public Optional<Airline> findByName(String name) {
        return airlineRepository.findByName(name);
    }
}
