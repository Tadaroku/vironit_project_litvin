package org.vironit.edu.backend.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vironit.edu.backend.entity.*;
import org.vironit.edu.backend.entity.enums.ReservationStatus;
import org.vironit.edu.backend.entity.enums.RoleName;
import org.vironit.edu.backend.entity.enums.TicketClass;
import org.vironit.edu.backend.repository.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@SuppressWarnings("Duplicates")
public class ConverterDto {

    private final UserRepository userRepository;
    private final FlightRepository flightRepository;
    private final ReservationRepository reservationRepository;
    private final AirportRepository airportRepository;
    private final AirlineRepository airlineRepository;


    @Autowired
    public ConverterDto(UserRepository userRepository, FlightRepository flightRepository, ReservationRepository reservationRepository, AirportRepository airportRepository, AirlineRepository airlineRepository) {
        this.userRepository = userRepository;
        this.flightRepository = flightRepository;
        this.reservationRepository = reservationRepository;
        this.airportRepository = airportRepository;
        this.airlineRepository = airlineRepository;


    }

    public User toUser(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setUsername(userDto.getUsername());
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setPhone(userDto.getPhone());
        user.setRole(RoleName.valueOf(userDto.getRole()));
        List<Reservation> reservations = new ArrayList<>();
        if (userDto.getReservations() != null) {
            reservations.add(reservationRepository.findById(Integer.parseInt(userDto.getReservations())).orElse(null));
        }
        return user;
    }

    public UserDto fromUser(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setUsername(user.getUsername());
        userDto.setName(user.getName());
        userDto.setSurname(user.getSurname());
        userDto.setPhone(user.getPhone());
        userDto.setRole(user.getRole().name());
        if (user.getReservations() != null) {
            userDto.setReservations(String.valueOf(user.getReservations()));
        }
        return userDto;
    }

    public Reservation toReservation(ReservationDto reservationDto) {
        Reservation reservation = new Reservation();
        reservation.setId(reservationDto.getId());
        reservation.setReservationDate(LocalDate.parse(reservationDto.getReservationDate()));
        reservation.setTicketClass(TicketClass.valueOf(reservationDto.getTicketClass()));
        reservation.setReservationStatus(ReservationStatus.valueOf(reservationDto.getReservationStatus()));
        reservation.setSeatNumber(Integer.parseInt(reservationDto.getSeatNumber()));
        reservation.setLuggage(reservationDto.isLuggage());
        reservation.setPrice(Double.valueOf(reservationDto.getPrice()));
        if (reservationDto.getAirline() != null) {
            reservation.setAirline(airlineRepository.findByName(reservationDto.getAirline()).orElse(null));
        }
        if (reservationDto.getFlight() != null) {
            reservation.setFlight(flightRepository.findById(Integer.parseInt(reservationDto.getFlight())).orElse(null));
        }
        if (reservationDto.getUser() != null) {
            reservation.setUser(userRepository.findById(Long.parseLong(reservationDto.getUser())).orElse(null));
        }
        return reservation;
    }

    public ReservationDto fromReservation(Reservation reservation) {
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(reservation.getId());
        reservationDto.setReservationDate(reservation.getReservationDate().toString());
        reservationDto.setLuggage(reservation.isLuggage());
        reservationDto.setPrice(reservation.getPrice().toString());
        reservationDto.setTicketClass(reservation.getTicketClass().name());
        reservationDto.setReservationStatus(reservation.getReservationStatus().name());
        reservationDto.setSeatNumber(String.valueOf(reservation.getSeatNumber()));
        if (reservation.getAirline() != null) {
            reservationDto.setAirline(String.valueOf(reservation.getAirline()));
        }
        if (reservation.getFlight() != null) {
            reservationDto.setFlight(String.valueOf(reservation.getFlight()));
        }
        if (reservation.getUser() != null) {
            reservationDto.setUser(String.valueOf(reservation.getUser()));
        }
        return reservationDto;
    }

    public Flight toFlight(FlightDto flightDto) throws ParseException {
        Flight flight = new Flight();
        flight.setId(flightDto.getId());
        String pattern = "yyyy-MM-dd HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        flight.setDepartureTime(simpleDateFormat.parse(flightDto.getDepartureTime()));
        flight.setArrivalTime(simpleDateFormat.parse(flightDto.getArrivalTime()));
        flight.setAirplane(flightDto.getAirplane());
        flight.setFlightNumber(flightDto.getFlightNumber());
        if (flightDto.getAirportFrom() != null) {
            flight.setAirportFrom(airportRepository.findByName(flightDto.getAirportFrom()).orElse(null));
        }
        if (flightDto.getAirportTo() != null) {
            flight.setAirportTo(airportRepository.findByName(flightDto.getAirportTo()).orElse(null));
        }
        List<Reservation> reservations = new ArrayList<>();
        if (flightDto.getReservations() != null) {
            reservations.add(reservationRepository.findById(Integer.parseInt(flightDto.getReservations())).orElse(null));
        }
        return flight;
    }

    public FlightDto fromFlight(Flight flight) {
        FlightDto flightDto = new FlightDto();
        flightDto.setId(flight.getId());
        flightDto.setDepartureTime(flight.getDepartureTime().toString());
        flightDto.setArrivalTime(flight.getArrivalTime().toString());
        flightDto.setAirplane(flight.getAirplane());
        flightDto.setFlightNumber(flight.getFlightNumber());
        if (flight.getAirportFrom() != null) {
            flightDto.setAirportFrom(String.valueOf(flight.getAirportFrom()));
        }
        if (flight.getAirportTo() != null) {
            flightDto.setAirportTo(String.valueOf(flight.getAirportTo()));
        }
        if (flight.getReservations() != null) {
            flightDto.setReservations(String.valueOf(flight.getReservations()));
        }
        return flightDto;
    }

    public Airport toAirport(AirportDto airportDto) {
        Airport airport = new Airport();
        airport.setId(airportDto.getId());
        airport.setCode(airportDto.getCode());
        airport.setName(airportDto.getName());
        airport.setLocation(airportDto.getLocation());
        Set<Airline> airlines = new HashSet<>();
        if (airportDto.getAirlines() != null) {
            airlines.add(airlineRepository.findByName(airportDto.getAirlines()).orElse(null));
        }
        return airport;
    }

    public AirportDto fromAirport(Airport airport) {
        AirportDto airportDto = new AirportDto();
        airportDto.setId(airport.getId());
        airportDto.setName(airport.getName());
        airportDto.setCode(airport.getCode());
        airportDto.setLocation(airport.getLocation());
        if (airport.getAirlines() != null) {
            airportDto.setAirlines(String.valueOf(airport.getAirlines()));
        }
        return airportDto;
    }

    public Airline toAirline(AirlineDto airlineDto) {
        Airline airline = new Airline();
        airline.setId(airlineDto.getId());
        airline.setName(airlineDto.getName());
        List<Reservation> reservations = new ArrayList<>();
        if (airlineDto.getReservations() != null) {
            reservations.add(reservationRepository.findById(Integer.parseInt(airlineDto.getReservations())).orElse(null));
        }
        Set<Airport> airports = new HashSet<>();
        if (airlineDto.getAirports() != null) {
            airports.add(airportRepository.findByName(airlineDto.getAirports()).orElse(null));
        }
        return airline;
    }

    public AirlineDto fromAirline(Airline airline) {
        AirlineDto airlineDto = new AirlineDto();
        airlineDto.setId(airline.getId());
        airlineDto.setName(airline.getName());
        if (airline.getAirports() != null) {
            airlineDto.setAirports(String.valueOf(airline.getAirports()));
        }
        if (airline.getReservations() != null) {
            airlineDto.setReservations(String.valueOf(airline.getReservations()));
        }
        return airlineDto;
    }

}
