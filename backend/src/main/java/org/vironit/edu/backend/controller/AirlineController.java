package org.vironit.edu.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.backend.dto.AirlineDto;
import org.vironit.edu.backend.dto.ConverterDto;
import org.vironit.edu.backend.entity.Airline;
import org.vironit.edu.backend.service.implementation.AirlineServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/airlines")
public class AirlineController {

    private final AirlineServiceImpl airlineService;

    private final ConverterDto converterDto;


    @Autowired
    public AirlineController(AirlineServiceImpl airlineService, ConverterDto converterDto) {
        this.airlineService = airlineService;
        this.converterDto = converterDto;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteAirlineById(@PathVariable Integer id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        airlineService.deleteAirline(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createAirline(@RequestBody AirlineDto airlineDto) {

        if (airlineDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            airlineService.saveAirline(converterDto.toAirline(airlineDto));
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateAirline(@PathVariable Integer id, @RequestBody AirlineDto airlineDto) {

        if (airlineDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        airlineService.saveAirline(converterDto.toAirline(airlineDto));

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<AirlineDto> getAirlineByName(@PathVariable String name) {

        if (name == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Optional<Airline> result = airlineService.findByName(name);
        return result.map(airline -> new ResponseEntity<>(converterDto.fromAirline(airline), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AirlineDto> getAirlineById(@PathVariable Integer id) {

        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        }
        Optional<Airline> result = airlineService.getAirlineById(id);

        return result.map(airline -> new ResponseEntity<>(converterDto.fromAirline(airline), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }

    @GetMapping
    public List<AirlineDto> getAllAirlines(
            @RequestParam(value = "page") int page) {

        List<Airline> airlines = airlineService.getAllAirlines(page);
        List<AirlineDto> airlineDto = new ArrayList<>();
        for (Airline aviacompany : airlines) {
            airlineDto.add(converterDto.fromAirline(aviacompany));

        }
        return airlineDto;

    }

}
