package org.vironit.edu.backend.service.interfaces;

import org.vironit.edu.backend.entity.Reservation;

import java.util.List;
import java.util.Optional;

public interface ReservationService {

    Optional<Reservation> getReservationById(Integer id);

    List<Reservation> getAllReservations(int page);

    Reservation saveReservation(Reservation reservation);

    void deleteReservation(Integer id);

//    List<Ticket> findTicketsByFlightId(Flight flightId);
//
//    List<Ticket> findTicketsByUsername(User username);

    List<Reservation> findByPriceBetween(Double startPrice, Double finalPrice);


}
