package org.vironit.edu.backend.entity.enums;

public enum RoleName {
    ADMIN,
    USER
}
