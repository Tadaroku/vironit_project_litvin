<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <form class="form-horizontal" action="do?command=Admin">
        <fieldset>

            <!-- Form Name -->
            <legend>Admin panel</legend>


            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=EditUsers" role="button">Edit users</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=AddFlights" role="button">Add Flight</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=AddReservations" role="button">Add Reservation</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=AddAirports" role="button">Add Airport</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=AddAirlines" role="button">Add Airline</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=EditAirports" role="button">Edit Airports</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=EditFlights" role="button">Edit Flights</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=EditAirlines" role="button">Edit Airlines</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="do?command=EditReservations" role="button">Edit Reservations</a></p>
            </div>

        </fieldset>
    </form>

</div>
</body>
</html>
