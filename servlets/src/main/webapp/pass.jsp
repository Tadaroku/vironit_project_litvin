<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <div class="jumbotron text-xs-center">
        <h1 class="display-3">Your password was changed!</h1>
        <p class="lead">
            <a class="btn btn-primary btn-sm" href="do?command=Profile" role="button">Go To Your Profile</a>
        </p>
    </div>

</div>
</body>
</html>
