<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <form class="form-horizontal" action="do?command=SignUp" method="post">
        <fieldset>

            <!-- Form Name -->
            <legend>Form SignUp</legend>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="email">Email</label>
                <div class="col-md-4">
                    <input id="email" name="email" value="" type="text" placeholder="Enter your email"
                           pattern="(.+)@(.+)$"
                           title="Email example: email@email.com"
                           class="form-control input-md" required="">
                    <c:if test="${error == 'error'}">
                        <span style="color: red">Email is already exists</span>
                    </c:if>
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>
                <div class="col-md-4">
                    <input id="password" name="password" value="" type="password" placeholder="Enter your password"
                           pattern="[a-zA-Z0-9а-яА-ЯёЁ]{4,}"
                           title="Password must contain at least 1 letter and minimum 4 characters"
                           class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="repeatPassword">Repeat Password</label>
                <div class="col-md-4">
                    <input id="repeatPassword" name="repeatPassword" value="" type="password"
                           placeholder="Confirm your password" pattern="[a-zA-Z0-9а-яА-ЯёЁ]{4,}"
                           title="Password must contain at least 1 letter and minimum 4 characters"
                           class="form-control input-md" required="">
                    <span id="password-message" style="color: red"></span>

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-4">
                    <input id="name" name="name" value="" type="text" placeholder="Enter your name"
                           pattern="[a-zA-Zа-яА-ЯёЁ]{2,}"
                           title="Name must contain at least 2 characters"
                           class="form-control input-md" required="">

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="surname">Surname</label>
                <div class="col-md-4">
                    <input id="surname" name="surname" value="" type="text" placeholder="Enter your surname"
                           pattern="[a-zA-Zа-яА-ЯёЁ]{2,}"
                           title="Surname must contain at least 2 characters"
                           class="form-control input-md" required="">

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="phone">Phone number</label>
                <div class="col-md-4">
                    <input id="phone" name="phone" value="+375 ()" type="text" placeholder="+375 (XX) XXX-XX-XX"
                           pattern="^\+375 \((17|29|33|44|25)\) [0-9]{3}-[0-9]{2}-[0-9]{2}$"
                           title="Phone example: +375 (17|29|33|44|25) 111-11-11"
                           class="input-medium bfh-phone" required="">

                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="signupsubmit"></label>
                <div class="col-md-4">
                    <button id="signupsubmit" name="signupsubmit" class="btn btn-success">SignUp</button>
                </div>
            </div>

        </fieldset>
    </form>


</div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="js/check.js"></script>
</html>




