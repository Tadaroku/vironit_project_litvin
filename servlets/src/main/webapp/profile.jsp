<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://example.com/functions" prefix="f" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>
    <h2>Profile Data</h2>
    <form class="form-horizontal" method="post" action="do?command=Profile">
        <fieldset>

            <!-- Form Name -->
            <legend>My form</legend>


            <!-- Password input-->
            <%--<div class="form-group">--%>
                <%--<label class="col-md-4 control-label" for="password">Password</label>--%>
                <%--<div class="col-md-4">--%>
                    <%--<input id="password" name="password" type="password" placeholder="" class="form-control input-md"--%>
                           <%--value="${user.password}">--%>
                    <%--<span class="help-block">${help_password}</span>--%>
                <%--</div>--%>
            <%--</div>--%>

            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-4">
                    <input id="name" name="name" type="text" placeholder="" class="form-control input-md"
                           value="${user.name}">
                    <span class="help-block">${help_name}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="surname">Surname</label>
                <div class="col-md-4">
                    <input id="surname" name="surname" type="text" placeholder="" class="form-control input-md"
                           value="${user.surname}">
                    <span class="help-block">${help_surname}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="phone">Phone</label>
                <div class="col-md-4">
                    <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md"
                           value="${user.phone}">
                    <span class="help-block">${help_phone}</span>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="update"></label>
                <div class="col-md-4">
                    <button id="update" name="update" class="btn btn-success">Update</button>
                </div>
            </div>

        </fieldset>
    </form>


    <c:forEach var="reservations" items="${reservations}">
        <h2>Reservation Info</h2>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Luggage</th>
                <th scope="col">Price</th>
                <th scope="col">Seat</th>
                <th scope="col">Reservation Date</th>
                <th scope="col">Status</th>
                <th scope="col">Airline</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${reservations.luggage? '+' : '-'}</td>
                <td>${reservations.price}</td>
                <td>${reservations.seat}</td>
                <td>${f:formatLocalDate(reservations.reservationDate,"dd-MM-yyyy")}</td>
                <td>${reservations.status? 'OK' : 'BOOKED'}</td>
                <td>${reservations.airline.name}</td>
            </tr>

            </tbody>
        </table>

        <h2>Flight Info</h2>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Departure Time</th>
                <th scope="col">Arrival Time</th>
                <th scope="col">Airport From</th>
                <th scope="col">Location From</th>
                <th scope="col">Airport To</th>
                <th scope="col">Location To</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${f:formatLocalDateTime(reservations.flight.departureTime,"dd-MM-yyyy HH:mm")}</td>
                <td>${f:formatLocalDateTime(reservations.flight.arrivalTime,"dd-MM-yyyy HH:mm")}</td>
                <td>${reservations.flight.airportFrom.name}</td>
                <td>${reservations.flight.airportFrom.location}</td>
                <td>${reservations.flight.airportTo.name}</td>
                <td>${reservations.flight.airportTo.location}</td>
            </tr>
            </tbody>
        </table>

        <p><a href="do?command=Cancel&reservation_id=${reservations.id}">
            <div class="form-group">
                <label class="col-md-4 control-label" for="cancel"></label>
                <div class="col-md-4">
                    <button id="cancel" name="cancel" class="btn btn-success">Cancel Reservation</button>
                </div>
            </div>
        </a></p>

    </c:forEach>


    <form class="form-horizontal" method="post" action="do?command=Profile">
        <fieldset>

            <!-- Form Name -->
            <h2>Logout</h2>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="logout"></label>
                <div class="col-md-4">
                    <button id="logout" name="logout" value="1" class="btn btn-success">Logout</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
<footer class="page-footer font-small unique-color-dark pt-4">
    <div class="footer-copyright text-center py-3">© 2019 Copyright: LE Aviasales </div>
</footer>
</body>
</html>
