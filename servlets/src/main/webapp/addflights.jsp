<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container" align="center">
    <%@ include file="include/menu.jsp" %>
    <form class="form-horizontal" action="do?command=AddFlights" method="post">
        <fieldset>

            <!-- Form Name -->
            <h3>Add Flights</h3>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="departureTime">Departure Time</label>
                <div class="col-md-4">
                    <input id="departureTime" name="departureTime" type="text" placeholder="" class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="arrivalTime">Arrival Time</label>
                <div class="col-md-4">
                    <input id="arrivalTime" name="arrivalTime" type="text" placeholder="" class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="airplaneName">Airplane Name</label>
                <div class="col-md-4">
                    <input id="airplaneName" name="airplaneName" type="text" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="flightNumber">Flight Number</label>
                <div class="col-md-4">
                    <input id="flightNumber" name="flightNumber" type="text" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="airportFromId">Airport From Id</label>
                <div class="col-md-4">
                    <input id="airportFromId" name="airportFromId" type="text" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="airportToId">Airport To Id</label>
                <div class="col-md-4">
                    <input id="airportToId" name="airportToId" type="text" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="submit"></label>
                <div class="col-md-4">
                    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>


        </fieldset>
    </form>
</body>
</html>
