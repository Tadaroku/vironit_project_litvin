<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<link href="css/pass.css" rel="stylesheet" id="bootstrap-css">
<script src="js/pass.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>
    <form class="form-horizontal" method="post" action="do?command=Edit">
            <div class="row">
                <div class="col-sm-4">

                    <!--Password input-->
                    <label>New Password</label>
                    <div class="form-group pass_show">
                        <input id="password" name="password" type="password" placeholder="New Password"
                               class="form-control">
                    </div>

                <label>Confirm Password</label>
                <div class="form-group pass_show">
                    <input id="confirmPassword" name="confirmPassword" type="password" placeholder="Confirm password"
                           class="form-control">
                </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label" for="update"></label>
                        <div class="col-md-4">
                            <button id="update" name="update" class="btn btn-success">Change password</button>
                        </div>
                        <c:if test="${error == 'error'}">
                            <span style="color: red">Passwords must be identical</span>
                        </c:if>
                    </div>

            </div>




        </div>
</form>
</div>
</body>
</html>
