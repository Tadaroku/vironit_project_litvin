<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <form class="form-horizontal" action="do?command=Login" method="post">
        <fieldset>

            <!-- Form Name -->
            <legend>Form Login</legend>

            <!-- Text input-->

            <div class="form-group">
                <label class="col-md-4 control-label" for="email">Email</label>
                <div class="col-md-4">
                    <input id="email" name="email" value="admin@mail.com" type="text" placeholder="Enter your email"
                           pattern="(.+)@(.+)$"
                           title="Email example: email@email.com" class="form-control input-md" required="">
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>
                <div class="col-md-4">
                    <input id="password" name="password" value="admin123" type="password"
                           placeholder="Enter your password" pattern="[a-zA-Z0-9а-яА-ЯёЁ]{4,}"
                           title="Password must contain at least 1 letter and minimum 4 characters"
                           class="form-control input-md" required="">

                </div>
            </div>


            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="signupsubmit"></label>
                <div class="col-md-4">
                    <button id="signupsubmit" name="signupsubmit" class="btn btn-success">SignIn</button>
                </div>
            </div>

            <input hidden name="command" value="authorization">
            <c:if test="${error == true}">
                <span style="color: red">Password or email doesn't match</span>
            </c:if>

        </fieldset>
    </form>


</body>
</html>

