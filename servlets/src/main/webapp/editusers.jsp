<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <div class="container">
        <div class="row">
            <div class=col-md-1>ID</div>
            <div class=col-md-2>Email</div>
            <div class=col-md-1>Password</div>
            <div class=col-md-2>Name</div>
            <div class=col-md-2>Surname</div>
            <div class=col-md-2>Phone</div>
            <div class=col-md-1>Role</div>
        </div>
    </div>

    <div class="container">
        <c:forEach items="${users}" var="user">
            <form class="update-user-${user.id}" action="do?command=EditUsers" method=POST>

                <div class="row">
                    <div class=col-md-1>
                        <input id="id" class="form-control input-md"
                               name="id"
                               value="${user.id}"/>
                    </div>
                    <div class=col-md-2>
                        <input id="email" pattern="^(.+)@(.+)$" class="form-control input-md" name="email"
                               value="${user.email}"/>
                    </div>
                    <div class=col-md-1>
                        <input id="password" pattern="[a-zA-Z0-9а-яА-ЯёЁ]{4,}" class="form-control input-md" name="password"
                               value="${user.password}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="name" pattern="[a-zA-Zа-яА-ЯёЁ]{2,}" class="form-control input-md" name="name"
                               value="${user.name}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="surname" pattern="[a-zA-Zа-яА-ЯёЁ]{2,}" class="form-control input-md" name="surname"
                               value="${user.surname}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="phone" pattern="^\+375 \((17|29|33|44)\) [0-9]{3}-[0-9]{2}-[0-9]{2}$"
                               class="form-control input-md" name="phone"
                               value="${user.phone}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="role" class="form-control input-md" name="role"
                               value="${user.role.role}"/>
                    </div>

                    <button id="delete" value="delete" name="delete" class="btn btn-danger col-md-1">
                        Delete
                    </button>
                </div>
            </form>
            <p></p>
        </c:forEach>
    </div>

</div>
</body>
