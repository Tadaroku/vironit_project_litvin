<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://example.com/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <h2>Reservation list:</h2>
    <form class="form-horizontal" method="post" action="do?command=UsersWithReservations">
        <c:forEach var="reservation" items="${reservations}">

        <div class="row">
            <div class="col-1">
            </div>

            <div class="col">
                <div class="media shadow p-3 mb-5 bg-white rounded">
                    <div class="media-body">
                        <h5 class="mt-0">Users with reservations</h5>


                        <p>
                            <span class="font-weight-bold">Email:</span> ${reservation.user.email}<br>
                            <span class="font-weight-bold">Name:</span> ${reservation.user.name}<br>
                            <span class="font-weight-bold">Surname:</span> ${reservation.user.surname}<br>
                            <span class="font-weight-bold">Phone:</span> ${reservation.user.phone}<br>
                            <span class="font-weight-bold">Price:</span> ${reservation.price}<br>
                            <span class="font-weight-bold">Luggage:</span> ${reservation.luggage? '+' : '-'}<br>
                            <span class="font-weight-bold">Price:</span> ${reservation.price}<br>
                            <span class="font-weight-bold">Seat:</span> ${reservation.seat}<br>
                            <span class="font-weight-bold">Reservation Date:</span> ${f:formatLocalDate(reservation.reservationDate,"dd-MM-yyyy")}<br>
                            <span class="font-weight-bold">Status:</span> ${reservation.status? 'FREE' : 'BOOKED'}<br>
                            <span class="font-weight-bold">Airline:</span> ${reservation.airline.name}<br>
                            <span class="font-weight-bold">Departure Time:</span> ${f:formatLocalDateTime(reservation.flight.departureTime,"dd-MM-yyyy HH:mm")}<br>
                            <span class="font-weight-bold">Arrival Time:</span> ${f:formatLocalDateTime(reservation.flight.arrivalTime,"dd-MM-yyyy HH:mm")}<br>
                            <span class="font-weight-bold">Airplane:</span> ${reservation.flight.airplane}<br>
                            <span class="font-weight-bold">Flight Number:</span> ${reservation.flight.flightNumber}<br>
                            <span class="font-weight-bold">Airport From:</span> ${reservation.flight.airportFrom.name}<br>
                            <span class="font-weight-bold">Location:</span> ${reservation.flight.airportFrom.location}<br>
                            <span class="font-weight-bold">Code:</span> ${reservation.flight.airportFrom.code}<br>
                            <span class="font-weight-bold">Airport To:</span> ${reservation.flight.airportTo.name}<br>
                            <span class="font-weight-bold">Location:</span> ${reservation.flight.airportTo.location}<br>
                            <span class="font-weight-bold">Code:</span> ${reservation.flight.airportTo.code}<br>

                    </div>
                    <div class="media-body">
                            <p><a href="do?command=Cancel&reservation_id=${reservation.id}">Cancel reservation</a></p>
                    </div>
                </div>
            </div>


            <div class="col-1">
            </div>
        </div>
        </c:forEach>

</div>
</body>
</html>
