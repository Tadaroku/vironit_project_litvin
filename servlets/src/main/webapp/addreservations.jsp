<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container" align="center">
    <%@ include file="include/menu.jsp" %>
    <form class="form-horizontal" action="do?command=AddReservations" method="post">
        <fieldset>

            <!-- Form Name -->
            <h3>Add Reservations</h3>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="luggage">Luggage</label>
                <div class="col-md-4">
                    <input id="luggage" name="luggage" type="text" value="true" placeholder="" class="form-control input-md"
                           required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="price">Price</label>
                <div class="col-md-4">
                    <input id="price" name="price" type="text" placeholder="" value="200.0" class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="seat">Seat</label>
                <div class="col-md-4">
                    <input id="seat" name="seat" type="text" placeholder=""  class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="reservationDate">Reservation Date</label>
                <div class="col-md-4">
                    <input id="reservationDate" name="reservationDate" value="2019-07-05" type="text" placeholder=""
                           class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="status">Status</label>
                <div class="col-md-4">
                    <input id="status" name="status" type="text" placeholder="" value="true" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="flightId">Flight Id</label>
                <div class="col-md-4">
                    <input id="flightId" name="flightId" type="text" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="airlineId">Airline Id</label>
                <div class="col-md-4">
                    <input id="airlineId" name="airlineId" type="text" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="submit"></label>
                <div class="col-md-4">
                    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>


        </fieldset>
    </form>
</body>
</html>
