<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://example.com/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <div class="container">
        <div class="row">
            <div class=col-md-1>ID</div>
            <div class=col-md-1>Luggage</div>
            <div class=col-md-2>Price</div>
            <div class=col-md-2>Seat</div>
            <div class=col-md-2>Reservation Date</div>
            <div class=col-md-1>Status</div>
        </div>
    </div>

    <div class="container">
        <c:forEach items="${reservations}" var="reservation">
            <form class="update-user-${reservation.id}" action="do?command=EditReservations" method=POST>
                <div class="row">
                    <div class=col-md-1>
                        <input id="id" class="form-control input-md" name="id"
                               value="${reservation.id}"/>
                    </div>
                    <div class=col-md-1>
                        <input id="luggage" class="form-control input-md" name="luggage"
                               value="${reservation.luggage}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="price" class="form-control input-md"
                               name="price"
                               value="${reservation.price}"/>
                    </div>


                    <div class=col-md-2>
                        <input id="seat" class="form-control input-md" name="seat"
                               value="${reservation.seat}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="reservationDate" class="form-control input-md" name="reservationDate"
                               value="${reservation.reservationDate}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="status"
                               class="form-control input-md" name="status"
                               value="${reservation.status}"/>
                    </div>

                    <button id="update" value="update" name="update" class="btn btn-success col-md-1">
                        Update
                    </button>

                    <button id="delete" value="delete" name="delete" class="btn btn-danger col-md-1">
                        Delete
                    </button>
                </div>
            </form>
            <p></p>
        </c:forEach>
    </div>

</div>
</body>
