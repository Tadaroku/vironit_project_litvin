$('#password, #repeatPassword').on('keyup',function () {
    if($('#password').val() != $('#repeatPassword').val()){
        console.log('password doesnt match');
        $('#password-message').html('Passwords should be identical');
        $('#password, #repeatPassword').css('border-color','red');
    }else{
        console.log('password match');
        $('#password-message').html('').css('color','green');
        $('#password, #repeatPassword').css('border-color','#ced4da');
    }
});

function fieldControl(obj, regex) {
    if(obj.val().match(regex)){
        obj.css('border-color','#ced4da');
    }else{
        obj.css('border-color','red');
    }
}
