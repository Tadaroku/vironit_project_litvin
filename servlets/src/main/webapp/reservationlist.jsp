<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://example.com/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <h2>Flight list:</h2>
    <form class="form-horizontal" method="post" action="do?command=ReservationList">
        <c:forEach var="reservation" items="${reservations}">
            <c:if test="${reservation.status==true}">

        <div class="row">
            <div class="col-1">
            </div>

            <div class="col">
                <div class="media shadow p-3 mb-5 bg-white rounded">
                    <div class="media-body">
                        <h5 class="mt-0">Reservation Info</h5>


                        <p>
                            <span class="font-weight-bold">Luggage:</span> ${reservation.luggage? '+' : '-'}<br>
                            <span class="font-weight-bold">Price:</span> ${reservation.price}<br>
                            <span class="font-weight-bold">Seat:</span> ${reservation.seat}<br>
                            <span class="font-weight-bold">Reservation Date:</span> ${f:formatLocalDate(reservation.reservationDate,"dd-MM-yyyy")}<br>
                            <span class="font-weight-bold">Status:</span> ${reservation.status? 'FREE' : 'BOOKED'}<br>
                            <span class="font-weight-bold">Airline:</span> ${reservation.airline.name}<br>
                            <span class="font-weight-bold">Departure Time:</span> ${f:formatLocalDateTime(reservation.flight.departureTime,"dd-MM-yyyy HH:mm")}<br>
                            <span class="font-weight-bold">Arrival Time:</span> ${f:formatLocalDateTime(reservation.flight.arrivalTime,"dd-MM-yyyy HH:mm")}<br>
                            <span class="font-weight-bold">Airplane:</span> ${reservation.flight.airplane}<br>
                            <span class="font-weight-bold">Flight Number:</span> ${reservation.flight.flightNumber}<br>
                            <span class="font-weight-bold">Airport From:</span> ${reservation.flight.airportFrom.name}<br>
                            <span class="font-weight-bold">Location:</span> ${reservation.flight.airportFrom.location}<br>
                            <span class="font-weight-bold">Code:</span> ${reservation.flight.airportFrom.code}<br>
                            <span class="font-weight-bold">Airport To:</span> ${reservation.flight.airportTo.name}<br>
                            <span class="font-weight-bold">Location:</span> ${reservation.flight.airportTo.location}<br>
                            <span class="font-weight-bold">Code:</span> ${reservation.flight.airportTo.code}<br>

                    </div>
                    <div class="media-body">
                        <c:if test="${user==null}">
                            <p><a href="do?command=Login">LogIn</a> or <a href="do?command=SignUp">SignUp</a> to make a
                                reservation</p>
                        </c:if>
                        <c:if test="${user!=null}">
                            <p><a href="do?command=Add&reservation_id=${reservation.id}">Add to my reservation</a></p>
                        </c:if>
                    </div>
                </div>
            </div>


            <div class="col-1">
            </div>
        </div>
            </c:if>
        </c:forEach>

</div>
</body>
</html>
