<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://example.com/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <div class="container">
        <div class="row">
            <div class=col-md-1>ID</div>
            <div class=col-md-2>Departure Time</div>
            <div class=col-md-2>Arrival Time</div>
            <div class=col-md-1>Airplane</div>
            <div class=col-md-1>Flight Number</div>
            <div class=col-md-1>Airport From</div>
            <div class=col-md-1>Airport To</div>
        </div>
    </div>

    <div class="container">
        <c:forEach items="${flights}" var="flight">
            <form class="update-user-${flight.id}" action="do?command=EditFlights" method=POST>
                <div class="row">
                    <div class=col-md-1>
                        <input id="id" class="form-control input-md" name="id"
                               value="${flight.id}"/>
                    </div>
                    <div class=col-md-2>
                        <input id="departureTime" class="form-control input-md" name="departureTime"
                               value="${flight.departureTime}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="arrivalTime" class="form-control input-md"
                               name="arrivalTime"
                               value="${flight.departureTime}"/>
                    </div>


                    <div class=col-md-1>
                        <input id="name" class="form-control input-md" name="name"
                               value="${flight.airplane}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="flightNumber" class="form-control input-md" name="flightNumber"
                               value="${flight.flightNumber}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="airportFrom"
                               class="form-control input-md" name="airportFrom"
                               value="${flight.airportFrom.name}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="airportTo" class="form-control input-md" name="airportTo"
                               value="${flight.airportTo.name}"/>
                    </div>

                    <button id="update" value="update" name="update" class="btn btn-success col-md-1">
                        Update
                    </button>

                    <button id="delete" value="delete" name="delete" class="btn btn-danger col-md-1">
                        Delete
                    </button>
                </div>
            </form>
            <p></p>
        </c:forEach>
    </div>

</div>
</body>
