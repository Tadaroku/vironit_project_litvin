<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <div class="container">
        <div class="row">
            <div class=col-md-1>ID</div>
            <div class=col-md-2>Airport Name</div>
            <div class=col-md-2>Airport Code</div>
            <div class=col-md-2>Airport Location</div>
        </div>
    </div>

    <div class="container">
        <c:forEach items="${airports}" var="airport">
            <form class="update-user-${airport.id}" action="do?command=EditAirports" method=POST>
                <div class="row">
                    <div class=col-md-1>
                        <input id="id" class="form-control input-md" name="id"
                               value="${airport.id}"/>
                    </div>
                    <div class=col-md-2>
                        <input id="name" class="form-control input-md" name="name"
                               value="${airport.name}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="code" class="form-control input-md"
                               name="code"
                               value="${airport.code}"/>
                    </div>


                    <div class=col-md-2>
                        <input id="location" class="form-control input-md" name="location"
                               value="${airport.location}"/>
                    </div>

                    <button id="update" value="update" name="update" class="btn btn-success col-md-2">
                        Update
                    </button>

                    <button id="delete" value="delete" name="delete" class="btn btn-danger col-md-2">
                        Delete
                    </button>
                </div>
            </form>
            <p></p>
        </c:forEach>
    </div>

</div>
</body>
