package org.vironit.edu.controller;

import org.vironit.edu.service.ReservationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CmdUsersWithReservations extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        ReservationService reservationService = new ReservationService();
        HttpSession session = req.getSession();
        session.setAttribute("reservations", reservationService.getAll());
        return null;
    }
}
