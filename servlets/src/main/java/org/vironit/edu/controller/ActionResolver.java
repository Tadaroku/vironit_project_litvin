package org.vironit.edu.controller;

import javax.servlet.http.HttpServletRequest;

public class ActionResolver {

    Action resolve(HttpServletRequest req) {
        Action result = Action.ERROR;
        String command = req.getParameter("command");
        if (command == null)
            return Action.HOME;
        try {
            Action action = Action.valueOf(command.toUpperCase());
            req.getServletContext().log("RESOLVE:" + action.cmd.toString());
            result = Action.valueOf(command.toUpperCase());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
