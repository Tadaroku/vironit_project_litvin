package org.vironit.edu.controller;

import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.bean.FlightBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.service.FlightService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

public class CmdEditFlights extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        FlightService flightService = new FlightService();
        AirportService airportService = new AirportService();
        UserBean admin = Util.getUserBean(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            int id = Integer.parseInt(Form.getString(req, "id"));
            LocalDateTime departureTime = LocalDateTime.parse(req.getParameter("departureTime"));
            LocalDateTime arrivalTime = LocalDateTime.parse(req.getParameter("arrivalTime"));
            String name = Form.getString(req, "name");
            String flightNumber = Form.getString(req, "flightNumber");
            AirportBean airportBeanFrom = new AirportBean();
            AirportBean airportBeanTo = new AirportBean();
            airportBeanFrom.setId(airportBeanFrom.getId());
            airportBeanTo.setId(airportBeanTo.getId());
            FlightBean flightBean = new FlightBean(id, departureTime, arrivalTime, name, flightNumber, airportBeanFrom, airportBeanTo);
            if (req.getParameter("update") != null) {
                flightService.updateFlightBean(flightBean);
            }
            if (req.getParameter("delete") != null) {
                flightService.deleteFlightBean(flightBean);
            }

        }
        req.getServletContext().setAttribute("flights", flightService.getAllFlightsBean());
        req.getServletContext().setAttribute("airports", airportService.findAllAirports());
        return null;
    }
}

