package org.vironit.edu.controller;

import org.vironit.edu.bean.UserBean;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdAdmin extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        UserBean user = Util.getUserBean(req);
        if (user == null || (!user.getEmail().equals("admin@mail.com") && !user.getPassword().equals("admin123"))) {
            return Action.LOGIN.cmd;
        }
        return null;
    }
}
