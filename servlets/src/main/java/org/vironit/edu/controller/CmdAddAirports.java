package org.vironit.edu.controller;

import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdAddAirports extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {

            String name = Form.getString(req, "name");
            String code = Form.getString(req, "code");
            String location = Form.getString(req, "location");
            AirportBean airport = new AirportBean(0, name, code, location);
            AirportService airportService = new AirportService();


            airportService.save(airport);
            return Action.EDITAIRPORTS.cmd;
        }
        return null;
    }
}
