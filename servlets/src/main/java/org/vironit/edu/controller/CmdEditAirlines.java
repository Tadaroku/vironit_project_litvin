package org.vironit.edu.controller;

import org.vironit.edu.bean.AirlineBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.service.AirlineService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdEditAirlines extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        AirlineService airlineService = new AirlineService();
        UserBean admin = Util.getUserBean(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            int id = Integer.parseInt(Form.getString(req, "id"));
            String name = Form.getString(req, "name");

            AirlineBean airlineBean = new AirlineBean(id, name);
            if (req.getParameter("update") != null) {
                airlineService.updateAirlineBean(airlineBean);
            }
            if (req.getParameter("delete") != null) {
                airlineService.deleteAirlineBean(airlineBean);
            }

        }
        req.getServletContext().setAttribute("airlines", airlineService.getAllAirlineBeans());
        return null;
    }
}
