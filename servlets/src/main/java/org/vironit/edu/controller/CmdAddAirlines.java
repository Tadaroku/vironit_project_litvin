package org.vironit.edu.controller;

import org.vironit.edu.bean.AirlineBean;
import org.vironit.edu.service.AirlineService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdAddAirlines extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {

            String name = Form.getString(req, "name");
            AirlineBean airlineBean = new AirlineBean(0, name);
            AirlineService airlineService = new AirlineService();
            airlineService.saveAirline(airlineBean);


            return Action.EDITAIRLINES.cmd;
        }
        return null;
    }
}
