package org.vironit.edu.controller;

public enum Action {

    LOGIN {{
        cmd = new CmdLogin();
    }},

    SIGNUP {{
        cmd = new CmdSignup();
    }},

    CANCEL {{
        cmd = new CmdCancel();
    }},
    ERROR {{
        cmd = new CmdError();
    }},

    PROFILE {{
        cmd = new CmdProfile();
    }},

    USERSWITHRESERVATIONS {{
        cmd = new CmdUsersWithReservations();
    }},

    EDIT {{
        cmd = new CmdEdit();
    }},

    ADMIN {{
        cmd = new CmdAdmin();
    }},

    EDITUSERS {{
        cmd = new CmdEditUsers();
    }},

    PASS {{
        cmd = new CmdPass();
    }},

    ADDFLIGHTS {{
        cmd = new CmdAddFlights();
    }},

    ADDAIRPORTS {{
        cmd = new CmdAddAirports();
    }},

    ADDAIRLINES {{
        cmd = new CmdAddAirlines();
    }},

    ADDRESERVATIONS {{
        cmd = new CmdAddReservations();
    }},

    EDITAIRPORTS {{
        cmd = new CmdEditAirports();
    }},

    EDITFLIGHTS {{
        cmd = new CmdEditFlights();
    }},

    EDITAIRLINES {{
        cmd = new CmdEditAirlines();
    }},

    EDITRESERVATIONS {{
        cmd = new CmdEditReservations();
    }},

    RESERVATIONLIST {{
        cmd = new CmdReservationList();
    }},

    ADD {{
        cmd = new CmdAdd();
    }},

    HOME {{
        cmd = new CmdHome();
    }};

    public String getJsp() {
        return "/" + this.cmd.toString().toLowerCase() + ".jsp";
    }

    public Cmd cmd = new CmdError();
}
