package org.vironit.edu.controller;

import org.vironit.edu.bean.UserBean;
import org.vironit.edu.service.UserService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdEdit extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        UserBean user = Util.getUserBean(req);
        if (user == null)
            return Action.LOGIN.cmd;

        if (Form.isPost(req)) {
            if (req.getParameter("update") != null) {
                String password = Form.getString(req, "password");
                String confirmPassword = Form.getString(req, "confirmPassword");
                if (!password.equals(confirmPassword)) {
                    req.setAttribute("error", "error");
                    return Action.EDIT.cmd;
                }
                else {
                    user.setPassword(password);
                    UserService userService = new UserService();
                    userService.updateWithPass(user);
                    return Action.PASS.cmd;

            }
        }

    }
        return null;
}
}
