package org.vironit.edu.controller;

import org.vironit.edu.bean.UserBean;
import org.vironit.edu.service.RoleService;
import org.vironit.edu.service.UserService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdEditUsers extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        UserService userService = new UserService();
        RoleService roleService = new RoleService();
        UserBean admin = Util.getUserBean(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            long id = Long.parseLong(Form.getString(req, "id"));
            String email = Form.getString(req, "email");
            String password = Form.getString(req, "password");
            String name = Form.getString(req, "name");
            String surname = Form.getString(req, "surname");
            String phone = Form.getString(req, "phone");
//            RoleBean roleBean = new RoleBean();
//            int roleId = Integer.parseInt(Form.getString(req, "id"));
//            String role = Form.getString(req, "role");
//            roleBean.setId(roleId);
//            roleBean.setRole(role);
//            admin.setRole(roleBean);


            UserBean user = new UserBean(id, email, password, name, surname, phone);
            if (req.getParameter("delete") != null) {
                userService.deleteUser(user);
            }

        }
        req.getServletContext().setAttribute("roles", roleService.getAllRoles());
        req.getServletContext().setAttribute("users", userService.getAllUsersBean());
        return null;
    }
}
