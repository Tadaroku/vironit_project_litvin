package org.vironit.edu.controller;

import org.vironit.edu.bean.ReservationBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;

public class CmdEditReservations extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        ReservationService reservationService = new ReservationService();
        ReservationBean reservationBean = new ReservationBean();
        UserBean admin = Util.getUserBean(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            reservationBean.setId(Integer.parseInt(req.getParameter("id")));
            reservationBean.setLuggage(Boolean.parseBoolean(req.getParameter("luggage")));
            reservationBean.setPrice(Double.parseDouble(req.getParameter("price")));
            reservationBean.setSeat(req.getParameter("seat"));
            reservationBean.setReservationDate(LocalDate.parse(req.getParameter("reservationDate")));
            reservationBean.setStatus(Boolean.parseBoolean(req.getParameter("status")));
            if (req.getParameter("update") != null) {
                reservationService.update(reservationBean);
            }
            if (req.getParameter("delete") != null) {
                reservationService.delete(reservationBean);
            }
        }
        List<ReservationBean> reservationBeanList = reservationService.getAllFlightsReservation();
        req.setAttribute("reservations", reservationBeanList);
        return null;
    }
}
