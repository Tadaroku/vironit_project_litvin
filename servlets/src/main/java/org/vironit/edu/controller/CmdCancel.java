package org.vironit.edu.controller;

import org.vironit.edu.bean.ReservationBean;
import org.vironit.edu.service.ReservationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdCancel extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        ReservationBean reservationBean = new ReservationBean();
        ReservationService reservationService = new ReservationService();
        int reservation_id = Integer.valueOf(req.getParameter("reservation_id"));
        reservationBean.setId(reservation_id);
        reservationService.unblockReservation(reservation_id);
        return null;
    }
}
