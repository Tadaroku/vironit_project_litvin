package org.vironit.edu.controller;

import org.vironit.edu.bean.AirlineBean;
import org.vironit.edu.bean.FlightBean;
import org.vironit.edu.bean.ReservationBean;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class CmdAddReservations extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {
            Boolean luggage = Boolean.valueOf(req.getParameter("luggage"));
            Double price = Double.valueOf(req.getParameter("price"));
            String seat = req.getParameter("seat");
            LocalDate reservationDate = LocalDate.parse(req.getParameter("reservationDate"));
            Boolean status = Boolean.valueOf(req.getParameter("status"));
//            int userId = Integer.parseInt(req.getParameter("userId"));
            int flightId = Integer.parseInt(req.getParameter("flightId"));
            int airlineId = Integer.parseInt(req.getParameter("airlineId"));


//            UserBean userBean = new UserBean();
//            userBean.setId(userId);

            FlightBean flightBean = new FlightBean();
            flightBean.setId(flightId);

            AirlineBean airlineBean = new AirlineBean();
            airlineBean.setId(airlineId);

            ReservationBean reservationBean = new ReservationBean(0, luggage, price, seat, reservationDate, status, flightBean, airlineBean);
            ReservationService reservationService = new ReservationService();
            reservationService.saveWOUser(reservationBean);
            return Action.EDITRESERVATIONS.cmd;
        }
        return null;
    }
}
