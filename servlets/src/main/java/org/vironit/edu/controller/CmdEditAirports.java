package org.vironit.edu.controller;

import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdEditAirports extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        AirportService airportService = new AirportService();
        UserBean admin = Util.getUserBean(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            int id = Integer.parseInt(Form.getString(req, "id"));
            String name = Form.getString(req, "name");
            String code = Form.getString(req, "code");
            String location = Form.getString(req, "location");

            AirportBean airport = new AirportBean(id, name, code, location);
            if (req.getParameter("update") != null) {
                airportService.updateAirportBean(airport);
            }
            if (req.getParameter("delete") != null) {
                airportService.deleteAirportBean(airport);
            }

        }
        req.getServletContext().setAttribute("airports", airportService.findAllAirports());
        return null;
    }
}
