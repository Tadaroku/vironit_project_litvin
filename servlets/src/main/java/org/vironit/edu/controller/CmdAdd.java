package org.vironit.edu.controller;

import org.vironit.edu.bean.ReservationBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.service.ReservationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdAdd extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        ReservationBean reservationBean = new ReservationBean();
        UserBean userBean;
        ReservationService reservationService = new ReservationService();
        userBean = (UserBean) req.getSession().getAttribute("user");
        int reservation_id = Integer.valueOf(req.getParameter("reservation_id"));
        String where = String.format(" where id='%s'", reservation_id);
        reservationService.getAllRes(where);

        reservationBean.setId(reservation_id);
        reservationBean.setUser(userBean);
        reservationService.up(reservationBean);
        reservationService.blockReservation(reservation_id);
        return null;
    }
}
