package org.vironit.edu.controller;

import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.bean.FlightBean;
import org.vironit.edu.dao.impl.FlightDaoImpl;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

public class CmdAddFlights extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {
            LocalDateTime departureTime = LocalDateTime.parse(req.getParameter("departureTime"));
            LocalDateTime arrivalTime = LocalDateTime.parse(req.getParameter("arrivalTime"));
            String airplaneName = Form.getString(req, "airplaneName");
            String flightNumber = Form.getString(req, "flightNumber");

            Integer airportFromId = Form.getInt(req, "airportFromId");
            AirportBean airportBeanFrom = new AirportBean();
            airportBeanFrom.setId(airportFromId);

            Integer airportToId = Form.getInt(req, "airportToId");

            AirportBean airportBeanTo = new AirportBean();
            airportBeanTo.setId(airportToId);

            FlightBean flightBean = new FlightBean(0, departureTime, arrivalTime, airplaneName, flightNumber, airportBeanFrom, airportBeanTo);
            FlightDaoImpl flightDao = new FlightDaoImpl();
            flightDao.saveFlight(flightBean);
            return Action.EDITFLIGHTS.cmd;
        }
        return null;
    }
}
