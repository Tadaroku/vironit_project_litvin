package org.vironit.edu.controller;

import org.vironit.edu.bean.RoleBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.pattern.Patterns;
import org.vironit.edu.service.UserService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.text.ParseException;

public class CmdSignup extends Cmd {

    @Override
    public Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws SQLException, ParseException {
        if (Form.isPost(req)) {
            String email = Form.getString(req, "email", Patterns.EMAIL);
            String password = Form.getString(req, "password", Patterns.PASSWORD);
            String name = Form.getString(req, "name", Patterns.NAME_SURNAME);
            String surname = Form.getString(req, "surname", Patterns.NAME_SURNAME);
            String phone = Form.getString(req, "phone", Patterns.PHONE);
            String repeatPassword = Form.getString(req, "repeatPassword", Patterns.PASSWORD);

            boolean valid = true;
            String where = String.format(" where `email`='%s'",email);
            UserService userService = new UserService();
//
//            Pattern pattern = Pattern.compile("^(.+)@(.+)$");
//            Matcher matcher = pattern.matcher(email);
//            if (!matcher.find()) {
//                req.setAttribute("error", "invalid");
//                valid = false;
//            }
//
//            if (!valid) {
//                return Action.SIGNUP.cmd;
//            }
//
//
//            Pattern p = Pattern.compile("[a-zа-яА-ЯёЁ]{2,}");
//            Matcher m = p.matcher(surname);
//            if (!m.find()) {
//                req.setAttribute("error", "surname");
//                valid = false;
//            }
//
//            if (!valid) {
//                return Action.SIGNUP.cmd;
//            }
//
//            m = p.matcher(name);
//            if (!m.find()) {
//                req.setAttribute("error", "name");
//                valid = false;
//            }
//
//            if (!valid) {
//                return Action.SIGNUP.cmd;
//            }
//
//            Pattern p1 = Pattern.compile("^\\+375 \\((17|29|33|44)\\) [0-9]{3}-[0-9]{2}-[0-9]{2}$");
//            Matcher m1 = p1.matcher(phone);
//            if (!m1.find()) {
//                req.setAttribute("error", "phone");
//                valid = false;
//            }
//
//            if (!valid) {
//                return Action.SIGNUP.cmd;
//            }
//
//            Pattern p2 = Pattern.compile("[a-zA-Z0-9а-яА-ЯёЁ]{4,}");
//            Matcher m2 = p2.matcher(password);
//            if (!m2.find()) {
//                req.setAttribute("error", "password");
//                valid = false;
//            }
//
//            if (!valid) {
//                return Action.SIGNUP.cmd;
//            }
//
            if (!password.equals(repeatPassword)) {
                req.setAttribute("error", "password");
                valid = false;
            }

            if (!valid) {
                return Action.SIGNUP.cmd;
            }


            if (userService.getAllUsersWhere(where).size()>0){
                req.setAttribute("error","error");
                return Action.SIGNUP.cmd;
            }

            RoleBean roleBean = new RoleBean();
            roleBean.setId(2);
            UserBean user = new UserBean(0, roleBean, email, password, name, surname, phone);
            userService.save(user);
            return Action.LOGIN.cmd;
        }
        return null;
    }
}
