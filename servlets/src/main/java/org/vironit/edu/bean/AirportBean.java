package org.vironit.edu.bean;

public class AirportBean {
    private int id;
    private String name;
    private String code;
    private String location;

    public AirportBean() {
    }

    public AirportBean(int id, String name, String code, String location) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.location = location;
    }

    public AirportBean(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
