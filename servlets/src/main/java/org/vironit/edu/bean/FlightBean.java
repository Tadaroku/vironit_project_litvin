package org.vironit.edu.bean;

import java.time.LocalDateTime;

public class FlightBean {
    private int id;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;
    private String airplane;
    private String flightNumber;
    private AirportBean airportFrom;
    private AirportBean airportTo;

    public FlightBean() {
    }

    public FlightBean(int id, LocalDateTime departureTime, LocalDateTime arrivalTime, String airplane, String flightNumber, AirportBean airportFrom, AirportBean airportTo) {
        this.id = id;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.airplane = airplane;
        this.flightNumber = flightNumber;
        this.airportFrom = airportFrom;
        this.airportTo = airportTo;
    }

    public FlightBean(int id, LocalDateTime departureTime, LocalDateTime arrivalTime, String airplane, String flightNumber) {
        this.id = id;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.airplane = airplane;
        this.flightNumber = flightNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getAirplane() {
        return airplane;
    }

    public void setAirplane(String airplane) {
        this.airplane = airplane;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public AirportBean getAirportFrom() {
        return airportFrom;
    }

    public void setAirportFrom(AirportBean airportFrom) {
        this.airportFrom = airportFrom;
    }

    public AirportBean getAirportTo() {
        return airportTo;
    }

    public void setAirportTo(AirportBean airportTo) {
        this.airportTo = airportTo;
    }
}
