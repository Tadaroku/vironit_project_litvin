package org.vironit.edu.bean;

import java.time.LocalDate;

public class ReservationBean {
    private int id;
    private boolean luggage;
    private double price;
    private String seat;
    private LocalDate reservationDate;
    private boolean status;
    private UserBean user;
    private FlightBean flight;
    private AirlineBean airline;

    public ReservationBean() {
    }

    public ReservationBean(int id, boolean luggage, double price, String seat, LocalDate reservationDate, boolean status, UserBean user, FlightBean flight, AirlineBean airline) {
        this.id = id;
        this.luggage = luggage;
        this.price = price;
        this.seat = seat;
        this.reservationDate = reservationDate;
        this.status = status;
        this.user = user;
        this.flight = flight;
        this.airline = airline;
    }

    public ReservationBean(int id, boolean luggage, double price, String seat, LocalDate reservationDate, boolean status, FlightBean flight, AirlineBean airline) {
        this.id = id;
        this.luggage = luggage;
        this.price = price;
        this.seat = seat;
        this.reservationDate = reservationDate;
        this.status = status;
        this.flight = flight;
        this.airline = airline;
    }


    public ReservationBean(int id, boolean luggage, double price, String seat, LocalDate reservationDate, boolean status) {
        this.id = id;
        this.luggage = luggage;
        this.price = price;
        this.seat = seat;
        this.reservationDate = reservationDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public FlightBean getFlight() {
        return flight;
    }

    public void setFlight(FlightBean flight) {
        this.flight = flight;
    }

    public AirlineBean getAirline() {
        return airline;
    }

    public void setAirline(AirlineBean airline) {
        this.airline = airline;
    }
}
