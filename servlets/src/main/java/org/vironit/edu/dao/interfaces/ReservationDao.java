package org.vironit.edu.dao.interfaces;

import org.vironit.edu.model.Reservation;

public interface ReservationDao extends BaseDao<Reservation, Integer> {
}
