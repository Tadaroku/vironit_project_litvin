package org.vironit.edu.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.dao.interfaces.AirportDao;
import org.vironit.edu.model.Airport;
import org.vironit.edu.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("Duplicates")
public class AirportDaoImpl implements AirportDao {

    private static final Logger logger = LoggerFactory.getLogger(AirportDaoImpl.class);

    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;


    @Override
    public Optional<Airport> findByAirportName(String airportName) {
        Airport airport = new Airport();
        String sql = "select * from vironit_db_servlets.airports where airports.airport_name = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, airport.getName());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airport.setId(resultSet.getInt("id"));
                airport.setName(resultSet.getString("airport_name"));
                airport.setCode(resultSet.getString("code"));
                airport.setLocation(resultSet.getString("location"));

            }
        } catch (Exception e) {
            logger.error("failed to find airport name: " + airportName);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return Optional.of(airport);
    }

    public Optional<AirportBean> findByAirportBeanName(String airportName) {
        AirportBean airport = new AirportBean();
        String sql = "select * from vironit_db_servlets.airports where airports.airport_name = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, airport.getName());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airport.setId(resultSet.getInt("id"));
                airport.setName(resultSet.getString("airport_name"));
                airport.setCode(resultSet.getString("code"));
                airport.setLocation(resultSet.getString("location"));

            }
        } catch (Exception e) {
            logger.error("failed to find airport name: " + airportName);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return Optional.of(airport);
    }

    @Override
    public Integer save(Airport airport) {
        String sql = "insert into vironit_db_servlets.airports (airport_name,code,location)" +
                " values (?,?,?);";
        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, airport.getName());
            preparedStatement.setString(2, airport.getCode());
            preparedStatement.setString(3, airport.getLocation());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            logger.error("failed to save airport: " + airport);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }

    public Integer saveAirport(AirportBean airport) {
        String sql = "insert into vironit_db_servlets.airports (airport_name,code,location)" +
                " values (?,?,?);";
        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, airport.getName());
            preparedStatement.setString(2, airport.getCode());
            preparedStatement.setString(3, airport.getLocation());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            logger.error("failed to save airport: " + airport);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }

    @Override
    public List<Airport> getAll() {
        String sql = "select * from vironit_db_servlets.airports;";
        Airport airport;
        List<Airport> airports = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airport = new Airport();
                airport.setId(resultSet.getInt("id"));
                airport.setName(resultSet.getString("airport_name"));
                airport.setLocation(resultSet.getString("location"));
                airport.setCode(resultSet.getString("code"));
                airports.add(airport);

            }
        } catch (Exception e) {
            logger.error("failed to get airports:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return airports;
    }

    public List<AirportBean> getAllAirports() {
        String sql = "select * from vironit_db_servlets.airports;";
        AirportBean airport;
        List<AirportBean> airports = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airport = new AirportBean();
                airport.setId(resultSet.getInt("id"));
                airport.setName(resultSet.getString("airport_name"));
                airport.setLocation(resultSet.getString("location"));
                airport.setCode(resultSet.getString("code"));
                airports.add(airport);

            }
        } catch (Exception e) {
            logger.error("failed to get airports:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return airports;
    }

    @Override
    public void update(Airport airport) {
        String sql = "update vironit_db_servlets.airports set airport_name=?, location=?, code=?" +
                " where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, airport.getId());
            preparedStatement.setString(2, airport.getName());
            preparedStatement.setString(3, airport.getLocation());
            preparedStatement.setString(4, airport.getCode());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to update airport: " + airport);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    public void updateAirport(AirportBean airport) {
        String sql = "update vironit_db_servlets.airports set airport_name=?, location=?, code=?" +
                " where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, airport.getName());
            preparedStatement.setString(2, airport.getLocation());
            preparedStatement.setString(3, airport.getCode());
            preparedStatement.setInt(4, airport.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to update airport: " + airport);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }


    @SuppressWarnings("Duplicates")
    @Override
    public void delete(Integer id) {
        String sql = "delete from vironit_db_servlets.airports where airports.id = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            logger.error("failed to delete airport by id: " + id);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    public Airport find(Airport airport) {
        Airport airportEntity = null;
        String sql = "select * from vironit_db_servlets.airports where airports.airport_name = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, airport.getName());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airportEntity = new Airport();
                airportEntity.setId(resultSet.getInt("id"));
                airportEntity.setName(resultSet.getString("airport_name"));
                airportEntity.setCode(resultSet.getString("code"));
                airportEntity.setLocation(resultSet.getString("location"));

            }
        } catch (Exception e) {
            logger.error("failed to find airport name: " + airport);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return airportEntity;
    }


    public AirportBean findById(int id) {
        String sql = "select * from vironit_db_servlets.airports where airports.id = ?;";
        AirportBean airport = null;

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airport = new AirportBean();
                airport.setId(resultSet.getInt("id"));
                airport.setName(resultSet.getString("airport_name"));
                airport.setCode(resultSet.getString("code"));
                airport.setLocation(resultSet.getString("location"));

            }
        } catch (Exception e) {
            logger.error("failed to find airport by id: " + id);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return airport;
    }

    public void deleteAirport(AirportBean airport) {
        String sql = String.format("delete from `airports` where `id` = %d", airport.getId());

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to delete user by id: " + airport);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }
}
