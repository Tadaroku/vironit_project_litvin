package org.vironit.edu.dao.interfaces;

import org.vironit.edu.model.Role;

public interface RoleDao extends BaseDao<Role, Integer> {
}
