package org.vironit.edu.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vironit.edu.bean.AirlineBean;
import org.vironit.edu.dao.interfaces.AirlineDao;
import org.vironit.edu.model.Airline;
import org.vironit.edu.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("Duplicates")
public class AirlineDaoImpl implements AirlineDao {

    private static final Logger logger = LoggerFactory.getLogger(AirlineDaoImpl.class);

    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    @Override
    public Optional<Airline> findByAirlineName(String airlineName) {
        Airline airline = new Airline();
        String sql = "select * from vironit_db_servlets.airlines where airlines.airline_name = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, airline.getName());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airline.setId(resultSet.getInt("id"));
                airline.setName(resultSet.getString("name"));

            }
        } catch (Exception e) {
            logger.error("failed to find airline name: " + airlineName);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return Optional.of(airline);
    }

    public Optional<AirlineBean> findByAirlineBeanName(String airlineName) {
        AirlineBean airline = new AirlineBean();
        String sql = "select * from vironit_db_servlets.airlines where airlines.airline_name = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, airline.getName());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airline.setId(resultSet.getInt("id"));
                airline.setName(resultSet.getString("name"));

            }
        } catch (Exception e) {
            logger.error("failed to find airline name: " + airlineName);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return Optional.of(airline);
    }

    @Override
    public Integer save(Airline airline) {
        String sql = "insert into vironit_db_servlets.airlines (airline_name)" +
                " values (?);";
        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, airline.getName());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }

        } catch (Exception e) {
            logger.error("failed to save airline: " + airline);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }

    public Integer saveAirline(AirlineBean airline) {
        String sql = "insert into vironit_db_servlets.airlines (airline_name)" +
                " values (?);";
        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, airline.getName());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }

        } catch (Exception e) {
            logger.error("failed to save airline: " + airline);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }


    @Override
    public List<Airline> getAll() {
        String sql = "select * from vironit_db_servlets.airlines;";
        Airline airline;
        List<Airline> airlines = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airline = new Airline();
                airline.setId(resultSet.getInt("id"));
                airline.setName(resultSet.getString("airline_name"));
                airlines.add(airline);
            }
        } catch (Exception e) {
            logger.error("failed to get airlines:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return airlines;
    }

    public List<AirlineBean> getAllAirlines() {
        String sql = "select * from vironit_db_servlets.airlines;";
        AirlineBean airline;
        List<AirlineBean> airlines = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                airline = new AirlineBean();
                airline.setId(resultSet.getInt("id"));
                airline.setName(resultSet.getString("airline_name"));
                airlines.add(airline);
            }
        } catch (Exception e) {
            logger.error("failed to get airlines:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return airlines;
    }

    @Override
    public void update(Airline airline) {
        String sql = "update vironit_db_servlets.airlines set airline_name=?" +
                " where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, airline.getName());
            preparedStatement.setInt(2, airline.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to update airline: " + airline);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    public void updateAirline(AirlineBean airline) {
        String sql = "update vironit_db_servlets.airlines set airline_name=?" +
                " where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, airline.getName());
            preparedStatement.setInt(2, airline.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to update airline: " + airline);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }


    @Override
    public void delete(Integer id) {
        {
            String sql = "delete from vironit_db_servlets.airlines where airlines.id = ?;";

            try {
                connection = ConnectionUtil.getDataSource().getConnection();
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();

            } catch (Exception e) {
                logger.error("failed to delete airline by id: " + id);
            } finally {
                ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
            }
        }

    }

    public AirlineBean findById(int id) {
        AirlineBean airline = null;
        String sql = "select * from vironit_db_servlets.airlines where airlines.id = ?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                airline = new AirlineBean();
                airline.setId(resultSet.getInt("id"));
                airline.setName(resultSet.getString("airline_name"));
            }
        } catch (Exception e) {
            System.err.println(e);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return airline;
    }

    public void deleteAirline(AirlineBean airline) {
        String sql = String.format("delete from `airlines` where `id` = %d", airline.getId());

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to delete user by id: " + airline);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }
}
