package org.vironit.edu.dao.interfaces;

import org.vironit.edu.model.Flight;

import java.util.Optional;

public interface FlightDao extends BaseDao<Flight, Integer> {
    Optional<Flight> findByFlightNumber(String flightNumber);
}
