package org.vironit.edu.dao.interfaces;

import org.vironit.edu.model.Airline;

import java.util.Optional;

public interface AirlineDao extends BaseDao<Airline, Integer> {
    Optional<Airline> findByAirlineName(String airlineName);

}
