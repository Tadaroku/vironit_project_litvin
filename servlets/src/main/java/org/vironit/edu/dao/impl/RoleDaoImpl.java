package org.vironit.edu.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vironit.edu.bean.RoleBean;
import org.vironit.edu.dao.interfaces.RoleDao;
import org.vironit.edu.model.Role;
import org.vironit.edu.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class RoleDaoImpl implements RoleDao {

    private static final Logger logger = LoggerFactory.getLogger(RoleDaoImpl.class);


    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;


    @SuppressWarnings("Duplicates")
    @Override
    public Integer save(Role role) {
        String sql = "insert into vironit_db.roles (role) values (?);";
        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, role.getRole());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            logger.error("failed to save role: " + role);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }

    @Override
    public List<Role> getAll() {
        String sql = "select * from vironit_db_servlets.roles;";
        Role role;
        List<Role> roles = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                role = new Role();
                role.setId(resultSet.getInt("id"));
                role.setRole(resultSet.getString("role"));
            }
        } catch (Exception e) {
            logger.error("failed to get roles: ");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return roles;
    }

    @Override
    public void update(Role role) {
        String sql = "update vironit_db_servlets.roles set role=? where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, role.getId());
            preparedStatement.setString(2, role.getRole());
            preparedStatement.executeUpdate();


        } catch (Exception e) {
            logger.error("failed to update role: " + role);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    public void updateRole(RoleBean role) {
        String sql = "update vironit_db_servlets.roles set role=? where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, role.getId());
            preparedStatement.setString(2, role.getRole());
            preparedStatement.executeUpdate();


        } catch (Exception e) {
            logger.error("failed to update role: " + role);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void delete(Integer id) {

        String sql = "delete from vironit_db_servlets.roles where users.id = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to delete role by id: " + id);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }
}
