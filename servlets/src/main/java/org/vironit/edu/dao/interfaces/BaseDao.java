package org.vironit.edu.dao.interfaces;

import java.util.List;

public interface BaseDao<T, ID> {


    ID save(T entity);

    List<T> getAll();

    void update(T entity);

    void delete(ID id);
}
