package org.vironit.edu.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.bean.FlightBean;
import org.vironit.edu.dao.interfaces.FlightDao;
import org.vironit.edu.model.Flight;
import org.vironit.edu.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("Duplicates")
public class FlightDaoImpl implements FlightDao {

    private static final Logger logger = LoggerFactory.getLogger(FlightDaoImpl.class);

    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    @Override
    public Optional<Flight> findByFlightNumber(String flightNumber) {
        Flight flight = new Flight();
        String sql = "select * from vironit_db_servlets.flights where flights.flight_number = ?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, flight.getFlightNumber());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                flight.setId(resultSet.getInt("id"));
                flight.setAirplane(resultSet.getString("airplane_name"));
                flight.setFlightNumber(resultSet.getString("flight_number"));
                flight.setAirportFromId(resultSet.getInt("airport_from_id"));
                flight.setAirportToId(resultSet.getInt("airport_to_id"));
                flight.setDepartureTime(resultSet.getTimestamp("departure_time").toLocalDateTime());
                flight.setArrivalTime(resultSet.getTimestamp("arrival_time").toLocalDateTime());

            }

        } catch (Exception e) {
            logger.error("failed to find flight by flight number: " + flightNumber);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return Optional.of(flight);
    }

    public Optional<FlightBean> findByFlightBeanNumber(String flightNumber) {
        FlightBean flight = new FlightBean();
        AirportBean airportFrom = new AirportBean();
        AirportBean airportTo = new AirportBean();
        String sql = "select f.id,f.departure_time,f.arrival_time, f.airplane_name, f.flight_number, a.id as \"from_id\", a1.id as \"to_id\", a.airport_name as \"from\",a.location as \"location_from\",a.code as \"code_from\", a1.airport_name as \"to\",  a1.location as \"location_to\", a1.code as \"code_to\" from flights as f " +
                "join airports as a on a.id=f.airport_from_id " +
                "join airports as a1 on a1.id=f.airport_to_id where f.flight_number=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, flight.getFlightNumber());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                flight.setId(resultSet.getInt("id"));
                flight.setAirplane(resultSet.getString("airplane_name"));
                flight.setFlightNumber(resultSet.getString("flight_number"));
                flight.setDepartureTime(resultSet.getTimestamp("departure_time").toLocalDateTime());
                flight.setArrivalTime(resultSet.getTimestamp("arrival_time").toLocalDateTime());

                airportFrom.setId(resultSet.getInt("from_id"));
                airportFrom.setLocation(resultSet.getString("location_from"));
                airportFrom.setName(resultSet.getString("from"));
                airportFrom.setCode(resultSet.getString("code_from"));

                airportTo.setId(resultSet.getInt("to_id"));
                airportTo.setLocation(resultSet.getString("location_to"));
                airportTo.setName(resultSet.getString("to"));
                airportTo.setCode(resultSet.getString("code_to"));

                flight.setAirportFrom(airportFrom);
                flight.setAirportTo(airportTo);

            }

        } catch (Exception e) {
            logger.error("failed to find flight by flight number: " + flightNumber);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return Optional.of(flight);
    }

    @Override
    public Integer save(Flight flight) {
        String sql = "insert into vironit_db_servlets.flights (departure_time,arrival_time,airport_from_id," +
                "airport_to_id,airplane_name,flight_number)" +
                " values (?,?,?,?,?,?); ";

        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setTimestamp(1, Timestamp.valueOf(flight.getDepartureTime()));
            preparedStatement.setTimestamp(2, Timestamp.valueOf(flight.getArrivalTime()));
            preparedStatement.setInt(3, flight.getAirportFromId());
            preparedStatement.setInt(4, flight.getAirportToId());
            preparedStatement.setString(5, flight.getAirplane());
            preparedStatement.setString(6, flight.getFlightNumber());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            logger.error("failed to save flight: " + flight);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }

    public Integer saveFlight(FlightBean flight) {
        String sql = "insert into vironit_db_servlets.flights (departure_time,arrival_time," +
                "airplane_name,flight_number,airport_from_id,airport_to_id)" +
                " values (?,?,?,?,?,?); ";

        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setTimestamp(1, Timestamp.valueOf(flight.getDepartureTime()));
            preparedStatement.setTimestamp(2, Timestamp.valueOf(flight.getArrivalTime()));
            preparedStatement.setString(3, flight.getAirplane());
            preparedStatement.setString(4, flight.getFlightNumber());
            preparedStatement.setInt(5, flight.getAirportFrom().getId());
            preparedStatement.setInt(6, flight.getAirportTo().getId());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            logger.error("failed to save flight: " + flight);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }


    @Override
    public List<Flight> getAll() {
        String sql = "select * from vironit_db_servlets.flights;";
        Flight flight;
        List<Flight> flights = new ArrayList<>();

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                flight = new Flight();
                flight.setId(resultSet.getInt("id"));
                flight.setAirplane(resultSet.getString("airplane_name"));
                flight.setFlightNumber(resultSet.getString("flight_number"));
                flight.setDepartureTime(resultSet.getTimestamp("departure_time").toLocalDateTime());
                flight.setArrivalTime(resultSet.getTimestamp("arrival_time").toLocalDateTime());
                flight.setAirportFromId(resultSet.getInt("airport_from_id"));
                flight.setAirportToId(resultSet.getInt("airport_to_id"));
                flights.add(flight);
            }
        } catch (Exception e) {
            logger.error("failed to get flights:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return flights;

    }

    public List<FlightBean> getAllFlights() {
        String sql = "select f.id,f.departure_time,f.arrival_time, f.airplane_name, f.flight_number, a.id as \"from_id\", a1.id as \"to_id\", a.airport_name as \"from\",a.location as \"location_from\",a.code as \"code_from\", a1.airport_name as \"to\",  a1.location as \"location_to\", a1.code as \"code_to\" from flights as f " +
                "join airports as a on a.id=f.airport_from_id " +
                "join airports as a1 on a1.id=f.airport_to_id;";
        FlightBean flight;
        AirportBean airportFrom;
        AirportBean airportTo;
        List<FlightBean> flights = new ArrayList<>();

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                flight = new FlightBean();
                airportFrom = new AirportBean();
                airportTo = new AirportBean();
                flight.setId(resultSet.getInt("id"));
                flight.setAirplane(resultSet.getString("airplane_name"));
                flight.setFlightNumber(resultSet.getString("flight_number"));
                flight.setDepartureTime(resultSet.getTimestamp("departure_time").toLocalDateTime());
                flight.setArrivalTime(resultSet.getTimestamp("arrival_time").toLocalDateTime());

                airportFrom.setId(resultSet.getInt("from_id"));
                airportFrom.setLocation(resultSet.getString("location_from"));
                airportFrom.setName(resultSet.getString("from"));
                airportFrom.setCode(resultSet.getString("code_from"));

                airportTo.setId(resultSet.getInt("to_id"));
                airportTo.setLocation(resultSet.getString("location_to"));
                airportTo.setName(resultSet.getString("to"));
                airportTo.setCode(resultSet.getString("code_to"));

                flight.setAirportFrom(airportFrom);
                flight.setAirportTo(airportTo);

                flights.add(flight);
            }
        } catch (Exception e) {
            logger.error("failed to get flights:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return flights;

    }

    @Override
    public void update(Flight flight) {
        String sql = "update vironit_db_servlets.flights set airplane_name=?, flight_number=?, departure_time=?, arrival_time=? where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, flight.getAirplane());
            preparedStatement.setString(2, flight.getFlightNumber());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(flight.getDepartureTime()));
            preparedStatement.setTimestamp(4, Timestamp.valueOf((flight.getArrivalTime())));
            preparedStatement.setInt(5, flight.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to update flight: " + flight);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    public void updateFlight(FlightBean flight) {
        String sql = "update vironit_db_servlets.flights set departure_time=?, arrival_time=?, airplane_name=?, flight_number=? where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setTimestamp(1, Timestamp.valueOf(flight.getDepartureTime()));
            preparedStatement.setTimestamp(2, Timestamp.valueOf((flight.getArrivalTime())));
            preparedStatement.setString(3, flight.getAirplane());
            preparedStatement.setString(4, flight.getFlightNumber());
            preparedStatement.setInt(5, flight.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to update flight: " + flight);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void delete(Integer id) {
        String sql = "delete from vironit_db_servlets.flights where flights.id = ?;";

        try {

            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            logger.error("failed to delete flight by id: " + id);
        } finally {

            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }


    @SuppressWarnings("Duplicates")
    public List<FlightBean> findFlightInfo() {
        String sql = "select f.id,f.departure_time,f.arrival_time, f.airplane_name, f.flight_number, a.id as \"from_id\", a1.id as \"to_id\", a.airport_name as \"from\",a.location as \"location_from\", a1.airport_name as \"to\",  a1.location as \"location_to\" from flights as f " +
                "join airports as a on a.id=f.airport_from_id " +
                "join airports as a1 on a1.id=f.airport_to_id;";

        FlightBean flightBean;
        List<FlightBean> flights = new ArrayList<>();

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                flightBean = new FlightBean();
                AirportBean airportBeanFrom = new AirportBean();
                AirportBean airportBeanTo = new AirportBean();
                flightBean.setId(resultSet.getInt("id"));
                flightBean.setDepartureTime(resultSet.getTimestamp("departure_time").toLocalDateTime());
                flightBean.setArrivalTime(resultSet.getTimestamp("arrival_time").toLocalDateTime());
                flightBean.setAirplane(resultSet.getString("airplane_name"));
                flightBean.setFlightNumber(resultSet.getString("flight_number"));

                airportBeanFrom.setId(resultSet.getInt("from_id"));
                airportBeanFrom.setName(resultSet.getString("from"));
                airportBeanFrom.setLocation(resultSet.getString("location_from"));
                flightBean.setAirportFrom(airportBeanFrom);

                airportBeanTo.setId(resultSet.getInt("to_id"));
                airportBeanTo.setName(resultSet.getString("to"));
                airportBeanTo.setLocation(resultSet.getString("location_to"));
                flightBean.setAirportTo(airportBeanTo);
                flights.add(flightBean);
            }
        } catch (Exception e) {
            logger.error("failed to get all flight info: ");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return flights;

    }

    public void deleteFlight(FlightBean flight) {
        String sql = String.format("delete from `flights` where `id` = %d", flight.getId());

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to delete user by id: " + flight);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

}
