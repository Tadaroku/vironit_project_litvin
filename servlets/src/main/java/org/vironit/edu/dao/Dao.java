package org.vironit.edu.dao;

import org.vironit.edu.dao.impl.*;

public class Dao {
    private static Dao instance;

    private UserDaoImpl userDao;
    private RoleDaoImpl roleDao;
    private ReservationDaoImpl reservationDao;
    private FlightDaoImpl flightDao;
    private AirportDaoImpl airportDao;
    private AirlineDaoImpl airlineDao;


    public UserDaoImpl getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDaoImpl userDao) {
        this.userDao = userDao;
    }

    public RoleDaoImpl getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(RoleDaoImpl roleDao) {
        this.roleDao = roleDao;
    }

    public ReservationDaoImpl getReservationDao() {
        return reservationDao;
    }

    public void setReservationDao(ReservationDaoImpl reservationDao) {
        this.reservationDao = reservationDao;
    }

    public FlightDaoImpl getFlightDao() {
        return flightDao;
    }

    public void setFlightDao(FlightDaoImpl flightDao) {
        this.flightDao = flightDao;
    }

    public AirportDaoImpl getAirportDao() {
        return airportDao;
    }

    public void setAirportDao(AirportDaoImpl airportDao) {
        this.airportDao = airportDao;
    }

    public AirlineDaoImpl getAirlineDao() {
        return airlineDao;
    }

    public void setAirlineDao(AirlineDaoImpl airlineDao) {
        this.airlineDao = airlineDao;
    }

    private Dao() {

    }

    public static Dao getDao() {
        if (instance == null) {
            synchronized (Dao.class) {
                if (instance == null) {
                    instance = new Dao();
                    instance.userDao = new UserDaoImpl();
                    instance.roleDao = new RoleDaoImpl();
                    instance.reservationDao = new ReservationDaoImpl();
                    instance.flightDao = new FlightDaoImpl();
                    instance.airportDao = new AirportDaoImpl();
                    instance.airlineDao = new AirlineDaoImpl();


                }
            }
        }
        return instance;
    }
}
