package org.vironit.edu.dao.interfaces;

import org.vironit.edu.model.Airport;

import java.util.Optional;

public interface AirportDao extends BaseDao<Airport, Integer>  {
    Optional<Airport> findByAirportName(String airportName);

}
