package org.vironit.edu.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vironit.edu.bean.RoleBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.dao.interfaces.UserDao;
import org.vironit.edu.model.User;
import org.vironit.edu.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("Duplicates")
public class UserDaoImpl implements UserDao {

    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    @Override
    public Optional<User> findById(Long userId) {
        String sql = "select * from vironit_db_servlets.users where users.id = ?;";
        User user = null;

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, Math.toIntExact(userId));
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getLong("id"));
                user.setRolesId(resultSet.getInt("roles_id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setPhone(resultSet.getString("phone"));

            }
        } catch (Exception e) {
            logger.error("failed to find user by id: " + userId);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return Optional.ofNullable(user);
    }

    public Optional<UserBean> findUserById(Long userId) {
        String sql = "select u.id, u.email, u.password, u.name, u.surname, u.phone, r.id, r.role from vironit_db_servlets.users as u join roles r on u.roles_id = r.id where u.id = ?;";
        UserBean user = null;
        RoleBean role = null;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, Math.toIntExact(userId));
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new UserBean();
                role = new RoleBean();
                role.setId(resultSet.getInt("id"));
                role.setRole(resultSet.getString("role"));

                user.setId(resultSet.getLong("id"));
                user.setRole(role);
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setPhone(resultSet.getString("phone"));

            }
        } catch (Exception e) {
            logger.error("failed to find user by id: " + userId);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return Optional.ofNullable(user);
    }


    @Override
    public Long save(User user) {
        String sql = "insert into vironit_db_servlets.users (name,surname,email,password,phone,roles_id)" +
                " values (?,?,?,?,?,?);";
        long id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
//            preparedStatement.setLong(1,user.getId());
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setInt(6, user.getRolesId());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getLong(1);
            }
        } catch (Exception e) {
            logger.error("failed to save user: " + user);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }

    public Long saveUser(UserBean user) {
        String sql = "insert into vironit_db_servlets.users (name,surname,email,password,phone,roles_id)" +
                " values (?,?,?,?,?,?);";
        long id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
//            preparedStatement.setLong(1,user.getId());
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setObject(6, user.getRole().getId());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getLong(1);
            }
        } catch (Exception e) {
            logger.error("failed to save user: " + user);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return id;
    }

    @Override
    public List<User> getAll() {
        String sql = "select * from vironit_db_servlets.users;";
        User user;
        List<User> users = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getLong("id"));
                user.setEmail(resultSet.getString("email"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setPhone(resultSet.getString("phone"));
                user.setRolesId(resultSet.getInt("roles_id"));
                users.add(user);
            }
        } catch (Exception e) {
            logger.error("failed to get users:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return users;
    }

    public List<UserBean> getAll(String where) {
        String sql = "select u.id, u.email, u.password, u.name, u.surname, u.phone, r.id, r.role from vironit_db_servlets.users as u join roles r on u.roles_id = r.id" + where + ";";
        UserBean user;
        RoleBean roleBean;
        List<UserBean> users = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new UserBean();
                roleBean = new RoleBean();
                user.setId(resultSet.getLong("id"));
                user.setEmail(resultSet.getString("email"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setPhone(resultSet.getString("phone"));

                roleBean.setId(resultSet.getInt("id"));
                roleBean.setRole(resultSet.getString("role"));

                user.setRole(roleBean);

                users.add(user);
            }
        } catch (Exception e) {
            logger.error("failed to get users:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return users;
    }

    public List<UserBean> getAllUsers() {
        String sql = "select u.id, u.email, u.password, u.name, u.surname, u.phone, r.id, r.role from vironit_db_servlets.users as u join roles r on u.roles_id = r.id";
        UserBean user;
        RoleBean roleBean;
        List<UserBean> users = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new UserBean();
                roleBean = new RoleBean();

                roleBean.setId(resultSet.getInt("id"));
                roleBean.setRole(resultSet.getString("role"));

                user.setId(resultSet.getLong("id"));
                user.setEmail(resultSet.getString("email"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setPhone(resultSet.getString("phone"));
                user.setRole(roleBean);
                users.add(user);
            }
        } catch (Exception e) {
            logger.error("failed to get users:");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return users;
    }

    @Override
    public void update(User user) {
        String sql = "update vironit_db_servlets.users set email=?, password=?, name=?, surname=?, phone=?" +
                " where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getName());
            preparedStatement.setString(4, user.getSurname());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setLong(6, user.getId());
            preparedStatement.executeUpdate();


        } catch (Exception e) {
            logger.error("failed to update user: " + user);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    public void updateUser(UserBean user) {
        String sql = "update vironit_db_servlets.users set email=?, password=?, name=?, surname=?, phone=?" +
                " where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getName());
            preparedStatement.setString(4, user.getSurname());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setLong(6, user.getId());
            preparedStatement.executeUpdate();


        } catch (Exception e) {
            logger.error("failed to update user: " + user);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    public void updateUserWOPassword(UserBean user) {
        String sql = "update vironit_db_servlets.users set name=?, surname=?, phone=?" +
                " where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getPhone());
            preparedStatement.setLong(4, user.getId());
            preparedStatement.executeUpdate();


        } catch (Exception e) {
            logger.error("failed to update user: " + user);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    public void updateUserWithPassword(UserBean user) {
        String sql = "update vironit_db_servlets.users set password=?" +
                " where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getPassword());
            preparedStatement.setLong(2, user.getId());
            preparedStatement.executeUpdate();


        } catch (Exception e) {
            logger.error("failed to update user: " + user);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void delete(Long id) {
        String sql = "delete from vironit_db_servlets.users where users.id = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to delete user by id: " + id);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        User user = new User();
        String sql = "select * from vironit_db_servlets.users where users.email = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getEmail());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setId(resultSet.getLong("id"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setName(resultSet.getString("name"));
                user.setPhone(resultSet.getString("phone"));
                user.setSurname(resultSet.getString("surname"));
                user.setRolesId(resultSet.getInt("roles_id"));
            }

        } catch (Exception e) {
            logger.error("failed to find user by email: " + email);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return Optional.of(user);
    }

    public Optional<UserBean> findUserByEmail(String email) {
        UserBean user = new UserBean();
        RoleBean roleBean = new RoleBean();
        String sql = "select u.id, u.email, u.password, u.name, u.surname, u.phone, r.id, r.role from vironit_db_servlets.users as u join roles r on u.roles_id = r.id where u.email = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getEmail());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setId(resultSet.getLong("id"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setName(resultSet.getString("name"));
                user.setPhone(resultSet.getString("phone"));
                user.setSurname(resultSet.getString("surname"));

                roleBean.setId(resultSet.getInt("id"));
                roleBean.setRole(resultSet.getString("role"));

                user.setRole(roleBean);
            }

        } catch (Exception e) {
            logger.error("failed to find user by email: " + email);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return Optional.of(user);
    }

    public boolean checkEmail(String email) {
        UserBean user = new UserBean();
        RoleBean roleBean = new RoleBean();
        String sql = "select u.id, u.email, u.password, u.name, u.surname, u.phone, r.id, r.role from vironit_db_servlets.users as u join roles r on u.roles_id = r.id where u.email = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getEmail());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setId(resultSet.getLong("id"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setName(resultSet.getString("name"));
                user.setPhone(resultSet.getString("phone"));
                user.setSurname(resultSet.getString("surname"));

                roleBean.setId(resultSet.getInt("id"));
                roleBean.setRole(resultSet.getString("role"));

                user.setRole(roleBean);
            }

        } catch (Exception e) {
            logger.error("failed to find user by email: " + email);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return true;
    }

    public void deleteUser(UserBean user) {
        String sql = String.format("delete from `users` where `id` = %d", user.getId());

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to delete user by id: " + user);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }
}
