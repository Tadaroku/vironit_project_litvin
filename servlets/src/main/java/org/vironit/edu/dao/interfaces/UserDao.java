package org.vironit.edu.dao.interfaces;

import org.vironit.edu.model.User;

import java.util.Optional;

public interface UserDao extends BaseDao<User, Long> {
    Optional<User> findByEmail(String email);
    Optional<User> findById(Long id);
}
