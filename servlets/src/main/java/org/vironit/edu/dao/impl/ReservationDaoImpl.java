package org.vironit.edu.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vironit.edu.bean.*;
import org.vironit.edu.dao.interfaces.ReservationDao;
import org.vironit.edu.model.Reservation;
import org.vironit.edu.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ReservationDaoImpl implements ReservationDao {

    private static final Logger logger = LoggerFactory.getLogger(ReservationDaoImpl.class);

    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    @Override
    public Integer save(Reservation reservation) {
        String sql = "insert into vironit_db_servlets.reservations (price, seat, reservation_date, " +
                "luggage, status, users_id, flights_id, airlines_id) values(?,?,?,?,?,?,?,?);";
        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setDouble(1, reservation.getPrice());
            preparedStatement.setString(2, reservation.getSeat());
            preparedStatement.setDate(3, Date.valueOf(reservation.getReservationDate()));
            preparedStatement.setBoolean(4, reservation.isLuggage());
            preparedStatement.setBoolean(5, reservation.isStatus());
            preparedStatement.setLong(6, reservation.getUsersId());
            preparedStatement.setInt(7, reservation.getFlightsId());
            preparedStatement.setInt(8, reservation.getAirlinesId());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            logger.error("failed to save reservation: " + reservation);

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return id;
    }

    @SuppressWarnings("Duplicates")
    public Integer saveReservation(ReservationBean reservation) {
        String sql = "insert into vironit_db_servlets.reservations (luggage,price, seat, reservation_date, " +
                " status, users_id, flights_id, airlines_id) values(?,?,?,?,?,?,?,?);";
        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setBoolean(1, reservation.isLuggage());
            preparedStatement.setDouble(2, reservation.getPrice());
            preparedStatement.setString(3, reservation.getSeat());
            preparedStatement.setDate(4, Date.valueOf(reservation.getReservationDate()));
            preparedStatement.setBoolean(5, reservation.isStatus());
            preparedStatement.setLong(6, reservation.getUser().getId());
            preparedStatement.setInt(7, reservation.getFlight().getId());
            preparedStatement.setInt(8, reservation.getAirline().getId());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            logger.error("failed to save reservation: " + reservation);

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return id;
    }

    @SuppressWarnings("Duplicates")
    public Integer saveWOUser(ReservationBean reservation) {
        String sql = "insert into vironit_db_servlets.reservations (luggage,price, seat, reservation_date, " +
                " status, flights_id, airlines_id) values(?,?,?,?,?,?,?);";
        int id = -1;
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setBoolean(1, reservation.isLuggage());
            preparedStatement.setDouble(2, reservation.getPrice());
            preparedStatement.setString(3, reservation.getSeat());
            preparedStatement.setDate(4, Date.valueOf(reservation.getReservationDate()));
            preparedStatement.setBoolean(5, reservation.isStatus());
            preparedStatement.setInt(6, reservation.getFlight().getId());
            preparedStatement.setInt(7, reservation.getAirline().getId());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            logger.error("failed to save reservation: " + reservation);

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

        return id;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public List<Reservation> getAll() {
        String sql = "select * from vironit_db_servlets.reservations;";
        Reservation reservation;
        List<Reservation> reservations = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                reservation = new Reservation();
                reservation.setId(resultSet.getInt("id"));
                reservation.setPrice(resultSet.getDouble("price"));
                reservation.setSeat(resultSet.getString("seat"));
                reservation.setReservationDate(resultSet.getDate("reservation_date").toLocalDate());
                reservation.setLuggage(resultSet.getBoolean("luggage"));
                reservation.setStatus(resultSet.getBoolean("status"));
                reservation.setUsersId(resultSet.getLong("users_id"));
                reservation.setFlightsId(resultSet.getInt("flights_id"));
                reservation.setAirlinesId(resultSet.getInt("airlines_id"));
                reservations.add(reservation);
            }
        } catch (Exception e) {
            logger.error("failed to get reservations:");

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return reservations;
    }

    @SuppressWarnings("Duplicates")
    public List<ReservationBean> getAllByIdWhere(String where) {
        String sql = String.format("select * from `reservations` %s", where);
        ReservationBean reservation;
        UserBean userBean;
        AirlineBean airlineBean;
        FlightBean flightBean;
        List<ReservationBean> reservations = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                reservation = new ReservationBean();
                userBean = new UserBean();
                airlineBean = new AirlineBean();
                flightBean = new FlightBean();
                reservation.setId(resultSet.getInt("id"));
                reservation.setPrice(resultSet.getDouble("price"));
                reservation.setSeat(resultSet.getString("seat"));
                reservation.setReservationDate(resultSet.getDate("reservation_date").toLocalDate());
                reservation.setLuggage(resultSet.getBoolean("luggage"));
                reservation.setStatus(resultSet.getBoolean("status"));

                userBean.setId(resultSet.getLong("users_id"));
                flightBean.setId(resultSet.getInt("flights_id"));
                airlineBean.setId(resultSet.getInt("airlines_id"));

                reservation.setUser(userBean);
                reservation.setFlight(flightBean);
                reservation.setAirline(airlineBean);
                reservations.add(reservation);
            }
        } catch (Exception e) {
            logger.error("failed to get reservations:");

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return reservations;
    }

    @Override
    public void update(Reservation reservation) {
        String sql = "update vironit_db_servlets.reservations set price=?, seat=?, reservation_date=?, status=?, luggage=? where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDouble(1, reservation.getPrice());
            preparedStatement.setString(2, reservation.getSeat());
            preparedStatement.setDate(3, Date.valueOf(reservation.getReservationDate()));
            preparedStatement.setBoolean(4, reservation.isStatus());
            preparedStatement.setBoolean(5, reservation.isLuggage());
            preparedStatement.setInt(6, reservation.getId());
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            logger.error("failed to update reservation: " + reservation);

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @SuppressWarnings("Duplicates")
    public void updateReservation(ReservationBean reservation) {
        String sql = "update vironit_db_servlets.reservations set luggage=?, price=?, seat=?, reservation_date=?, status=? where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setBoolean(1, reservation.isLuggage());
            preparedStatement.setDouble(2, reservation.getPrice());
            preparedStatement.setString(3, reservation.getSeat());
            preparedStatement.setDate(4, Date.valueOf(reservation.getReservationDate()));
            preparedStatement.setBoolean(5, reservation.isStatus());
            preparedStatement.setInt(6, reservation.getId());
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            logger.error("failed to update reservation: " + reservation);

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @SuppressWarnings("Duplicates")
    public void up(ReservationBean reservation) {
        String sql = "update vironit_db_servlets.reservations set users_id=? where id=?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, reservation.getUser().getId());
            preparedStatement.setInt(2, reservation.getId());
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            logger.error("failed to update reservation: " + reservation);

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }


    @SuppressWarnings("Duplicates")
    @Override
    public void delete(Integer id) {
        String sql = "delete from vironit_db_servlets.reservations where reservations.id = ?;";

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to delete reservation by id: " + id);

        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }

    }

    @SuppressWarnings("Duplicates")
    public void deleteReservation(ReservationBean reservationBean) {
        String sql = String.format("delete from `reservations` where `id` = %d", reservationBean.getId());

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to delete user by id: " + reservationBean);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @SuppressWarnings("Duplicates")
    public void blockReservationById(int reservationId) {
        String sql = "update vironit_db_servlets.reservations set status = false where reservations.id = ?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, reservationId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to block reservation: " + reservationId);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @SuppressWarnings("Duplicates")
    public void unblockReservationById(int reservationId) {
        String sql = "update vironit_db_servlets.reservations set status = true, users_id = null where reservations.id = ?;";
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, reservationId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("failed to unblock reservation: " + reservationId);
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
    }

    @SuppressWarnings("Duplicates")
    public List<ReservationBean> getAllUsersReservationInfo() {
        String sql = "select r.id, r.luggage, r.price, r.seat, r.reservation_date, r.status, u.email, u.name, u.surname," +
                "u.phone, a.airline_name from reservations as r join users as u on u.id=r.users_id join airlines as a on a.id=r.airlines_id;";

        ReservationBean reservationBean;
        UserBean userBean;
        AirlineBean airlineBean;
        List<ReservationBean> reservations = new ArrayList<>();

        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                reservationBean = new ReservationBean();
                userBean = new UserBean();
                airlineBean = new AirlineBean();

                reservationBean.setId(resultSet.getInt("id"));
                reservationBean.setLuggage(resultSet.getBoolean("luggage"));
                reservationBean.setPrice(resultSet.getDouble("price"));
                reservationBean.setSeat(resultSet.getString("seat"));
                reservationBean.setReservationDate(resultSet.getDate("reservation_date").toLocalDate());
                reservationBean.setStatus(resultSet.getBoolean("status"));

                userBean.setEmail(resultSet.getString("email"));
                userBean.setName(resultSet.getString("name"));
                userBean.setSurname(resultSet.getString("surname"));
                userBean.setPhone(resultSet.getString("phone"));
                reservationBean.setUser(userBean);

                airlineBean.setName(resultSet.getString("airline_name"));
                reservationBean.setAirline(airlineBean);

                reservations.add(reservationBean);
            }

        } catch (Exception e) {
            logger.error("failed to find users reservation info: ");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return reservations;
    }

    @SuppressWarnings("Duplicates")
    public List<ReservationBean> getAllFlightReservationInfo() {
        String sql = "select r.id, r.luggage, r.price, r.seat, r.reservation_date, r.status, f.departure_time,f.arrival_time, " +
                "f.airplane_name, f.flight_number, ai.airline_name as \"airline_name\", a.airport_name as \"airport_from\", a.location as \"location_from\", " +
                "a.code as \"code_from\", a1.airport_name as \"airport_to\", a1.location as \"location_to\", a1.code as \"code_to\" from reservations as r " +
                " join flights as f on f.id=r.flights_id join airlines as ai on ai.id = r.airlines_id join airports as a on a.id = f.airport_from_id join airports as a1 on a1.id = f.airport_to_id ";

        ReservationBean reservationBean;
        FlightBean flightBean;
        AirportBean airportBeanFrom;
        AirportBean airportBeanTo;
        AirlineBean airlineBean;

        List<ReservationBean> reservations = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                reservationBean = new ReservationBean();
                flightBean = new FlightBean();
                airportBeanFrom = new AirportBean();
                airportBeanTo = new AirportBean();
                airlineBean = new AirlineBean();

                reservationBean.setId(resultSet.getInt("id"));
                reservationBean.setLuggage(resultSet.getBoolean("luggage"));
                reservationBean.setPrice(resultSet.getDouble("price"));
                reservationBean.setSeat(resultSet.getString("seat"));
                reservationBean.setReservationDate(resultSet.getDate("reservation_date").toLocalDate());
                reservationBean.setStatus(resultSet.getBoolean("status"));

                airlineBean.setName(resultSet.getString("airline_name"));
                reservationBean.setAirline(airlineBean);

                flightBean.setDepartureTime(resultSet.getTimestamp("departure_time").toLocalDateTime());
                flightBean.setArrivalTime(resultSet.getTimestamp("arrival_time").toLocalDateTime());
                flightBean.setAirplane(resultSet.getString("airplane_name"));
                flightBean.setFlightNumber(resultSet.getString("flight_number"));

                airportBeanFrom.setName(resultSet.getString("airport_from"));
                airportBeanFrom.setLocation(resultSet.getString("location_from"));
                airportBeanFrom.setCode(resultSet.getString("code_from"));
                flightBean.setAirportFrom(airportBeanFrom);

                airportBeanTo.setName(resultSet.getString("airport_to"));
                airportBeanTo.setLocation(resultSet.getString("location_to"));
                airportBeanTo.setCode(resultSet.getString("code_to"));
                flightBean.setAirportTo(airportBeanTo);

                reservationBean.setFlight(flightBean);
                reservations.add(reservationBean);


            }
        } catch (Exception e) {
            logger.error("failed to find flight reservation info");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return reservations;
    }

    @SuppressWarnings("Duplicates")
    public List<ReservationBean> getAllFlightReservationInfoById(String where) {
        String sql = "select r.id, r.luggage, r.price, r.seat, r.reservation_date, r.status, f.departure_time,f.arrival_time, " +
                "f.airplane_name, f.flight_number, ai.airline_name as \"airline_name\", a.airport_name as \"airport_from\", a.location as \"location_from\", " +
                "a1.airport_name as \"airport_to\", a1.location as \"location_to\" from reservations as r " +
                " join flights as f on f.id=r.flights_id join airlines as ai on ai.id = r.airlines_id join airports as a on a.id = f.airport_from_id join airports as a1 on a1.id = f.airport_to_id " + where + ";";

        ReservationBean reservationBean;
        FlightBean flightBean;
        AirportBean airportBeanFrom;
        AirportBean airportBeanTo;
        AirlineBean airlineBean;

        List<ReservationBean> reservations = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                reservationBean = new ReservationBean();
                flightBean = new FlightBean();
                airportBeanFrom = new AirportBean();
                airportBeanTo = new AirportBean();
                airlineBean = new AirlineBean();

                reservationBean.setId(resultSet.getInt("id"));
                reservationBean.setLuggage(resultSet.getBoolean("luggage"));
                reservationBean.setPrice(resultSet.getDouble("price"));
                reservationBean.setSeat(resultSet.getString("seat"));
                reservationBean.setReservationDate(resultSet.getDate("reservation_date").toLocalDate());
                reservationBean.setStatus(resultSet.getBoolean("status"));

                airlineBean.setName(resultSet.getString("airline_name"));
                reservationBean.setAirline(airlineBean);

                flightBean.setDepartureTime(resultSet.getTimestamp("departure_time").toLocalDateTime());
                flightBean.setArrivalTime(resultSet.getTimestamp("arrival_time").toLocalDateTime());
                flightBean.setAirplane(resultSet.getString("airplane_name"));
                flightBean.setFlightNumber(resultSet.getString("flight_number"));

                airportBeanFrom.setName(resultSet.getString("airport_from"));
                airportBeanFrom.setLocation(resultSet.getString("location_from"));
                flightBean.setAirportFrom(airportBeanFrom);

                airportBeanTo.setName(resultSet.getString("airport_to"));
                airportBeanTo.setLocation(resultSet.getString("location_to"));
                flightBean.setAirportTo(airportBeanTo);

                reservationBean.setFlight(flightBean);
                reservations.add(reservationBean);


            }
        } catch (Exception e) {
            logger.error("failed to find flight reservation info");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return reservations;
    }

    @SuppressWarnings("Duplicates")
    public List<ReservationBean> getAlll() {
        String sql = "select r.id, r.luggage, r.price, r.seat, r.reservation_date, r.status, f.departure_time,f.arrival_time, " +
                "f.airplane_name, f.flight_number, ai.airline_name as \"airline_name\", a.airport_name as \"airport_from\", a.location as \"location_from\", a.code as \"code_from\", a1.airport_name as \"airport_to\", a1.location as \"location_to\", a1.code as \"code_to\", u.email, u.password, u.name, u.surname, u.phone " +
                " from reservations as r " +
                " join flights as f on f.id=r.flights_id join users as u on u.id=r.users_id join airlines as ai on ai.id = r.airlines_id join airports as a on a.id = f.airport_from_id join airports as a1 on a1.id = f.airport_to_id";

        ReservationBean reservationBean;
        FlightBean flightBean;
        AirportBean airportBeanFrom;
        AirportBean airportBeanTo;
        AirlineBean airlineBean;
        UserBean userBean;

        List<ReservationBean> reservations = new ArrayList<>();
        try {
            connection = ConnectionUtil.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                reservationBean = new ReservationBean();
                flightBean = new FlightBean();
                airlineBean = new AirlineBean();
                userBean = new UserBean();
                airportBeanFrom = new AirportBean();
                airportBeanTo = new AirportBean();

                reservationBean.setId(resultSet.getInt("id"));
                reservationBean.setLuggage(resultSet.getBoolean("luggage"));
                reservationBean.setPrice(resultSet.getDouble("price"));
                reservationBean.setSeat(resultSet.getString("seat"));
                reservationBean.setReservationDate(resultSet.getDate("reservation_date").toLocalDate());
                reservationBean.setStatus(resultSet.getBoolean("status"));

                userBean.setEmail(resultSet.getString("email"));
                userBean.setPassword(resultSet.getString("password"));
                userBean.setName(resultSet.getString("name"));
                userBean.setSurname(resultSet.getString("surname"));
                userBean.setPhone(resultSet.getString("phone"));
                reservationBean.setUser(userBean);

                airlineBean.setName(resultSet.getString("airline_name"));
                reservationBean.setAirline(airlineBean);

                flightBean.setDepartureTime(resultSet.getTimestamp("departure_time").toLocalDateTime());
                flightBean.setArrivalTime(resultSet.getTimestamp("arrival_time").toLocalDateTime());
                flightBean.setAirplane(resultSet.getString("airplane_name"));
                flightBean.setFlightNumber(resultSet.getString("flight_number"));

                airportBeanFrom.setName(resultSet.getString("airport_from"));
                airportBeanFrom.setLocation(resultSet.getString("location_from"));
                airportBeanFrom.setCode(resultSet.getString("code_from"));
                flightBean.setAirportFrom(airportBeanFrom);

                airportBeanTo.setName(resultSet.getString("airport_to"));
                airportBeanTo.setLocation(resultSet.getString("location_to"));
                airportBeanTo.setCode(resultSet.getString("code_to"));
                flightBean.setAirportTo(airportBeanTo);

                reservationBean.setFlight(flightBean);
                reservations.add(reservationBean);


            }
        } catch (Exception e) {
            logger.error("failed to find flight reservation info");
        } finally {
            ConnectionUtil.closeAll(connection, preparedStatement, resultSet);
        }
        return reservations;
    }

}
