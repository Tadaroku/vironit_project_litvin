package org.vironit.edu.pattern;

public interface Patterns {

    String EMAIL = "^(.+)@(.+)$";
    String NAME_SURNAME = "[a-zA-Zа-яА-ЯёЁ]{2,}";
    String PHONE = "^\\+375 \\((17|29|33|44|25)\\) [0-9]{3}-[0-9]{2}-[0-9]{2}$";
    String PASSWORD = "[a-zA-Z0-9а-яА-ЯёЁ]{4,}";
}
