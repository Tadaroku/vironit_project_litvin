package org.vironit.edu.service;

import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.dao.impl.AirportDaoImpl;
import org.vironit.edu.model.Airport;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("Duplicates")
public class AirportService {
    private AirportDaoImpl airportDao;

    public AirportService() {
        airportDao = new AirportDaoImpl();
    }

    public void save(AirportBean airport) {
        airportDao.saveAirport(airport);
    }


    public List<AirportBean> findAllAirports() {
        return airportDao.getAllAirports();
    }

    public Optional<Airport> findAirportByName(String airportName) {
        return airportDao.findByAirportName(airportName);
    }

    public Optional<AirportBean> findAirportBeanByName(String airportName) {
        return airportDao.findByAirportBeanName(airportName);
    }

    public void updateAirport(Airport airport) {
        airportDao.update(airport);
    }

    public void updateAirportBean(AirportBean airport) {
        airportDao.updateAirport(airport);
    }

    public void deleteAirport(Integer id) {
        airportDao.delete(id);
    }

    public void deleteAirportBean(AirportBean airport) {
        airportDao.deleteAirport(airport);
    }

    public AirportBean findById(int id) {
        return airportDao.findById(id);
    }

}
