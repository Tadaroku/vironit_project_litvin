package org.vironit.edu.service;

import org.vironit.edu.bean.UserBean;
import org.vironit.edu.dao.impl.UserDaoImpl;
import org.vironit.edu.model.User;

import java.util.List;
import java.util.Optional;

public class UserService {

    private UserDaoImpl userDao;

    public UserService() {
        userDao = new UserDaoImpl();
    }

    public void addUser(User user) {
        userDao.save(user);
    }

    public Optional<User> findUserById(Long userId) {
        return userDao.findById(userId);
    }

    public Optional<UserBean> findUserBeanById(Long userId) {
        return userDao.findUserById(userId);
    }

    public List<User> getAllUsers() {
        return userDao.getAll();
    }

    public List<UserBean> getAllUsersBean() {
        return userDao.getAllUsers();
    }

    public List<UserBean> getAllUsersWhere(String where) {
        return userDao.getAll(where);
    }


    public void updateUser(User user) {
        userDao.update(user);
    }

    public void updateUserBean(UserBean user) {
        userDao.updateUser(user);
    }

    public void updateWOPass(UserBean user) {
        userDao.updateUserWOPassword(user);
    }

    public void updateWithPass(UserBean user) {
        userDao.updateUserWithPassword(user);
    }

    public void save(UserBean userBean) {
        userDao.saveUser(userBean);
    }

    public void deleteUserById(Long id) {
        userDao.delete(id);
    }

    public void deleteUser(UserBean user) {
        userDao.deleteUser(user);
    }

    public Optional<User> findUserByEmail(String email) {
        return userDao.findByEmail(email);
    }

    public Optional<UserBean> findUserBeanByEmail(String email) {
        return userDao.findUserByEmail(email);
    }

    public boolean checkUserEmail(String email){
        return userDao.checkEmail(email);
    }

//    public User checkUser(User user) {
//        User registeredUser = userDao.find(user);
//        if (registeredUser == null) {
//            return null;
//        } else if (!user.getPassword().equals(registeredUser.getPassword())) {
//            return null;
//        } else
//            return registeredUser;
//    }
}

