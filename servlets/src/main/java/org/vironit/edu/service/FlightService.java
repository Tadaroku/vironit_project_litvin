package org.vironit.edu.service;

import org.vironit.edu.bean.FlightBean;
import org.vironit.edu.dao.impl.AirportDaoImpl;
import org.vironit.edu.dao.impl.FlightDaoImpl;
import org.vironit.edu.model.Flight;

import java.util.List;
import java.util.Optional;

public class FlightService {

    private FlightDaoImpl flightDao;
    private AirportDaoImpl airportDao;

    public FlightService() {
        flightDao = new FlightDaoImpl();
        airportDao = new AirportDaoImpl();
    }

    public void addFlight(Flight flight) {
        flightDao.save(flight);
    }

    public void saveFlight(FlightBean flight) {
        flightDao.saveFlight(flight);
    }

    public Optional<Flight> findFlightByFlightNumber(String flightNumber) {
        return flightDao.findByFlightNumber(flightNumber);
    }

    public Optional<FlightBean> findFlightBeanByFlightNumber(String flightNumber) {
        return flightDao.findByFlightBeanNumber(flightNumber);
    }

    public void deleteFlight(Integer id) {
        flightDao.delete(id);
    }

    public void deleteFlightBean(FlightBean flightBean) {
        flightDao.deleteFlight(flightBean);
    }

    public void updateFlight(Flight flight) {
        flightDao.update(flight);
    }

    public void updateFlightBean(FlightBean flight) {
        flightDao.updateFlight(flight);
    }

    public List<Flight> getAllFlights() {
        return flightDao.getAll();
    }

    public List<FlightBean> getAllFlightsBean() {
        return flightDao.getAllFlights();
    }


    public List<FlightBean> findAllFlightsInfo() {
        return flightDao.findFlightInfo();

    }
}
