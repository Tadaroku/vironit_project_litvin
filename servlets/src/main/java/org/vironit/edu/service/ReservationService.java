package org.vironit.edu.service;

import org.vironit.edu.bean.ReservationBean;
import org.vironit.edu.dao.impl.AirlineDaoImpl;
import org.vironit.edu.dao.impl.FlightDaoImpl;
import org.vironit.edu.dao.impl.ReservationDaoImpl;
import org.vironit.edu.dao.impl.UserDaoImpl;
import org.vironit.edu.model.Reservation;

import java.util.List;

public class ReservationService {

    private ReservationDaoImpl reservationDao;
    private UserDaoImpl userDao;
    private FlightDaoImpl flightDao;
    private AirlineDaoImpl airlineDao;

    public ReservationService() {
        userDao = new UserDaoImpl();
        flightDao = new FlightDaoImpl();
        reservationDao = new ReservationDaoImpl();
        airlineDao = new AirlineDaoImpl();

    }


    public void addReservation(Reservation reservation) {
        reservationDao.save(reservation);
    }

    public void save(ReservationBean reservation) {
        reservationDao.saveReservation(reservation);
    }

    public void saveWOUser(ReservationBean reservation) {
        reservationDao.saveWOUser(reservation);
    }

    public void updateReservation(Reservation reservation) {
        reservationDao.update(reservation);
    }

    public void update(ReservationBean reservation) {
        reservationDao.updateReservation(reservation);
    }

    public void up(ReservationBean reservation) {
        reservationDao.up(reservation);
    }

    public void deleteReservation(Integer id) {
        reservationDao.delete(id);
    }

    public void delete(ReservationBean reservationBean) {
        reservationDao.deleteReservation(reservationBean);
    }

    public List<Reservation> getAllReservations() {
        return reservationDao.getAll();
    }

    public void unblockReservation(int reservationId) {
        reservationDao.unblockReservationById(reservationId);
    }

    public void blockReservation(int reservationId) {
        reservationDao.blockReservationById(reservationId);
    }

    public List<ReservationBean> getAllUsersReservation() {
        return reservationDao.getAllUsersReservationInfo();
    }

    public List<ReservationBean> getAllRes(String where) {
        return reservationDao.getAllByIdWhere(where);
    }

    public List<ReservationBean> getAllFlightsReservation() {
        return reservationDao.getAllFlightReservationInfo();
    }

    public List<ReservationBean> getAll() {
        return reservationDao.getAlll();
    }

    public List<ReservationBean> getAllFlightsReservationById(String where) {
        return reservationDao.getAllFlightReservationInfoById(where);
    }

}
