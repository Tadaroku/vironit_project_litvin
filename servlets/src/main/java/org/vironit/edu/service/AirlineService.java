package org.vironit.edu.service;

import org.vironit.edu.bean.AirlineBean;
import org.vironit.edu.dao.impl.AirlineDaoImpl;
import org.vironit.edu.model.Airline;

import java.util.List;
import java.util.Optional;

public class AirlineService {

    private AirlineDaoImpl airlineDao;

    public AirlineService() {
        airlineDao = new AirlineDaoImpl();
    }

    public Optional<Airline> findAirlineByName(String airlineName) {
        return airlineDao.findByAirlineName(airlineName);
    }

    public Optional<AirlineBean> findAirlineBeanByName(String airlineName) {
        return airlineDao.findByAirlineBeanName(airlineName);
    }

    public void addAirline(Airline airline) {
        airlineDao.save(airline);
    }

    public void saveAirline(AirlineBean airline) {
        airlineDao.saveAirline(airline);
    }

    public void updateAirline(Airline airline) {
        airlineDao.update(airline);
    }

    public void updateAirlineBean(AirlineBean airline) {
        airlineDao.updateAirline(airline);
    }

    public void deleteAirline(Integer id) {
        airlineDao.delete(id);
    }

    public void deleteAirlineBean(AirlineBean airlineBean) {
        airlineDao.deleteAirline(airlineBean);
    }

    public List<Airline> getAllAirlines() {
        return airlineDao.getAll();
    }

    public List<AirlineBean> getAllAirlineBeans() {
        return airlineDao.getAllAirlines();
    }


}
