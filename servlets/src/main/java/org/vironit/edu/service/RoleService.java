package org.vironit.edu.service;

import org.vironit.edu.bean.RoleBean;
import org.vironit.edu.dao.impl.RoleDaoImpl;
import org.vironit.edu.model.Role;

import java.util.List;

public class RoleService {

    private RoleDaoImpl roleDao;

    public RoleService() {
        roleDao = new RoleDaoImpl();
    }

    public void addRole(Role role) {
        roleDao.save(role);
    }

    public void updateRole(Role role) {
        roleDao.update(role);
    }

    public void updateRoleBean(RoleBean role) {
        roleDao.updateRole(role);
    }

    public List<Role> getAllRoles() {
        return roleDao.getAll();
    }

    public void deleteRole(Integer id) {
        roleDao.delete(id);
    }
}
