package org.vironit.edu.util;

import org.vironit.edu.bean.UserBean;
import org.vironit.edu.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Util {

    public static User getUser(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session != null) {
            Object oUser = session.getAttribute("user");
            if (oUser != null) {
                return (User) oUser;
            }

        }
        return null;

    }

    public static UserBean getUserBean(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session != null) {
            Object oUser = session.getAttribute("user");
            if (oUser != null) {
                return (UserBean) oUser;
            }

        }
        return null;

    }
}
