package org.vironit.edu.util;

import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConnectionUtil {

    private static final String DB_DRIVER = "datasource.driver";
    private static final String DB_URL = "datasource.url";
    private static final String DB_USERNAME = "datasource.username";
    private static final String DB_PASSWORD = "datasource.password";
    private static Properties properties = null;


    private static HikariDataSource dataSource;

    static {

        try (InputStream application = ConnectionUtil.class.getClassLoader().getResourceAsStream("connection.properties")) {
            properties = new Properties();
            properties.load(application);

            dataSource = new HikariDataSource();
            dataSource.setDriverClassName(properties.getProperty(DB_DRIVER));

            dataSource.setJdbcUrl(properties.getProperty(DB_URL));
            dataSource.setUsername(properties.getProperty(DB_USERNAME));
            dataSource.setPassword(properties.getProperty(DB_PASSWORD));

            dataSource.setMinimumIdle(5);
            dataSource.setMaximumPoolSize(20);
            dataSource.setAutoCommit(true);
            dataSource.setLoginTimeout(3);

        } catch (IOException | SQLException e1) {
            e1.printStackTrace();
        }
    }


    public static DataSource getDataSource() {

        return dataSource;

    }


    public static void closeAll(Connection Connection, Statement Statement, ResultSet ResultSet) {
        closeResultSet(ResultSet);
        closeStatement(Statement);
        closeConnection(Connection);
    }


    private static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException();
            }
        }
    }

    private static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException();
            }
        }
    }

    private static void closeResultSet(ResultSet ResultSet) {
        if (ResultSet != null) {
            try {
                ResultSet.close();
            } catch (SQLException e) {
                throw new RuntimeException();
            }
        }
    }
}
