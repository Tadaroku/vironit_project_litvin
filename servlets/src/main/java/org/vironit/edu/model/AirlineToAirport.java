package org.vironit.edu.model;

public class AirlineToAirport {

    private int airlineId;
    private int airportId;

    public AirlineToAirport() {
    }

    public AirlineToAirport(int airlineId, int airportId) {
        this.airlineId = airlineId;
        this.airportId = airportId;
    }

    public int getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(int airlineId) {
        this.airlineId = airlineId;
    }

    public int getAirportId() {
        return airportId;
    }

    public void setAirportId(int airportId) {
        this.airportId = airportId;
    }
}
