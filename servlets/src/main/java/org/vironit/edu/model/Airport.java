package org.vironit.edu.model;

public class Airport {
    private int id;
    private String code;
    private String location;
    private String name;

    public Airport() {
    }

    public Airport(int id, String code, String location, String name) {
        this.id = id;
        this.code = code;
        this.location = location;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
