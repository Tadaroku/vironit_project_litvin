package org.vironit.edu.model;

import java.time.LocalDateTime;

public class Flight {
    private int id;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;
    private String airplane;
    private String flightNumber;
    private int airportFromId;
    private int airportToId;

    public Flight() {
    }

    public Flight(int id, LocalDateTime departureTime, LocalDateTime arrivalTime, String airplane, String flightNumber, int airportFromId, int airportToId) {
        this.id = id;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.airplane = airplane;
        this.flightNumber = flightNumber;
        this.airportFromId = airportFromId;
        this.airportToId = airportToId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getAirplane() {
        return airplane;
    }

    public void setAirplane(String airplane) {
        this.airplane = airplane;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public int getAirportFromId() {
        return airportFromId;
    }

    public void setAirportFromId(int airportFromId) {
        this.airportFromId = airportFromId;
    }

    public int getAirportToId() {
        return airportToId;
    }

    public void setAirportToId(int airportToId) {
        this.airportToId = airportToId;
    }
}
