package org.vironit.edu.model;

import java.time.LocalDate;

public class Reservation {
    private int id;
    private double price;
    private boolean luggage;
    private LocalDate reservationDate;
    private String seat;
    private boolean status;
    private long usersId;
    private int flightsId;
    private int airlinesId;


    public Reservation() {
    }

    public Reservation(int id, double price, boolean luggage, LocalDate reservationDate, String seat, boolean status, long usersId, int flightsId, int airlinesId) {
        this.id = id;
        this.price = price;
        this.luggage = luggage;
        this.reservationDate = reservationDate;
        this.seat = seat;
        this.status = status;
        this.usersId = usersId;
        this.flightsId = flightsId;
        this.airlinesId = airlinesId;
    }

    public Reservation(double price, boolean luggage, LocalDate reservationDate, String seat, boolean status) {
        this.price = price;
        this.luggage = luggage;
        this.reservationDate = reservationDate;
        this.seat = seat;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public long getUsersId() {
        return usersId;
    }

    public void setUsersId(long usersId) {
        this.usersId = usersId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getFlightsId() {
        return flightsId;
    }

    public void setFlightsId(int flightsId) {
        this.flightsId = flightsId;
    }

    public int getAirlinesId() {
        return airlinesId;
    }

    public void setAirlinesId(int airlinesId) {
        this.airlinesId = airlinesId;
    }
}
