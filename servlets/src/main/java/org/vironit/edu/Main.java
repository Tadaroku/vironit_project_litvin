package org.vironit.edu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vironit.edu.bean.RoleBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.dao.Dao;
import org.vironit.edu.dao.impl.UserDaoImpl;
import org.vironit.edu.model.Flight;
import org.vironit.edu.model.Reservation;
import org.vironit.edu.model.User;
import org.vironit.edu.service.FlightService;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.service.UserService;

public class Main {
    private static final Logger LOG = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

//        Connection connection = ConnectionUtil.getDataSource().getConnection();


        Dao dao = Dao.getDao();
//        dao.getUserDao().delete(74L);
        dao.getUserDao().findUserByEmail("litvin.edward@mail.com");
//        System.out.println(dao.getFlightDao().findFlightInfo());

//        LOG.debug("This Will Be Printed On Debug");
//        LOG.info("This Will Be Printed On Info");
//        LOG.warn("This Will Be Printed On Warn");
//        LOG.error("This Will Be Printed On Error");
//        LOG.fatal("This Will Be Printed On Fatal");
//
//        LOG.info("Appending string: {}.", "Hello, World");


//        User user = new User(1, "email1238", "password", "name", "surname", "phone", 1);
//        UserService userService = new UserService();

//        FlightService flightService = new FlightService();

//        ReservationService reservationService = new ReservationService();
////        reservationService.getAllUsersReservation();
//        reservationService.getAllFlightsReservation();

//        userService.addUser(user);
//        dao.getUserDao().save(user);
//        dao.getUserDao().delete((long) 24);
//        System.out.println("deleted" + userDao);

//        System.out.println(dao.userDao.findById((long) 4));
//        dao.userDao.update(new User(4,"email1","pass","name1","surname1","phone1",1));


//        Flight flight = new Flight();
//
//        String str = "2011-10-02 18:48:05";
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        DateUtil parsedDate = dateFormat.parse(str);
//        Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
//
//
//        flight.setDepartureTime(timestamp);
//        flight.setArrivalTime(timestamp);
//        flight.setFlightNumber("");
//        flight.setAirplane("plane");
//        flight.setAirportFromId(1);
//        flight.setAirportToId(1);
//        dao.flightDao.save(flight);

//        Reservation reservation = new Reservation();
//        LocalDate localDate = LocalDate.of(2018,12,31);
//        reservation.setReservationDate(localDate);
//        reservation.setPrice(123);
//        reservation.setLuggage(true);
//        reservation.setSeat("seat");
//        reservation.setStatus("status");
//        reservation.setUsersId(2);
//        reservation.setFlightsId(1);
//        reservation.setAirlinesId(1);
//        dao.reservationDao.save(reservation);

    }
}
