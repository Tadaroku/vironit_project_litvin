package org.vironit.edu.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vironit.edu.bean.ReservationBean;
import org.vironit.edu.dao.impl.ReservationDaoImpl;
import org.vironit.edu.model.Reservation;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReservationDaoTest {

    private ReservationDaoImpl reservationDao = new ReservationDaoImpl();

    private Reservation actualReservation;

    @Before
    public void setUp() {
        actualReservation = new Reservation();
        actualReservation.setId(1);
        actualReservation.setStatus(true);
        actualReservation.setSeat("VIP");
        actualReservation.setLuggage(true);
        actualReservation.setReservationDate(LocalDate.now());
        actualReservation.setPrice(255);
        actualReservation.setUsersId(50L);
        actualReservation.setFlightsId(1);
        actualReservation.setAirlinesId(1);
    }

    @Test
    public void testSave() {
        reservationDao.save(actualReservation);
        assertNotNull(actualReservation);
    }


    @Test
    public void testDeleteReservation() {
        reservationDao.delete(actualReservation.getId());
        assert true;
    }

    @After
    public void dropDown() {
        actualReservation = null;
    }
}
