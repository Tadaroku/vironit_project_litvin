package org.vironit.edu.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vironit.edu.bean.RoleBean;
import org.vironit.edu.bean.UserBean;
import org.vironit.edu.dao.impl.UserDaoImpl;
import org.vironit.edu.model.User;

import java.util.List;

import static org.junit.Assert.*;

public class UserDaoTest {

    private UserDaoImpl userDao = new UserDaoImpl();

    private UserBean actualUser;
    private RoleBean roleBean;

    @Before
    public void setUp() {
        actualUser = new UserBean();
        roleBean = new RoleBean();
        actualUser.setId(36L);
        actualUser.setEmail("email@email");
        actualUser.setPassword("pass");
        actualUser.setName("name");
        actualUser.setSurname("surname");
        actualUser.setPhone("phone");
        roleBean.setId(1);
        roleBean.setRole("ADMIN");
        actualUser.setRole(roleBean);
    }

    @Test
    public void testSave() {
        userDao.saveUser(actualUser);
        assertNotNull(actualUser);
    }

    @Test
    public void testFindByEmail() {
        userDao.findUserByEmail(actualUser.getEmail());
        assertTrue(actualUser.getEmail().contains("email@email"));
    }

    @Test
    public void testGetAllUsers() {
        List<UserBean> users = userDao.getAllUsers();
        assertTrue(users.size() > 1);
    }

    @Test
    public void testDeleteUser() {
        userDao.delete(actualUser.getId());
        assert true;
    }

    @Test
    public void testGetUserById() {
        userDao.findUserById(actualUser.getId());
        assertEquals(actualUser.getId(), 36L);
    }


    @After
    public void dropDown() {
        actualUser = null;
    }


}
