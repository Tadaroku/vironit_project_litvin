package org.vironit.edu.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.bean.FlightBean;
import org.vironit.edu.dao.impl.FlightDaoImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FlightDaoTest {

    private FlightDaoImpl flightDao = new FlightDaoImpl();

    private FlightBean actualFlight;
    private AirportBean airportFrom;
    private AirportBean airportTo;

    @Before
    public void setUp() {
        String now = "2016-11-09 10:30";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        LocalDateTime formatDateTime = LocalDateTime.parse(now, formatter);
        actualFlight = new FlightBean();
        airportFrom = new AirportBean();
        airportTo = new AirportBean();
        actualFlight.setId(1);
        actualFlight.setArrivalTime(formatDateTime);
        actualFlight.setDepartureTime(formatDateTime);
        actualFlight.setFlightNumber("220");
        actualFlight.setAirplane("Boing");

        airportFrom.setId(1);
        airportFrom.setName("AirportFrom");
        airportFrom.setCode("AAA");
        airportFrom.setLocation("Minsk");

        airportTo.setId(2);
        airportTo.setName("AirportTo");
        airportFrom.setCode("BBB");
        airportFrom.setLocation("Brest");

        actualFlight.setAirportFrom(airportFrom);
        actualFlight.setAirportTo(airportTo);

    }

    @Test
    public void tesSave() {
        flightDao.saveFlight(actualFlight);
        assertNotNull(actualFlight);
    }

    @Test
    public void testGetAllFlights() {
        List<FlightBean> flights = flightDao.getAllFlights();
        assertTrue(flights.size() > 1);
    }

    @Test
    public void testDeleteFlight() {
        flightDao.delete(actualFlight.getId());
        assert true;
    }

    @After
    public void dropDown() {
        actualFlight = null;
    }

}
