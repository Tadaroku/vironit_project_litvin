package org.vironit.edu.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vironit.edu.bean.AirportBean;
import org.vironit.edu.dao.impl.AirportDaoImpl;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AirportDaoTest {

    private AirportDaoImpl airportDao = new AirportDaoImpl();

    private AirportBean actualAirport;

    @Before
    public void setUp() {
        actualAirport = new AirportBean();
        actualAirport.setId(4);
        actualAirport.setName("Airport");
        actualAirport.setCode("AAB");
        actualAirport.setLocation("Brest");
    }

    @Test
    public void testSave() {
        airportDao.saveAirport(actualAirport);
        assertNotNull(actualAirport);
    }

    @Test
    public void testGetAllAirports() {
        List<AirportBean> airports = airportDao.getAllAirports();
        assertTrue(airports.size() > 1);
    }

    @Test
    public void testDeleteAirport() {
        airportDao.delete(actualAirport.getId());
        assert true;
    }

    @Test
    public void testGetAirportByName() {
        airportDao.findByAirportBeanName(actualAirport.getName());
        assertTrue(actualAirport.getName().contains("Airport"));
    }

    @After
    public void dropDown() {
        actualAirport = null;
    }
}
