package org.vironit.edu.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vironit.edu.bean.AirlineBean;
import org.vironit.edu.dao.impl.AirlineDaoImpl;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AirlineDaoTest {

    private AirlineDaoImpl airlineDao = new AirlineDaoImpl();

    private AirlineBean actualAirline;

    @Before
    public void setUp() {
        actualAirline = new AirlineBean();
        actualAirline.setId(4);
        actualAirline.setName("Airline");
    }

    @Test
    public void testSave() {
        airlineDao.saveAirline(actualAirline);
        assertNotNull(actualAirline);
    }

    @Test
    public void testGetAllAirlines() {
        List<AirlineBean> airlines = airlineDao.getAllAirlines();
        assertTrue(airlines.size() > 1);
    }

    @Test
    public void testDeleteAirport() {
        airlineDao.delete(actualAirline.getId());
        assert true;
    }

    @Test
    public void testGetAirlineByName() {
        airlineDao.findByAirlineBeanName(actualAirline.getName());
        assertTrue(actualAirline.getName().contains("Airline"));
    }

    @After
    public void dropDown() {
        actualAirline = null;
    }
}
