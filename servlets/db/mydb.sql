-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema vironit_db_servlets
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `vironit_db_servlets` ;

-- -----------------------------------------------------
-- Schema vironit_db_servlets
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `vironit_db_servlets` DEFAULT CHARACTER SET utf8 ;
USE `vironit_db_servlets` ;

-- -----------------------------------------------------
-- Table `vironit_db_servlets`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vironit_db_servlets`.`roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vironit_db_servlets`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vironit_db_servlets`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `airplane` VARCHAR(45) NULL DEFAULT NULL,
  `surname` VARCHAR(45) NULL DEFAULT NULL,
  `phone` VARCHAR(45) NULL DEFAULT NULL,
  `roles_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_users_roles_idx` (`roles_id` ASC),
  CONSTRAINT `fk_users_roles`
    FOREIGN KEY (`roles_id`)
    REFERENCES `vironit_db_servlets`.`roles` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vironit_db_servlets`.`airports`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vironit_db_servlets`.`airports` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `airport_name` VARCHAR(45) NULL,
  `code` VARCHAR(3) NULL,
  `location` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vironit_db_servlets`.`flights`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vironit_db_servlets`.`flights` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `arrival_time` TIMESTAMP NULL,
  `departure_time` TIMESTAMP NULL,
  `airport_from_id` INT NOT NULL,
  `airport_to_id` INT NOT NULL,
  `airplane_name` VARCHAR(45) NULL,
  `flight_number` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_flights_airports1_idx` (`airport_from_id` ASC),
  INDEX `fk_flights_airports2_idx` (`airport_to_id` ASC),
  CONSTRAINT `fk_flights_airports1`
    FOREIGN KEY (`airport_from_id`)
    REFERENCES `vironit_db_servlets`.`airports` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_flights_airports2`
    FOREIGN KEY (`airport_to_id`)
    REFERENCES `vironit_db_servlets`.`airports` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vironit_db_servlets`.`airlines`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vironit_db_servlets`.`airlines` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `airline_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vironit_db_servlets`.`reservations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vironit_db_servlets`.`reservations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `luggage` BIT(1) NULL DEFAULT NULL,
  `price` DOUBLE NULL DEFAULT NULL,
  `seat` VARCHAR(45) NULL DEFAULT NULL,
  `reservation_date` DATE NULL DEFAULT NULL,
  `users_id` INT(11) NOT NULL,
  `flights_id` INT NOT NULL,
  `airlines_id` INT NOT NULL,
  `status` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_reservations_users1_idx` (`users_id` ASC),
  INDEX `fk_reservations_flights1_idx` (`flights_id` ASC),
  INDEX `fk_reservations_aviacompanies1_idx` (`airlines_id` ASC),
  CONSTRAINT `fk_reservations_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `vironit_db_servlets`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_reservations_flights1`
    FOREIGN KEY (`flights_id`)
    REFERENCES `vironit_db_servlets`.`flights` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservations_airlines1`
    FOREIGN KEY (`airlines_id`)
    REFERENCES `vironit_db_servlets`.`airlines` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vironit_db_servlets`.`airline_to_airport`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vironit_db_servlets`.`airline_to_airport` (
  `airport_id` INT NOT NULL,
  `airline_id` INT NOT NULL,
  INDEX `fk_airports_has_aviacompanies_aviacompanies1_idx` (`airline_id` ASC),
  INDEX `fk_airports_has_aviacompanies_airports1_idx` (`airport_id` ASC),
  PRIMARY KEY (`airport_id`, `airline_id`),
  CONSTRAINT `fk_airports_has_aviacompanies_airports1`
    FOREIGN KEY (`airport_id`)
    REFERENCES `vironit_db_servlets`.`airports` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_airports_has_aviacompanies_aviacompanies1`
    FOREIGN KEY (`airline_id`)
    REFERENCES `vironit_db_servlets`.`airlines` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vironit_db_servlets`.`airports_to_airlines`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vironit_db_servlets`.`airports_to_airlines` (
  `airports_id` INT NOT NULL,
  `airlines_id` INT NOT NULL,
  PRIMARY KEY (`airports_id`, `airlines_id`),
  INDEX `fk_airports_has_airlines_airlines1_idx` (`airlines_id` ASC),
  INDEX `fk_airports_has_airlines_airports1_idx` (`airports_id` ASC),
  CONSTRAINT `fk_airports_has_airlines_airports1`
    FOREIGN KEY (`airports_id`)
    REFERENCES `vironit_db_servlets`.`airports` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_airports_has_airlines_airlines1`
    FOREIGN KEY (`airlines_id`)
    REFERENCES `vironit_db_servlets`.`airlines` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
