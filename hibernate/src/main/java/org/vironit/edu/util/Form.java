package org.vironit.edu.util;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.time.LocalDateTime;

public class Form {

    public static boolean isPost(HttpServletRequest req) {
        return req.getMethod().equalsIgnoreCase("post");
    }

    public static boolean isGet(HttpServletRequest req) {
        return req.getMethod().equalsIgnoreCase("get");
    }

    public static String getString(HttpServletRequest request, String name) throws ParseException {
        return getString(request, name, ".*");
    }

    public static String getString(HttpServletRequest request, String name, String pattern) throws ParseException {
        String value = request.getParameter(name);
        if (value != null && value.matches(pattern))
            return value;
        throw new ParseException("Field " + name + " is incorrect. ", 0);
    }

    public static Integer getInt(HttpServletRequest request, String name) throws ParseException {
        String value = request.getParameter(name);
        if (value != null && value.matches("[-0-9]+"))
            return Integer.valueOf(value);
        throw new ParseException("Field " + name + " is incorrect. ", 0);
    }

    public static Long getLong(HttpServletRequest request, String name) throws ParseException {
        String value = request.getParameter(name);
        if (value != null && value.matches("[-0-9]+"))
            return Long.valueOf(value);
        throw new ParseException("Field " + name + " is incorrect. ", 0);
    }


    public static LocalDateTime getTime(HttpServletRequest request, LocalDateTime name) throws ParseException {
        LocalDateTime value = LocalDateTime.parse(request.getParameter(String.valueOf(name)));
        DateUtil.formatLocalDateTime(value, "dd-MM-yyyy HH:mm");
        return value;
    }
}
