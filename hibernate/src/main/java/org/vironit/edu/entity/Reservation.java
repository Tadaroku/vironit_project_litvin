package org.vironit.edu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "reservations")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "reservation_date", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate reservationDate;

    @Column(name = "luggage")
    private boolean luggage;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "seat", nullable = false)
    private String seat;

    @Column(name = "status")
    private boolean status;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "airline_id")
    private Airline airline;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "flight_id")
    private Flight flight;

    public Reservation() {
    }

    public Reservation(LocalDate reservationDate, boolean luggage, Double price, String seat, boolean status, Airline airline, Flight flight) {
        this.reservationDate = reservationDate;
        this.luggage = luggage;
        this.price = price;
        this.seat = seat;
        this.status = status;
        this.airline = airline;
        this.flight = flight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return luggage == that.luggage &&
                status == that.status &&
                Objects.equals(id, that.id) &&
                Objects.equals(reservationDate, that.reservationDate) &&
                Objects.equals(price, that.price) &&
                Objects.equals(seat, that.seat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, reservationDate, luggage, price, seat, status);
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", reservationDate=" + reservationDate +
                ", luggage=" + luggage +
                ", price=" + price +
                ", seat='" + seat + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
