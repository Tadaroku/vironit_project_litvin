package org.vironit.edu.controller;

import org.vironit.edu.entity.Reservation;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.service.UserService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@SuppressWarnings("Duplicates")
public class CmdProfile extends Cmd {

    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User user = Util.getUser(req);
        if (user == null)
            return Action.LOGIN.cmd;

        if (Form.isPost(req)) {
            if (req.getParameter("update") != null) {
                user.setPassword(req.getParameter("password"));
                user.setName(req.getParameter("name"));
                user.setSurname(req.getParameter("surname"));
                user.setPhone(req.getParameter("phone"));
                UserService.getInstance().update(user);

            } else if (req.getParameter("logout") != null) {
                resp.addCookie(new Cookie("password", ""));
                HttpSession session = req.getSession();
                session.invalidate();
                return Action.LOGIN.cmd;
            }

        }


        List<Reservation> reservations = ReservationService.getInstance().getAllById(user.getId());

        HttpSession session = req.getSession();
        session.setAttribute("reservations", reservations);

        return null;
    }

}
