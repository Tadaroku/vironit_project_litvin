package org.vironit.edu.controller;

public enum Action {

    ERROR {{
        cmd = new CmdError();
    }},

    LOGIN {{
        cmd = new CmdLogin();
    }},

    CANCEL {{
        cmd = new CmdCancel();
    }},

    SIGNUP {{
        cmd = new CmdSignup();
    }},

    PROFILE {{
        cmd = new CmdProfile();
    }},

    USERSWITHRESERVATIONS {{
        cmd = new CmdUsersWithReservations();
    }},

    ADMIN {{
        cmd = new CmdAdmin();
    }},

    HOME {{
        cmd = new CmdHome();
    }},

    ADDAIRLINES {{
        cmd = new CmdAddAirlines();
    }},

    EDITAIRLINES {{
        cmd = new CmdEditAirlines();
    }},

    ADDAIRPORTS {{
        cmd = new CmdAddAirports();
    }},

    EDITAIRPORTS {{
        cmd = new CmdEditAirports();
    }},

    ADDFLIGHTS {{
        cmd = new CmdAddFlights();
    }},

    EDITFLIGHTS {{
        cmd = new CmdEditFlights();
    }},

    ADDRESERVATIONS {{
        cmd = new CmdAddReservations();
    }},

    EDITRESERVATIONS {{
        cmd = new CmdEditReservations();
    }},

    RESERVATIONLIST {{
        cmd = new CmdReservationList();
    }},

    ADD {{
        cmd = new CmdAdd();
    }},

    EDITUSERS {{
        cmd = new CmdEditUsers();
    }};


    public String getJsp() {
        return "/" + this.cmd.toString().toLowerCase() + ".jsp";
    }

    public Cmd cmd = new CmdError();
}
