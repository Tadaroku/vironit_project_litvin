package org.vironit.edu.controller;

import org.apache.commons.codec.digest.DigestUtils;
import org.vironit.edu.entity.User;
import org.vironit.edu.pattern.Patterns;
import org.vironit.edu.service.UserService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@SuppressWarnings("Duplicates")
public class CmdLogin extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User user = Util.getUser(req);
        if (user != null) {
            return Action.PROFILE.cmd;
        }
        if (Form.isPost(req)) {
            String email = Form.getString(req, "email", Patterns.EMAIL);
            String password = Form.getString(req, "password", Patterns.PASSWORD);
            List<User> users = UserService.getInstance().getAllWhere(email, password);
            if (users.size() > 0) {
                HttpSession session = req.getSession();
                session.setMaxInactiveInterval(30*30);
                Cookie loginCookie = new Cookie("email", email);
                Cookie passwordCookie = new Cookie("password", DigestUtils.md5Hex(password));
                loginCookie.setMaxAge(60*60);
                passwordCookie.setMaxAge(60*60);
                resp.addCookie(loginCookie);
                resp.addCookie(passwordCookie);
                session.setAttribute("user", users.get(0));
                return Action.PROFILE.cmd;
            } else {
                req.setAttribute("error", true);
                return Action.LOGIN.cmd;
            }

        }
        return null;
    }
}
