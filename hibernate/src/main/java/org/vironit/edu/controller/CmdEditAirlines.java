package org.vironit.edu.controller;

import org.vironit.edu.entity.Airline;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.AirlineService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdEditAirlines extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User admin = Util.getUser(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            int id = Integer.parseInt(Form.getString(req, "id"));
            String name = Form.getString(req, "name");

            Airline airline = new Airline(name);
            airline.setId(id);
            if (req.getParameter("update") != null) {
                AirlineService.getInstance().update(airline);
            }
            if (req.getParameter("delete") != null) {
                AirlineService.getInstance().delete(id);
            }

        }
        req.getServletContext().setAttribute("airlines", AirlineService.getInstance().getAll());
        return null;
    }
}
