package org.vironit.edu.controller;

import org.vironit.edu.entity.User;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("Duplicates")
public class CmdAdmin extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User user = Util.getUser(req);
        if (user == null || (!user.getEmail().equals("admin@mail.com") && !user.getPassword().equals("admin123"))) {
            return Action.LOGIN.cmd;
        }
        return null;
    }
}
