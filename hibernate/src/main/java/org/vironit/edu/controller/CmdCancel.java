package org.vironit.edu.controller;

import org.vironit.edu.entity.Reservation;
import org.vironit.edu.service.ReservationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdCancel extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        int reservation_id = Integer.valueOf(req.getParameter("reservation_id"));
        Reservation reservation = ReservationService.getInstance().getById(reservation_id);
        reservation.setUser(null);
        reservation.setStatus(true);
        ReservationService.getInstance().update(reservation);
        return null;
    }
}
