package org.vironit.edu.controller;

import org.vironit.edu.entity.User;
import org.vironit.edu.pattern.Patterns;
import org.vironit.edu.service.UserService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.text.ParseException;

@SuppressWarnings("Duplicates")
public class CmdSignup extends Cmd {

    @Override
    public Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws SQLException, ParseException {
        User loggedUser = Util.getUser(req);
        if (loggedUser != null) {
            return Action.PROFILE.cmd;
        }
        if (Form.isPost(req)) {
            String email = Form.getString(req, "email", Patterns.EMAIL);
            String password = Form.getString(req, "password", Patterns.PASSWORD);
            String name = Form.getString(req, "name", Patterns.NAME_SURNAME);
            String surname = Form.getString(req, "surname", Patterns.NAME_SURNAME);
            String phone = Form.getString(req, "phone", Patterns.PHONE);
            String repeatPassword = Form.getString(req, "repeatPassword", Patterns.PASSWORD);

            boolean valid = true;

            if (!password.equals(repeatPassword)) {
                req.setAttribute("error", "password");
                valid = false;
            }

            if (!valid) {
                return Action.SIGNUP.cmd;
            }


            if (UserService.getInstance().isEmailExist(email)) {
                req.setAttribute("error", "error");
                return Action.SIGNUP.cmd;
            }


            User user = new User();
            user.setEmail(email);
            user.setName(name);
            user.setRole("USER");
            user.setPassword(password);
            user.setPhone(phone);
            user.setSurname(surname);
            UserService.getInstance().save(user);
            return Action.LOGIN.cmd;
        }
        return null;
    }
}
