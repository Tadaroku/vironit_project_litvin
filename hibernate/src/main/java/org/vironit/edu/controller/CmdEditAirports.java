package org.vironit.edu.controller;

import org.vironit.edu.entity.Airport;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("Duplicates")
public class CmdEditAirports extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User admin = Util.getUser(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            int id = Integer.parseInt(Form.getString(req, "id"));
            String name = Form.getString(req, "name");
            String code = Form.getString(req, "code");
            String location = Form.getString(req, "location");

            Airport airport = new Airport(name, code, location);
            airport.setId(id);
            if (req.getParameter("update") != null) {
                AirportService.getInstance().update(airport);
            }
            if (req.getParameter("delete") != null) {
                AirportService.getInstance().delete(id);
            }

        }
        req.getServletContext().setAttribute("airports", AirportService.getInstance().getAll());
        return null;
    }
}
