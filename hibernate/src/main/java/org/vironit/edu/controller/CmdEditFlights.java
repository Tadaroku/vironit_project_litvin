package org.vironit.edu.controller;

import org.vironit.edu.entity.Airport;
import org.vironit.edu.entity.Flight;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.service.FlightService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

@SuppressWarnings("Duplicates")
public class CmdEditFlights extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User admin = Util.getUser(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            int id = Integer.parseInt(Form.getString(req, "id"));
            LocalDateTime departureTime = LocalDateTime.parse(req.getParameter("departureTime"));
            LocalDateTime arrivalTime = LocalDateTime.parse(req.getParameter("arrivalTime"));
            String name = Form.getString(req, "name");
            String flightNumber = Form.getString(req, "flightNumber");

            Integer airportFromId = Form.getInt(req, "airportFromId");
            Integer airportToId = Form.getInt(req, "airportToId");

            Airport from = AirportService.getInstance().getById(airportFromId);
            Airport to = AirportService.getInstance().getById(airportToId);

            Flight flight = new Flight(departureTime, arrivalTime, name, flightNumber, from, to);
            flight.setId(id);
            if (req.getParameter("update") != null) {
                FlightService.getInstance().update(flight);
            }
            if (req.getParameter("delete") != null) {
                FlightService.getInstance().delete(id);
            }

        }
        req.getServletContext().setAttribute("flights", FlightService.getInstance().getAll());
        req.getServletContext().setAttribute("airports", AirportService.getInstance().getAll());
        return null;
    }
}

