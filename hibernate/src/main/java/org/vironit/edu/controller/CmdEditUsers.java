package org.vironit.edu.controller;

import org.vironit.edu.entity.User;
import org.vironit.edu.service.UserService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("Duplicates")
public class CmdEditUsers extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {
            long id = Long.parseLong(Form.getString(req, "id"));
            String email = Form.getString(req, "email");
            String password = Form.getString(req, "password");
            String name = Form.getString(req, "name");
            String surname = Form.getString(req, "surname");
            String phone = Form.getString(req, "phone");
            String role = Form.getString(req, "role");

            User user = new User(email, name, password, phone, role, surname);
            user.setId(id);
            if (req.getParameter("update") != null) {
                UserService.getInstance().update(user);
            }
            if (req.getParameter("delete") != null) {
                UserService.getInstance().delete(id);
            }
        }
        req.getServletContext().setAttribute("users", UserService.getInstance().getAll());
        return null;
    }
}
