package org.vironit.edu.controller;

import org.vironit.edu.entity.Airport;
import org.vironit.edu.entity.Flight;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.service.FlightService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

@SuppressWarnings("Duplicates")
public class CmdAddFlights extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {
            LocalDateTime departureTime = LocalDateTime.parse(req.getParameter("departureTime"));
            LocalDateTime arrivalTime = LocalDateTime.parse(req.getParameter("arrivalTime"));
            String airplaneName = Form.getString(req, "airplaneName");
            String flightNumber = Form.getString(req, "flightNumber");

            Integer airportFromId = Form.getInt(req, "airportFromId");
            Integer airportToId = Form.getInt(req, "airportToId");

            Airport from = AirportService.getInstance().getById(airportFromId);
            Airport to = AirportService.getInstance().getById(airportToId);

            Flight flight = new Flight(departureTime, arrivalTime, airplaneName, flightNumber, from, to);
            FlightService.getInstance().save(flight);
            return Action.EDITFLIGHTS.cmd;
        }
        return null;
    }
}
