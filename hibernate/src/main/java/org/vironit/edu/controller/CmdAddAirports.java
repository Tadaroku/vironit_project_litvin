package org.vironit.edu.controller;

import org.vironit.edu.entity.Airport;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("Duplicates")
public class CmdAddAirports extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {

            String name = Form.getString(req, "name");
            String code = Form.getString(req, "code");
            String location = Form.getString(req, "location");
            Airport airport = new Airport(name, code, location);

            AirportService.getInstance().save(airport);
            return Action.EDITAIRPORTS.cmd;
        }
        return null;
    }
}
