package org.vironit.edu.controller;

import org.vironit.edu.entity.Airline;
import org.vironit.edu.service.AirlineService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CmdAddAirlines extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {

            String name = Form.getString(req, "name");
            Airline airline = new Airline(name);
            AirlineService.getInstance().save(airline);
            return Action.EDITAIRLINES.cmd;
        }
        return null;
    }
}
