package org.vironit.edu.controller;

import org.vironit.edu.entity.Airline;
import org.vironit.edu.entity.Flight;
import org.vironit.edu.entity.Reservation;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.AirlineService;
import org.vironit.edu.service.FlightService;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.util.Form;
import org.vironit.edu.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class CmdEditReservations extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User admin = Util.getUser(req);
        if (admin == null) {
            return Action.LOGIN.cmd;
        }
        if (Form.isPost(req)) {
            int id = Integer.parseInt(req.getParameter("id"));
            boolean luggage = Boolean.parseBoolean(req.getParameter("luggage"));
            double price = Double.parseDouble(req.getParameter("price"));
            String seat = req.getParameter("seat");
            LocalDate reservationDate = LocalDate.parse(req.getParameter("reservationDate"));
            boolean status = Boolean.parseBoolean(req.getParameter("status"));

            Integer flightId = Form.getInt(req, "flightId");
            Integer airlineId = Form.getInt(req, "airlineId");

            Airline airline = AirlineService.getInstance().getById(airlineId);
            Flight flight = FlightService.getInstance().getById(flightId);


            Reservation reservation = new Reservation(reservationDate, luggage, price, seat, status, airline, flight);
            reservation.setId(id);
            if (req.getParameter("update") != null) {
                ReservationService.getInstance().update(reservation);
            }
            if (req.getParameter("delete") != null) {
                ReservationService.getInstance().delete(id);
            }
        }
        req.getServletContext().setAttribute("reservations", ReservationService.getInstance().getAll());
        req.getServletContext().setAttribute("airlines", AirlineService.getInstance().getAll());
        req.getServletContext().setAttribute("flights", FlightService.getInstance().getAll());
        return null;
    }
}
