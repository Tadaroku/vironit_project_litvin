package org.vironit.edu.controller;

import org.vironit.edu.entity.Airline;
import org.vironit.edu.entity.Flight;
import org.vironit.edu.entity.Reservation;
import org.vironit.edu.service.AirlineService;
import org.vironit.edu.service.FlightService;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.util.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

@SuppressWarnings("Duplicates")
public class CmdAddReservations extends Cmd {
    @Override
    Cmd execute(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (Form.isPost(req)) {
            Boolean luggage = Boolean.valueOf(req.getParameter("luggage"));
            Double price = Double.valueOf(req.getParameter("price"));
            String seat = req.getParameter("seat");
            LocalDate reservationDate = LocalDate.parse(req.getParameter("reservationDate"));
            Boolean status = Boolean.valueOf(req.getParameter("status"));

            Integer flightId = Integer.parseInt(req.getParameter("flightId"));
            Integer airlineId = Integer.parseInt(req.getParameter("airlineId"));

            Flight flight = FlightService.getInstance().getById(flightId);
            Airline airline = AirlineService.getInstance().getById(airlineId);


            Reservation reservation = new Reservation(reservationDate, luggage, price, seat, status, airline, flight);
            ReservationService.getInstance().save(reservation);

            return Action.EDITRESERVATIONS.cmd;
        }
        return null;
    }
}
