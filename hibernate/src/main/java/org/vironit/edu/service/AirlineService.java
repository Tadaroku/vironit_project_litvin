package org.vironit.edu.service;

import org.vironit.edu.dao.impl.AirlineDaoImpl;
import org.vironit.edu.dao.interfaces.AirlineDao;
import org.vironit.edu.entity.Airline;

import java.util.List;

public class AirlineService {

    private AirlineDao airlineDao;
    private static AirlineService instance;

    public AirlineService() {
        airlineDao = new AirlineDaoImpl();
    }

    public static AirlineService getInstance() {
        if (instance == null) {
            instance = new AirlineService();
        }
        return instance;
    }

    public List<Airline> getAll() {
        return airlineDao.getAll();
    }

    public Airline getById(int id) {
        return airlineDao.getById(id);
    }

    public void save(Airline airline) {
        airlineDao.save(airline);
    }

    public void update(Airline airline) {
        airlineDao.update(airline);
    }

    public void delete(Integer id) {
        Airline airline = airlineDao.getById(id);
        airlineDao.delete(airline);

    }
}

