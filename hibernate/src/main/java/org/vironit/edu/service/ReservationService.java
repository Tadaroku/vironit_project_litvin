package org.vironit.edu.service;

import org.vironit.edu.dao.impl.ReservationDaoImpl;
import org.vironit.edu.dao.interfaces.ReservationDao;
import org.vironit.edu.entity.Reservation;

import java.util.List;

public class ReservationService {

    private ReservationDao reservationDao;
    private static ReservationService instance;

    public ReservationService() {
        reservationDao = new ReservationDaoImpl();
    }

    public static ReservationService getInstance() {
        if (instance == null) {
            instance = new ReservationService();
        }
        return instance;
    }

    public List<Reservation> getAll() {
        return reservationDao.getAll();
    }

    public List<Reservation> getAllById(long id) {
        return reservationDao.getAllById(id);
    }

    public Reservation getById(int id) {
        return reservationDao.getById(id);
    }

    public void save(Reservation reservation) {
        reservationDao.save(reservation);
    }

    public void update(Reservation reservation) {
        reservationDao.update(reservation);
    }

    public void setUser(long id) {
        reservationDao.setUser(id);
    }

    public void block(int id) {
        reservationDao.blockReservationById(id);
    }

    public void delete(Integer id) {
        Reservation reservation = reservationDao.getById(id);
        reservationDao.delete(reservation);

    }

}
