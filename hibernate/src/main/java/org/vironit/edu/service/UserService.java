package org.vironit.edu.service;

import org.vironit.edu.dao.impl.UserDaoImpl;
import org.vironit.edu.dao.interfaces.UserDao;
import org.vironit.edu.entity.User;

import java.util.List;
import java.util.Optional;

public class UserService {

    private UserDao userDao;
    private static UserService instance;

    public UserService() {
        userDao = new UserDaoImpl();
    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public List<User> getAll() {
        return userDao.getAll();
    }

    public List<User> getAllWhere(String email, String password) {
        return userDao.getAllWhere(email, password);
    }

    public Optional<User> getByEmail(String email) {
        return userDao.getByEmail(email);
    }

    public boolean isEmailExist(String email) {
        return userDao.isEmailExist(email);
    }

    public boolean isExist(String email, String password) {
        return userDao.isExist(email, password);
    }

    public User getById(long id) {
        return userDao.getById(id);
    }

    public void save(User user) {
        userDao.save(user);
    }

    public void update(User user) {
        userDao.update(user);
    }

    public void delete(Long id) {
        User user = userDao.getById(id);
        userDao.delete(user);

    }
}
