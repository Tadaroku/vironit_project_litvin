package org.vironit.edu.service;

import org.vironit.edu.dao.impl.FlightDaoImpl;
import org.vironit.edu.dao.interfaces.FlightDao;
import org.vironit.edu.entity.Flight;

import java.util.List;

public class FlightService {
    private FlightDao flightDao;
    private static FlightService instance;

    public FlightService() {
        flightDao = new FlightDaoImpl();
    }

    public static FlightService getInstance() {
        if (instance == null) {
            instance = new FlightService();
        }
        return instance;
    }

    public List<Flight> getAll() {
        return flightDao.getAll();
    }

    public Flight getById(int id) {
        return flightDao.getById(id);
    }

    public void save(Flight flight) {
        flightDao.save(flight);
    }

    public void update(Flight flight) {
        flightDao.update(flight);
    }

    public void delete(Integer id) {
        Flight flight = flightDao.getById(id);
        flightDao.delete(flight);

    }
}
