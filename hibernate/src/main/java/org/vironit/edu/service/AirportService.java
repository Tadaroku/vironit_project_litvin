package org.vironit.edu.service;

import org.vironit.edu.dao.impl.AirportDaoImpl;
import org.vironit.edu.dao.interfaces.AirportDao;
import org.vironit.edu.entity.Airport;

import java.util.List;

public class AirportService {

    private AirportDao airportDao;
    private static AirportService instance;

    public AirportService() {
        airportDao = new AirportDaoImpl();
    }

    public static AirportService getInstance() {
        if (instance == null) {
            instance = new AirportService();
        }
        return instance;
    }

    public List<Airport> getAll() {
        return airportDao.getAll();
    }

    public Airport getById(int id) {
        return airportDao.getById(id);
    }

    public void save(Airport airport) {
        airportDao.save(airport);
    }

    public void update(Airport airport) {
        airportDao.update(airport);
    }

    public void delete(Integer id) {
        Airport airport = airportDao.getById(id);
        airportDao.delete(airport);

    }
}
