package org.vironit.edu.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.vironit.edu.dao.interfaces.UserDao;
import org.vironit.edu.entity.User;
import org.vironit.edu.util.HibernateSessionFactoryUtil;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("Duplicates")
public class UserDaoImpl implements UserDao {

    @Override
    public Optional<User> getByEmail(String email) {
        String sql = "from org.vironit.edu.entity.User u where u.email =:email";
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery(sql);
        query.setParameter("email", email);
        return Optional.ofNullable((User) query.list().get(0));
    }

    @Override
    public boolean isExist(String email, String password) {
        String sql = "from org.vironit.edu.entity.User u where u.email =:email and u.password  =:password";
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery(sql);
        query.setParameter("email", email);
        query.setParameter("password", password);
        List<User> users = query.list();
        return users.size() != 0;
    }

    @Override
    public boolean isEmailExist(String email) {
        String sql = "from org.vironit.edu.entity.User u where u.email =:email";
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery(sql);
        query.setParameter("email", email);
        return query.list().size() != 0;
    }

    @Override
    public User getById(long id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        User user = session.get(User.class, id);
        transaction.commit();
        session.close();
        return user;
    }

    @Override
    public void delete(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(user);
        transaction.commit();
        session.close();
    }

    @Override
    public List<User> getAllWhere(String email, String password) {
        String sql = "from org.vironit.edu.entity.User u where u.email =:email and u.password =:password";
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery(sql);
        query.setParameter("email", email);
        query.setParameter("password", password);
        List<User> users = query.list();
        session.close();
        return users;
    }

    @Override
    public void save(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        transaction.commit();
        session.close();
    }

    @Override
    public List<User> getAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from org.vironit.edu.entity.User");
        List<User> users = query.list();
        session.close();
        return users;
    }

    @Override
    public void update(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(user);
        transaction.commit();
        session.close();
    }

}
