package org.vironit.edu.dao.interfaces;

import java.util.List;

public interface BaseDao<T, ID> {

    void save(T entity);

    List<T> getAll();

    void update(T entity);

}
