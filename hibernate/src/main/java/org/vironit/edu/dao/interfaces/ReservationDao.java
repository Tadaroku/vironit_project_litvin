package org.vironit.edu.dao.interfaces;

import org.vironit.edu.entity.Reservation;

import java.util.List;

public interface ReservationDao extends BaseDao<Reservation, Integer> {
    Reservation getById(int id);

    void delete(Reservation reservation);

    List<Reservation> getAllById(long id);

    void blockReservationById(int id);

    void setUser(long id);
}
