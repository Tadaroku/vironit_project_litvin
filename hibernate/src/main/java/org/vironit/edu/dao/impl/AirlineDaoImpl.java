package org.vironit.edu.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.vironit.edu.dao.interfaces.AirlineDao;
import org.vironit.edu.entity.Airline;
import org.vironit.edu.util.HibernateSessionFactoryUtil;

import java.util.List;

@SuppressWarnings("Duplicates")
public class AirlineDaoImpl implements AirlineDao {

    @Override
    public Airline getById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Airline airline = session.get(Airline.class, id);
        transaction.commit();
        session.close();
        return airline;
    }

    @Override
    public void delete(Airline airline) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(airline);
        transaction.commit();
        session.close();
    }

    @Override
    public void save(Airline airline) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(airline);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Airline> getAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from org.vironit.edu.entity.Airline");
        List<Airline> airlines = query.list();
        session.close();
        return airlines;
    }

    @Override
    public void update(Airline airline) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(airline);
        transaction.commit();
        session.close();
    }
}
