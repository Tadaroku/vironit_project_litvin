package org.vironit.edu.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.vironit.edu.dao.interfaces.AirportDao;
import org.vironit.edu.entity.Airport;
import org.vironit.edu.util.HibernateSessionFactoryUtil;

import java.util.List;

@SuppressWarnings("Duplicates")
public class AirportDaoImpl implements AirportDao {


    @Override
    public Airport getById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Airport airport = session.get(Airport.class, id);
        transaction.commit();
        session.close();
        return airport;
    }

    @Override
    public void delete(Airport airport) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(airport);
        transaction.commit();
        session.close();
    }

    @Override
    public void save(Airport airport) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(airport);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Airport> getAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from org.vironit.edu.entity.Airport");
        List<Airport> airports = query.list();
        session.close();
        return airports;
    }

    @Override
    public void update(Airport airport) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(airport);
        transaction.commit();
        session.close();
    }
}
