package org.vironit.edu.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.vironit.edu.dao.interfaces.FlightDao;
import org.vironit.edu.entity.Flight;
import org.vironit.edu.util.HibernateSessionFactoryUtil;

import java.util.List;

@SuppressWarnings("Duplicates")
public class FlightDaoImpl implements FlightDao {

    @Override
    public Flight getById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Flight flight = session.get(Flight.class, id);
        transaction.commit();
        session.close();
        return flight;
    }

    @Override
    public void delete(Flight flight) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(flight);
        transaction.commit();
        session.close();
    }

    @Override
    public void save(Flight flight) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(flight);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Flight> getAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from org.vironit.edu.entity.Flight");
        List<Flight> flights = query.list();
        session.close();
        return flights;
    }

    @Override
    public void update(Flight flight) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(flight);
        transaction.commit();
        session.close();
    }
}
