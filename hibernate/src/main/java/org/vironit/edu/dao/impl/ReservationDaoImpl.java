package org.vironit.edu.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.vironit.edu.dao.interfaces.ReservationDao;
import org.vironit.edu.entity.Reservation;
import org.vironit.edu.util.HibernateSessionFactoryUtil;

import java.util.List;

@SuppressWarnings("Duplicates")
public class ReservationDaoImpl implements ReservationDao {

    @Override
    public Reservation getById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Reservation reservation = session.get(Reservation.class, id);
        transaction.commit();
        session.close();
        return reservation;
    }

    @Override
    public void delete(Reservation reservation) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(reservation);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Reservation> getAllById(long id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from org.vironit.edu.entity.Reservation where user.id =:id")
                .setParameter("id", id);
        List<Reservation> reservations = query.list();
        session.close();
        return reservations;
    }

    @Override
    public void save(Reservation reservation) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(reservation);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Reservation> getAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from org.vironit.edu.entity.Reservation");
        List<Reservation> reservations = query.list();
        session.close();
        return reservations;
    }

    @Override
    public void update(Reservation reservation) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(reservation);
        transaction.commit();
        session.close();
    }

    @Override
    public void blockReservationById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("update Reservation r set r.status = false where r.id =:id")
                .setParameter("id",id);
        session.update(query);
        transaction.commit();
        session.close();
    }

    @Override
    public void setUser(long id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("update Reservation set user.id =:id")
                .setParameter("id",id);
        session.update(query);
        transaction.commit();
        session.close();
    }
}
