package org.vironit.edu.dao.interfaces;

import org.vironit.edu.entity.Airport;

public interface AirportDao extends BaseDao<Airport, Integer> {

    Airport getById(int id);

    void delete(Airport airport);
}
