package org.vironit.edu.dao.interfaces;

import org.vironit.edu.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserDao extends BaseDao<User, Long> {
    Optional<User> getByEmail(String email);

    boolean isEmailExist(String email);

    boolean isExist(String email, String password);

    User getById(long id);

    void delete(User user);

    List<User> getAllWhere(String email, String password);

}
