package org.vironit.edu.filter;

import org.vironit.edu.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthorizationFilter implements Filter {

    public void init(FilterConfig fConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        User user = (User) req.getSession().getAttribute("user");
        String url = req.getRequestURI();

        RequestDispatcher requestDispatcher;

        if (user != null || url.endsWith("login") || url.endsWith("signup") || url.endsWith("html") || url.endsWith("css")
                || url.endsWith("jpg") || url.endsWith("js")) {
            chain.doFilter(request, response);
        } else {
            requestDispatcher = req.getRequestDispatcher("do");
            requestDispatcher.forward(req, resp);
        }

    }

    public void destroy() {
    }
}
