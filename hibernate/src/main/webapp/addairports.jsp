<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container" align="center">
    <%@ include file="include/menu.jsp" %>
    <form class="form-horizontal" action="do?command=AddAirports" method="post">
        <fieldset>

            <!-- Form Name -->
            <h3>Add Airports</h3>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Airport Name</label>
                <div class="col-md-4">
                    <input id="name" name="name" type="text" placeholder="" class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="code">Airport Code</label>
                <div class="col-md-4">
                    <input id="code" name="code" type="text" placeholder="" class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="location">Airport Location</label>
                <div class="col-md-4">
                    <input id="location" name="location" type="text" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="submit"></label>
                <div class="col-md-4">
                    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>


        </fieldset>
    </form>
</body>
</html>
