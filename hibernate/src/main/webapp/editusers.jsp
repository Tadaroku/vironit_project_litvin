<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="include/head.htm" %>
<body>
<div class="container">
    <%@ include file="include/menu.jsp" %>

    <div class="container">
        <div class="row">
            <div class=col-md-1>ID</div>
            <div class=col-md-2>Email</div>
            <div class=col-md-1>Password</div>
            <div class=col-md-1>Name</div>
            <div class=col-md-1>Surname</div>
            <div class=col-md-2>Phone</div>
            <div class=col-md-1>Role</div>
        </div>
    </div>

    <div class="container">
        <c:forEach items="${users}" var="user">
            <form class="update-user-${user.id}" action="do?command=EditUsers" method=POST>
                <div class="row">

                    <div class=col-md-1>
                        <input id="id" class="form-control input-md"
                               name="id"
                               value="${user.id}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="email"  class="form-control input-md" name="email"
                               value="${user.email}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="password"  class="form-control input-md"
                               name="password" type="password"
                               value="${user.password}"/>
                    </div>


                    <div class=col-md-1>
                        <input id="name"  class="form-control input-md" name="name"
                               value="${user.name}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="surname"  class="form-control input-md" name="surname"
                               value="${user.surname}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="phone"
                               class="form-control input-md" name="phone"
                               value="${user.phone}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="role" class="form-control input-md" name="role"
                               value="${user.role}"/>
                    </div>

                    <button id="update" value="update" name="update" class="btn btn-success col-md-1">
                        Update
                    </button>

                    <button id="delete" value="delete" name="delete" class="btn btn-danger col-md-1">
                        Delete
                    </button>
                </div>
            </form>
            <p></p>
        </c:forEach>
    </div>

</div>
</body>
