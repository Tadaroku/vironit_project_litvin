package org.vironit.edu.service;

import org.vironit.edu.entity.Airport;

import java.util.List;

public interface AirportService {

    List<Airport> getAll();

    void save(Airport airport);

    void delete(Integer id);

    void update(Airport airport);

    Airport getById(Integer id);
}
