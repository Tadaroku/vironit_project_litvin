package org.vironit.edu.service;

import org.vironit.edu.entity.User;

import java.util.List;

public interface UserService {

    User findByEmail(String email);

    List<User> getAll();

    void save(User user);

    void delete(Long id);

    void update(User user);

    User getById(Long id);

    boolean isEmailExist(String email);

    boolean isExist(String email, String password);

    List<User> getAllWhere(String email, String password);

    void updateProfile(User upUser);

    void updatePassword(User upUser);


}
