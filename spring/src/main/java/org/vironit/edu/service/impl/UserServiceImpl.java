package org.vironit.edu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.vironit.edu.dao.impl.UserDaoImpl;
import org.vironit.edu.dao.interfaces.UserDao;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserDao userDao;

    public UserServiceImpl() {
        userDao = new UserDaoImpl();
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().name()));
        return authorities;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("user with email" + email + " not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority(user));
    }

    @Override
    public User findByEmail(String email) {
        return userDao.getByEmail(email);
    }

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

    @Override
    public void delete(Long id) {
        User user = userDao.getById(id);
        userDao.delete(user);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public User getById(Long id) {
        return userDao.getById(id);
    }

    @Override
    public List<User> getAll() {
        return userDao.getAll();
    }

    @Override
    public boolean isEmailExist(String email) {
        return userDao.isEmailExist(email);
    }

    @Override
    public boolean isExist(String email, String password) {
        return userDao.isExist(email, password);
    }

    @Override
    public List<User> getAllWhere(String email, String password) {
        return userDao.getAllWhere(email, password);
    }

    @Override
    public void updateProfile(User upUser) {
        User user = getById(upUser.getId());
        user.setName(upUser.getName());
        user.setSurname(upUser.getSurname());
        user.setPhone(upUser.getPhone());
        userDao.update(user);
    }

    @Override
    public void updatePassword(User upUser) {
        User user = getById(upUser.getId());
        user.setPassword(bCryptPasswordEncoder.encode(upUser.getPassword()));
        userDao.update(user);
    }

}
