package org.vironit.edu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vironit.edu.dao.interfaces.AirlineDao;
import org.vironit.edu.entity.Airline;
import org.vironit.edu.service.AirlineService;

import java.util.List;

@Service
public class AirlineServiceImpl implements AirlineService {

    @Autowired
    private AirlineDao airlineDao;

    @Override
    public List<Airline> getAll() {
        return airlineDao.getAll();
    }

    @Override
    public void save(Airline airline) {
        airlineDao.save(airline);
    }

    @Override
    public void delete(Integer id) {
        Airline airline = airlineDao.getById(id);
        airlineDao.delete(airline);
    }

    @Override
    public void update(Airline airline) {
        airlineDao.update(airline);
    }

    @Override
    public Airline getById(Integer id) {
        return airlineDao.getById(id);
    }
}
