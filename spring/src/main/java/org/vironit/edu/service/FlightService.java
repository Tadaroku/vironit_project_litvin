package org.vironit.edu.service;

import org.vironit.edu.entity.Flight;

import java.util.List;

public interface FlightService {

    List<Flight> getAll();

    void save(Flight flight);

    void delete(Integer id);

    void update(Flight flight);

    void updateFlight(Flight upFlight);

    Flight getById(Integer id);
}
