package org.vironit.edu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vironit.edu.dao.interfaces.FlightDao;
import org.vironit.edu.entity.Flight;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.service.FlightService;

import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightDao flightDao;

    @Autowired
    private AirportService airportService;

    @Override
    public List<Flight> getAll() {
        return flightDao.getAll();
    }

    @Override
    public void save(Flight flight) {
        flightDao.save(flight);
    }

    @Override
    public void delete(Integer id) {
        Flight flight = flightDao.getById(id);
        flightDao.delete(flight);
    }

    @Override
    public void update(Flight flight) {
        flightDao.update(flight);
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void updateFlight(Flight upFlight) {
        Flight flight = getById(upFlight.getId());
        flight.setDepartureTime(upFlight.getDepartureTime());
        flight.setArrivalTime(upFlight.getArrivalTime());
        flight.setAirplane(upFlight.getAirplane());
        flight.setFlightNumber(upFlight.getFlightNumber());
        flightDao.update(flight);
    }

    @Override
    public Flight getById(Integer id) {
        return flightDao.getById(id);
    }
}
