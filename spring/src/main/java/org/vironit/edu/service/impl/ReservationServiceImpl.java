package org.vironit.edu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vironit.edu.dao.interfaces.ReservationDao;
import org.vironit.edu.entity.Reservation;
import org.vironit.edu.entity.enums.ReservationStatus;
import org.vironit.edu.service.ReservationService;

import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationDao reservationDao;

    @Override
    public List<Reservation> getAll() {
        return reservationDao.getAll();
    }

    @Override
    public void save(Reservation reservation) {
        reservationDao.save(reservation);
    }

    @Override
    public void delete(Integer id) {
        Reservation reservation = reservationDao.getById(id);
        reservationDao.delete(reservation);
    }

    @Override
    public void update(Reservation reservation) {
        reservationDao.update(reservation);
    }

    @Override
    public void cancel(Reservation upReservation) {
        Reservation reservation = getById(upReservation.getId());
        reservation.setUser(null);
        reservation.setReservationStatus(ReservationStatus.FREE);
        reservationDao.update(reservation);
    }

    @Override
    public void add(Reservation upReservation) {
        Reservation reservation = getById(upReservation.getId());
        reservation.setUser(upReservation.getUser());
        reservation.setReservationStatus(ReservationStatus.RESERVED);
        reservationDao.update(reservation);
    }

    @Override
    public void updateReservation(Reservation upReservation) {
        Reservation reservation = getById(upReservation.getId());
        reservation.setReservationDate(upReservation.getReservationDate());
        reservation.setTicketClass(upReservation.getTicketClass());
        reservation.setReservationStatus(upReservation.getReservationStatus());
        reservation.setPrice(upReservation.getPrice());
        reservation.setLuggage(upReservation.isLuggage());
        reservation.setSeatNumber(upReservation.getSeatNumber());
        reservationDao.update(reservation);
    }

    @Override
    public Reservation getById(Integer id) {
        return reservationDao.getById(id);
    }

    @Override
    public List<Reservation> getAllById(long id) {
        return reservationDao.getAllById(id);
    }

    @Override
    public void setUser(long id) {
        reservationDao.setUser(id);
    }
}
