package org.vironit.edu.service;

import org.vironit.edu.entity.Reservation;

import java.util.List;

public interface ReservationService {

    List<Reservation> getAll();

    void save(Reservation reservation);

    void delete(Integer id);

    void update(Reservation reservation);

    void cancel(Reservation upReservation);

    void add(Reservation upReservation);

    void updateReservation(Reservation upReservation);

    Reservation getById(Integer id);

    List<Reservation> getAllById(long id);

    void setUser(long id);
}
