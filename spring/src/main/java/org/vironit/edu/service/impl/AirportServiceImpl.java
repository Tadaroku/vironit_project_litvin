package org.vironit.edu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vironit.edu.dao.interfaces.AirportDao;
import org.vironit.edu.entity.Airport;
import org.vironit.edu.service.AirportService;

import java.util.List;

@Service
public class AirportServiceImpl implements AirportService {

    @Autowired
    private AirportDao airportDao;

    @Override
    public List<Airport> getAll() {
        return airportDao.getAll();
    }

    @Override
    public void save(Airport airport) {
        airportDao.save(airport);
    }

    @Override
    public void delete(Integer id) {
        Airport airport = airportDao.getById(id);
        airportDao.delete(airport);
    }

    @Override
    public void update(Airport airport) {
        airportDao.update(airport);
    }

    @Override
    public Airport getById(Integer id) {
        return airportDao.getById(id);
    }
}
