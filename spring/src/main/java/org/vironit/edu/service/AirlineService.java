package org.vironit.edu.service;

import org.vironit.edu.entity.Airline;

import java.util.List;

public interface AirlineService {

    List<Airline> getAll();

    void save(Airline airline);

    void delete(Integer id);

    void update(Airline airline);

    Airline getById(Integer id);
}
