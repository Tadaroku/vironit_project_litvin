package org.vironit.edu.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.vironit.edu.dto.UserDto;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.UserService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@SuppressWarnings("Duplicates")
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto user = (UserDto) o;

        Matcher emailMatcher = Pattern.compile("^(.+)@(.+)$").matcher(user.getEmail());
        if (!emailMatcher.matches()) {
            errors.rejectValue("email", "email is not correct","email is not correct");
        }
        if (user.getEmail().length() < 4 || user.getEmail().length() > 32) {
            errors.rejectValue("email", "email length is not correct", "email length is not correct");
        }
        if (userService.isEmailExist(user.getEmail())) {
            errors.rejectValue("email", "email is already exists","email is already exists");
        }

        Matcher nameMatcher = Pattern.compile("[a-zA-Zа-яА-ЯёЁ]{2,}").matcher(user.getName());
        if (!nameMatcher.matches()) {
            errors.rejectValue("name", "name is not correct","name is not correct");
        }

        Matcher surnameMatcher = Pattern.compile("[a-zA-Zа-яА-ЯёЁ]{2,}").matcher(user.getSurname());
        if (!surnameMatcher.matches()) {
            errors.rejectValue("surname", "surname is not correct", "surname is not correct");
        }

        Matcher phoneMatcher = Pattern.compile("^\\+375 \\((17|29|33|44|25)\\) [0-9]{3}-[0-9]{2}-[0-9]{2}$").matcher(user.getPhone());
        if (!phoneMatcher.matches()) {
            errors.rejectValue("phone", "phone is not correct", "phone is not correct");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        Matcher passwordMatcher = Pattern.compile("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$").matcher(user.getPassword());
        if (!passwordMatcher.matches()) {
            errors.rejectValue("password", "password is not correct", "password is not correct");
        }
        if (user.getPassword().length() < 4) {
            errors.rejectValue("password", "password length is not correct", "password length is not correct");
        }
        if (!user.getPassword().equals(user.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "password must be identical");
        }
    }
}
