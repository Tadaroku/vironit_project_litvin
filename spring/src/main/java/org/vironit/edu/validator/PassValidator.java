package org.vironit.edu.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.vironit.edu.dto.UserDto;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.UserService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@SuppressWarnings("Duplicates")
public class PassValidator implements Validator {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto user = (UserDto) o;


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        Matcher passwordMatcher = Pattern.compile("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$").matcher(user.getPassword());
        if (!passwordMatcher.matches()) {
            errors.rejectValue("password", "password is not correct", "password is not correct");
        }
        if (user.getPassword().length() < 4) {
            errors.rejectValue("password", "password length is not correct", "password length is not correct");
        }
        if (!user.getPassword().equals(user.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "password must be identical");
        }


    }
}
