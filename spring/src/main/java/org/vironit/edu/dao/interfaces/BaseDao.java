package org.vironit.edu.dao.interfaces;

import java.util.List;

public interface BaseDao<T, ID> {
    void save(T entity);

    void update(T entity);

    List<T> getAll();

    T getById(ID id);

    void delete(T entity);
}
