package org.vironit.edu.dao.interfaces;

import org.vironit.edu.entity.Flight;

public interface FlightDao extends BaseDao<Flight,Integer> {
}
