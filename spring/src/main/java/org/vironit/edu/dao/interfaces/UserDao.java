package org.vironit.edu.dao.interfaces;

import org.vironit.edu.entity.User;

import java.util.List;

public interface UserDao extends BaseDao<User, Long> {

    User getByEmail(String email);

    boolean isEmailExist(String email);

    boolean isExist(String email, String password);

    List<User> getAllWhere(String email, String password);

}
