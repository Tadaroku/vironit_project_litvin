package org.vironit.edu.dao.impl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.vironit.edu.dao.interfaces.UserDao;
import org.vironit.edu.entity.User;

import java.util.List;

@Repository
public class UserDaoImpl extends BaseDaoImpl<User, Long> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    @Transactional
    public User getByEmail(String email) {
        String sql = "from org.vironit.edu.entity.User user where user.email = :email";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("email", email);
        return (User) query.getSingleResult();
    }

    @Override
    @Transactional
    public boolean isEmailExist(String email) {
        String sql = "from org.vironit.edu.entity.User user where user.email = :email";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("email", email);
        return query.list().size() != 0;
    }

    @Override
    @Transactional
    public boolean isExist(String email, String password) {
        String sql = "from org.vironit.edu.entity.User user where user.email = :email and user.password = :password";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("email", email);
        query.setParameter("password", password);
        List<User> users = query.list();
        return users.size() == 0 ? false : true;
    }

    @Override
    @Transactional
    public List<User> getAllWhere(String email, String password) {
        String sql = "from org.vironit.edu.entity.User user  where user.email =:email and user.password =:password";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("email", email);
        query.setParameter("password", password);
        List<User> users = query.list();
        return users;
    }
}
