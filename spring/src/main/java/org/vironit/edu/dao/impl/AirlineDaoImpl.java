package org.vironit.edu.dao.impl;

import org.springframework.stereotype.Repository;
import org.vironit.edu.dao.interfaces.AirlineDao;
import org.vironit.edu.entity.Airline;

@Repository
public class AirlineDaoImpl extends BaseDaoImpl<Airline, Integer> implements AirlineDao {

    public AirlineDaoImpl() {
        super(Airline.class);
    }
}
