package org.vironit.edu.dao.interfaces;

import org.vironit.edu.entity.Reservation;

import java.util.List;

public interface ReservationDao extends BaseDao<Reservation,Integer> {

    List<Reservation> getAllById(long id);

    void setUser(long id);
}
