package org.vironit.edu.dao.interfaces;

import org.vironit.edu.entity.Airline;

public interface AirlineDao extends BaseDao<Airline, Integer> {
}
