package org.vironit.edu.dao.impl;

import org.springframework.stereotype.Repository;
import org.vironit.edu.dao.interfaces.AirportDao;
import org.vironit.edu.entity.Airport;

@Repository
public class AirportDaoImpl extends BaseDaoImpl<Airport,Integer> implements AirportDao {

    public AirportDaoImpl() {
        super(Airport.class);
    }
}
