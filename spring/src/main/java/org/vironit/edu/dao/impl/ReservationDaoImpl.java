package org.vironit.edu.dao.impl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.vironit.edu.dao.interfaces.ReservationDao;
import org.vironit.edu.entity.Reservation;

import java.util.List;

@Repository
public class ReservationDaoImpl extends BaseDaoImpl<Reservation, Integer> implements ReservationDao {

    public ReservationDaoImpl() {
        super(Reservation.class);
    }

    @Override
    @Transactional
    public List<Reservation> getAllById(long id) {
        String sql = "from org.vironit.edu.entity.Reservation where user.id =:id";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("id", id);
        List<Reservation> reservations = query.list();
        return reservations;
    }

    @Override
    @Transactional
    public void setUser(long id) {
        String sql = "update org.vironit.edu.entity.Reservation set user.id =:id";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("id", id);
    }
}
