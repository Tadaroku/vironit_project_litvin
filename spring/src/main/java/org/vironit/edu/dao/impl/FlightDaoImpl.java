package org.vironit.edu.dao.impl;

import org.springframework.stereotype.Repository;
import org.vironit.edu.dao.interfaces.FlightDao;
import org.vironit.edu.entity.Flight;

@Repository
public class FlightDaoImpl extends BaseDaoImpl<Flight, Integer> implements FlightDao {

    public FlightDaoImpl() {
        super(Flight.class);
    }
}
