package org.vironit.edu.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.vironit.edu.dao.interfaces.BaseDao;

import java.io.Serializable;
import java.util.List;

public class BaseDaoImpl<T, ID extends Serializable> implements BaseDao<T, ID> {

    private Class<T> classOfEntity;

    @Autowired
    private SessionFactory sessionFactory;

    public BaseDaoImpl(Class classOfEntity) {
        this.classOfEntity = classOfEntity;
    }

    @Override
    @Transactional
    public void save(T entity) {
        getCurrentSession().save(entity);
    }

    @Override
    @Transactional
    public void update(T entity) {
        getCurrentSession().update(entity);
    }

    @Override
    @Transactional
    public List getAll() {
        String GET_ALL = "from " + classOfEntity.getName();
        List<T> entities = (List<T>) getCurrentSession().createQuery(GET_ALL).list();
        return entities;
    }

    @Override
    @Transactional
    public T getById(ID id) {
        T entity = (T) getCurrentSession().get(classOfEntity.getName(), id);
        return entity;
    }

    @Override
    @Transactional
    public void delete(T entity) {
        getCurrentSession().delete(entity);
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
