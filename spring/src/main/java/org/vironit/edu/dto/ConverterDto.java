package org.vironit.edu.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vironit.edu.dao.interfaces.*;
import org.vironit.edu.entity.Flight;
import org.vironit.edu.entity.Reservation;
import org.vironit.edu.entity.User;
import org.vironit.edu.entity.enums.ReservationStatus;
import org.vironit.edu.entity.enums.RoleName;
import org.vironit.edu.entity.enums.TicketClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Collections;

@SuppressWarnings("Duplicates")
@Service
public class ConverterDto  {

    private final UserDao userDao;

    @Autowired
    private FlightDao flightDao;

    @Autowired
    private AirportDao airportDao;

    @Autowired
    private ReservationDao reservationDao;

    @Autowired
    private AirlineDao airlineDao;

    @Autowired
    public ConverterDto(UserDao userDao) {
        this.userDao = userDao;
    }

    public User toUser(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setPassword(userDto.getConfirmPassword());
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setPhone(userDto.getPhone());
        if (userDto.getRole()!=null) {
            user.setRole(RoleName.valueOf(userDto.getRole()));
        }
        return user;
    }

    public UserDto fromUser(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setConfirmPassword(user.getPassword());
        userDto.setName(user.getName());
        userDto.setSurname(user.getSurname());
        userDto.setPhone(user.getPhone());
        if (user.getRole()!=null) {
            userDto.setRole(user.getRole().name());
        }
        return userDto;
    }

    public Flight toFlight(FlightDto flightDto) throws ParseException {
        Flight flight = new Flight();
        flight.setId(flightDto.getId());
        String pattern = "yyyy-MM-dd HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        flight.setDepartureTime(simpleDateFormat.parse(flightDto.getDepartureTime()));
        flight.setArrivalTime(simpleDateFormat.parse(flightDto.getArrivalTime()));
        flight.setFlightNumber(flightDto.getFlightNumber());
        flight.setAirplane(flightDto.getAirplane());
        flight.setAirportFrom(airportDao.getById(Integer.valueOf(flightDto.getAirportFrom())));
        flight.setAirportTo(airportDao.getById(Integer.valueOf(flightDto.getAirportTo())));

        if (flightDto.getReservations()!=null) {
            flight.setReservations(Collections.singletonList(reservationDao.getById(Integer.valueOf(flightDto.getReservations()))));
        }
        return flight;
    }

    public FlightDto fromFlight(Flight flight) {
        FlightDto flightDto = new FlightDto();
        flightDto.setId(flight.getId());
        flightDto.setDepartureTime(flight.getDepartureTime().toString());
        flightDto.setArrivalTime(flight.getArrivalTime().toString());
        flightDto.setAirplane(flight.getAirplane());
        flightDto.setFlightNumber(flight.getFlightNumber());
        flightDto.setAirportFrom(String.valueOf(flight.getAirportFrom().getId()));
        flightDto.setAirportTo(String.valueOf(flight.getAirportTo().getId()));


        if (flight.getReservations()!=null) {
            flightDto.setReservations(String.valueOf(flight.getReservations()));
        }
        return flightDto;
    }

    public Reservation toReservation(ReservationDto reservationDto){
        Reservation reservation = new Reservation();
        reservation.setId(reservationDto.getId());
        reservation.setReservationDate(LocalDate.parse(reservationDto.getReservationDate()));
        reservation.setTicketClass(TicketClass.valueOf(reservationDto.getTicketClass()));
        reservation.setReservationStatus(ReservationStatus.valueOf(reservationDto.getReservationStatus()));
        reservation.setSeatNumber(Integer.parseInt(reservationDto.getSeatNumber()));
        reservation.setLuggage(reservationDto.isLuggage());
        reservation.setPrice(Double.valueOf(reservationDto.getPrice()));
        if (reservationDto.getAirline() != null) {
            reservation.setAirline(airlineDao.getById(Integer.valueOf(reservationDto.getAirline())));
        }
        if (reservationDto.getFlight() != null) {
            reservation.setFlight(flightDao.getById(Integer.parseInt(reservationDto.getFlight())));
        }
        if (reservationDto.getUser() != null) {
            reservation.setUser(userDao.getById(Long.parseLong(reservationDto.getUser())));
        }
        return reservation;
    }

    public ReservationDto fromReservation(Reservation reservation) {
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(reservation.getId());
        reservationDto.setReservationDate(reservation.getReservationDate().toString());
        reservationDto.setLuggage(reservation.isLuggage());
        reservationDto.setPrice(reservation.getPrice().toString());
        reservationDto.setTicketClass(reservation.getTicketClass().name());
        reservationDto.setReservationStatus(reservation.getReservationStatus().name());
        reservationDto.setSeatNumber(String.valueOf(reservation.getSeatNumber()));
        if (reservation.getAirline() != null) {
            reservationDto.setAirline(String.valueOf(reservation.getAirline()));
        }
        if (reservation.getFlight() != null) {
            reservationDto.setFlight(String.valueOf(reservation.getFlight()));
        }
        if (reservation.getUser() != null) {
            reservationDto.setUser(String.valueOf(reservation.getUser()));
        }
        return reservationDto;
    }

}
