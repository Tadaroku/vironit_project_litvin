package org.vironit.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.vironit.edu.dto.ConverterDto;
import org.vironit.edu.dto.FlightDto;
import org.vironit.edu.dto.ReservationDto;
import org.vironit.edu.entity.*;
import org.vironit.edu.service.*;

import java.text.ParseException;

@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private AirlineService airlineService;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private FlightService flightService;

    @Autowired
    private AirportService airportService;

    @Autowired
    private ConverterDto converterDto;

    @GetMapping("/admin")
    public String welcomeAdmin(Model model) {
        return "admin";
    }

    @GetMapping("/admin/edit/users")
    public String editUsers(Model model) {
        model.addAttribute("user",userService.getAll());

        return "editUsers";
    }

    @GetMapping("/admin/usersWithReservations")
    public String reservationList(Model model) {
        model.addAttribute("reservations", reservationService.getAll());
//        model.addAttribute("user");
        return "usersWithReservations";
    }

    @PostMapping("/admin/cancelReservation/{id}")
    public String cancelReservation(@PathVariable("id") int id, @ModelAttribute("reservation") Reservation reservation) {
        reservationService.cancel(reservation);
        return "cancel";
    }


    @GetMapping("/admin/airlines")
    public String airlinesPage(Model model) {
        return "airlines";
    }

    @PostMapping("/admin/edit/users")
    public String editUsersInfo(@ModelAttribute("user") User user) {
        userService.delete(user.getId());
        return "redirect: /admin/edit/users";
    }

    @GetMapping("/admin/airlines/delete")
    public String deleteAirlines(Model model) {
        model.addAttribute("airline",airlineService.getAll());

        return "deleteAirlines";
    }

    @GetMapping("/admin/airlines/update")
    public String updateAirlines(Model model) {
        model.addAttribute("airline",airlineService.getAll());

        return "updateAirlines";
    }

    @PostMapping("/admin/airlines/delete")
    public String deleteAirlinesInfo(@ModelAttribute("airline") Airline airline) {
        airlineService.delete(airline.getId());
        return "redirect: /admin/airlines/delete";
    }

    @PostMapping("/admin/airlines/update")
    public String updateAirlinesInfo(@ModelAttribute("airline") Airline airline) {
        airlineService.update(airline);
        return "redirect: /admin/airlines/update";
    }

    @GetMapping("/admin/airlines/add")
    public String addAirlines(Model model) {
        model.addAttribute("airline",airlineService.getAll());

        return "addAirlines";
    }

    @PostMapping("/admin/airlines/add")
    public String addAirlinesInfo(@ModelAttribute("airline") Airline airline) {
        airlineService.save(airline);
        return "redirect: /admin/airlines/delete";
    }



    @GetMapping("/admin/airports")
    public String airportsPage(Model model) {
        return "airports";
    }



    @GetMapping("/admin/airports/delete")
    public String deleteAirports(Model model) {
        model.addAttribute("airport",airportService.getAll());

        return "deleteAirports";
    }

    @GetMapping("/admin/airports/update")
    public String updateAirports(Model model) {
        model.addAttribute("airport",airportService.getAll());

        return "updateAirports";
    }

    @PostMapping("/admin/airports/delete")
    public String deleteAirportsInfo(@ModelAttribute("airport") Airport airport) {
        airportService.delete(airport.getId());
        return "redirect: /admin/airports/delete";
    }

    @PostMapping("/admin/airports/update")
    public String updateAirportsInfo(@ModelAttribute("airport") Airport airport) {
        airportService.update(airport);
        return "redirect: /admin/airports/update";
    }

    @GetMapping("/admin/airports/add")
    public String addAirports(Model model) {
        model.addAttribute("airport",airportService.getAll());

        return "addAirports";
    }

    @PostMapping("/admin/airports/add")
    public String addAirportsInfo(@ModelAttribute("airport") Airport airport) {
        airportService.save(airport);
        return "redirect: /admin/airports/delete";
    }


    @GetMapping("/admin/flights")
    public String flightsPage(Model model) {
        return "flights";
    }


    @GetMapping("/admin/flights/delete")
    public String deleteFlights(Model model) {
        model.addAttribute("flight",flightService.getAll());

        return "deleteFlights";
    }

    @GetMapping("/admin/flights/update")
    public String updateFlights(Model model) {
        model.addAttribute("flight",flightService.getAll());

        return "updateFlights";
    }

    @PostMapping("/admin/flights/delete")
    public String deleteFlightsInfo(@ModelAttribute("flight") Flight flight) {
        flightService.delete(flight.getId());
        return "redirect: /admin/flights/delete";
    }

    @PostMapping("/admin/flights/update")
    public String updateFlightsInfo(@ModelAttribute("flight") Flight flight) {
        flightService.updateFlight(flight);
        return "redirect: /admin/flights/update";
    }

    @GetMapping("/admin/flights/add")
    public String addFlights(Model model) {
        model.addAttribute("flight",flightService.getAll());

        return "addFlights";
    }

    @PostMapping("/admin/flights/add")
    public String addFlightsInfo(@ModelAttribute(value = "flight") FlightDto flightDto)
            throws ParseException {
        flightService.save(converterDto.toFlight(flightDto));
        return "redirect: /admin/flights/delete";
    }


    @GetMapping("/admin/reservations")
    public String reservationsPage(Model model) {
        return "reservations";
    }



    @GetMapping("/admin/reservations/delete")
    public String deleteReservations(Model model) {
        model.addAttribute("reservation",reservationService.getAll());

        return "deleteReservations";
    }

    @GetMapping("/admin/reservations/update")
    public String updateReservations(Model model) {
        model.addAttribute("reservation",reservationService.getAll());

        return "updateReservations";
    }

    @PostMapping("/admin/reservations/delete")
    public String deleteReservationsInfo(@ModelAttribute("reservation") Reservation reservation) {
        reservationService.delete(reservation.getId());
        return "redirect: /admin/reservations/delete";
    }

    @PostMapping("/admin/reservations/update")
    public String updateReservationsInfo(@ModelAttribute("reservation") Reservation reservation) {
        reservationService.updateReservation(reservation);
        return "redirect: /admin/reservations/update";
    }

    @GetMapping("/admin/reservations/add")
    public String addReservations(Model model) {
        model.addAttribute("reservation",reservationService.getAll());

        return "addReservations";
    }

    @PostMapping("/admin/reservations/add")
    public String addReservationsInfo(@ModelAttribute(value = "reservation") ReservationDto reservationDto) {
        reservationService.save(converterDto.toReservation(reservationDto));
        return "redirect: /admin/reservations/delete";
    }

}
