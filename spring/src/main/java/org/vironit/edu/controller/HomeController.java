package org.vironit.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.vironit.edu.entity.Reservation;
import org.vironit.edu.entity.User;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.service.UserService;

import java.security.Principal;

@Controller
public class HomeController {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private UserService userService;

    @GetMapping("/home")
    public String welcomeHome(Model model) {
        return "home";
    }

    @GetMapping("/aboutus")
    public String aboutUs(Model model) {
        return "aboutus";
    }


    @GetMapping("/cancel")
    public String cancel(Model model) {
        return "cancel";
    }

    @GetMapping("/reservationlist")
    public String reservationList(Model model) {
        model.addAttribute("reservations",reservationService.getAll());
//        model.addAttribute("user");
        return "reservationlist";
    }

    @PostMapping("/reservationlist/addReservation/{id}")
    public String addReservation(@PathVariable("id") int id, @ModelAttribute("reservation") Reservation reservation, @ModelAttribute("user") User user, Principal principal) {
        user.setId(userService.findByEmail(principal.getName()).getId());
        reservation.setUser(user);
        reservationService.add(reservation);
        return "success";
    }

}
