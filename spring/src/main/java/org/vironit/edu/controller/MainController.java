package org.vironit.edu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @GetMapping("/403")
    public String accessDenied(Model model) {
        return "403";
    }
}
