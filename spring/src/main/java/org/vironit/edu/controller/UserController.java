package org.vironit.edu.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.vironit.edu.dto.ConverterDto;
import org.vironit.edu.dto.UserDto;
import org.vironit.edu.entity.Reservation;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.service.UserService;
import org.vironit.edu.validator.HelpValidator;
import org.vironit.edu.validator.PassValidator;
import org.vironit.edu.validator.UserValidator;

import java.security.Principal;

@SuppressWarnings("Duplicates")
@Controller
public class UserController {
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private HelpValidator helpValidator;

    @Autowired
    private PassValidator passValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ConverterDto converterDto;

    @PostMapping("/signup")
    public String signup(@ModelAttribute("user") UserDto user, BindingResult bindingResult, Model model) {
        userValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            logger.info("Binding result sign up " + bindingResult);
            return "signup";
        }
        user.setRole("USER");
        userService.save(converterDto.toUser(user));
        return "redirect: /login";
    }

    @GetMapping("/profile")
    public String profile(Model model, Principal principalUser) {
        UserDto user = converterDto.fromUser(userService.findByEmail(principalUser.getName()));
        model.addAttribute("user",user);
        model.addAttribute("reservations",reservationService.getAllById(user.getId()));
        return "profile";
    }

    @PostMapping("/profile/edit")
    public String editProfileInfo(@ModelAttribute("user") UserDto user, BindingResult bindingResult, Principal principal) {
        helpValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            logger.info("Binding result edit " + bindingResult);
            return "editProfile";
        }
        user.setId(userService.findByEmail(principal.getName()).getId());
        userService.updateProfile(converterDto.toUser(user));
        return "redirect: /profile";
    }

    @GetMapping("/profile/edit")
    public String editProfile(Model model, Principal principalUser) {
        UserDto user = converterDto.fromUser(userService.findByEmail(principalUser.getName()));
        model.addAttribute("user",user);
        return "editProfile";
    }

    @GetMapping("/profile/pass")
    public String editPass(Model model, Principal principalUser) {
//        UserDto user = converterDto.fromUser(userService.findByEmail(principalUser.getName()));
        model.addAttribute("user",new UserDto());
        return "editPass";
    }

    @PostMapping("/profile/pass")
    public String changePass(@ModelAttribute("user") UserDto user, BindingResult bindingResult, Principal principal) {
        passValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            logger.info("Binding result edit " + bindingResult);
            return "editPass";
        }
        user.setId(userService.findByEmail(principal.getName()).getId());
        userService.updatePassword(converterDto.toUser(user));
        return "redirect: /profile/pass/password";
    }

    @GetMapping("/login")
    public String login(Model model, @ModelAttribute("error")String error) {
        if (error != null && !error.isEmpty()) {
            model.addAttribute(error, error);
        }
        return "login";
    }

    @GetMapping("/profile/pass/password")
    public String pass(Model model) {
        return "password";
    }


    @GetMapping("/signup")
    public String signup(Model model) {
        model.addAttribute("user",new UserDto());
        return "signup";
    }

    @PostMapping("/cancelReservation/{id}")
    public String cancelReservation(@PathVariable("id") int id, @ModelAttribute("reservation") Reservation reservation) {
        reservationService.cancel(reservation);
        return "cancel";
    }


}
