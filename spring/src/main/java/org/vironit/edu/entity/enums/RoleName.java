package org.vironit.edu.entity.enums;

public enum RoleName {
    ADMIN,
    USER
}
