package org.vironit.edu.entity.enums;

public enum ReservationStatus {
    FREE,
    RESERVED
}
