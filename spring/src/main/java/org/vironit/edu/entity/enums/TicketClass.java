package org.vironit.edu.entity.enums;

public enum TicketClass {
    GENERAL,
    BUSINESS,
    ECONOM
}
