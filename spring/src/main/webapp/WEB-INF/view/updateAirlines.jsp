<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>

    <div class="container">
        <div class="row">
            <div class=col-md-1>ID</div>
            <div class=col-md-2>Airline Name</div>
        </div>
    </div>

    <div class="container">
        <c:forEach items="${airline}" var="airline">
            <form class="update-user-${airline.id}" action="/admin/airlines/update" method=POST>
                <div class="row">
                    <div class=col-md-1>
                        <input id="id" class="form-control input-md"
                               name="id"
                               value="${airline.id}"/>
                    </div>
                    <div class=col-md-2>
                        <input id="name" class="form-control input-md" name="name"
                               value="${airline.name}"/>
                    </div>

                        <form id="updateAirlines" method="POST" action="/admin/airlines/update">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button id="update" value="update" name="update" class="btn btn-success col-md-2">
                        Update
                        </button>
                        </form>


                </div>
            </form>
            <p></p>
        </c:forEach>
    </div>
    <h2><a href="/admin/airlines">Airlines Page</a></h2>

</div>
</body>
