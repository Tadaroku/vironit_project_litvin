<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<link rel="stylesheet" href="/resources/css/error.css"/>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>


    <form:form id="signup" action="/signup" method="post" modelAttribute="user">
        <fieldset>

            <!-- Form Name -->
            <legend>Form SignUp</legend>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="email">Email</label>
                <div class="col-md-4">
                    <form:errors path="email" cssClass="error"/>
                    <form:input path="email" id="email" name="email" value="" type="text" aria-describedby="emailHelp" placeholder="Enter your email"
                           pattern="(.+)@(.+)$"
                           title="Email example: email@email.com"
                           class="form-control input-md" required=""/>
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>
                <div class="col-md-4">
                    <form:errors path="password" cssClass="error"/>
                    <form:input path="password" id="password" name="password" value="" type="password" placeholder="Enter your password"
                           pattern="[a-zA-Z0-9а-яА-ЯёЁ]{4,}"
                           title="Password must contain at least 1 letter and minimum 4 characters"
                           class="form-control input-md" required=""/>

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="repeatPassword">Repeat Password</label>
                <div class="col-md-4">
                    <form:errors path="confirmPassword" cssClass="error"/>
                    <form:input path="confirmPassword" id="repeatPassword" name="repeatPassword" value="" type="password"
                           placeholder="Confirm your password" pattern="[a-zA-Z0-9а-яА-ЯёЁ]{4,}"
                           title="Password must contain at least 1 letter and minimum 4 characters"
                           class="form-control input-md" required=""/>
                    <span id="password-message" style="color: red"></span>

                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-4">
                    <form:errors path="name" cssClass="error"/>
                    <form:input path="name" id="name" name="name" value="" type="text" placeholder="Enter your name"
                           pattern="[a-zA-Zа-яА-ЯёЁ]{2,}"
                           title="Name must contain at least 2 characters"
                           class="form-control input-md" required=""/>

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="surname">Surname</label>
                <div class="col-md-4">
                    <form:errors path="surname" cssClass="error"/>
                    <form:input path="surname" id="surname" name="surname" value="" type="text" placeholder="Enter your surname"
                           pattern="[a-zA-Zа-яА-ЯёЁ]{2,}"
                           title="Surname must contain at least 2 characters"
                           class="form-control input-md" required=""/>

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="phone">Phone number</label>
                <div class="col-md-4">
                    <form:errors path="phone" cssClass="error"/>
                    <form:input path="phone" id="phone" name="phone" value="+375 ()" type="text" placeholder="+375 (XX) XXX-XX-XX"
                           pattern="^\+375 \((17|29|33|44|25)\) [0-9]{3}-[0-9]{2}-[0-9]{2}$"
                           title="Phone example: +375 (17|29|33|44|25) 111-11-11"
                           class="input-medium bfh-phone" required=""/>

                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="signupsubmit"></label>
                <div class="col-md-4">
                    <button id="signupsubmit" name="signupsubmit" class="btn btn-success">SignUp</button>
                </div>
            </div>

        </fieldset>
</form:form>


</div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="resources/js/check.js"></script>
</html>




