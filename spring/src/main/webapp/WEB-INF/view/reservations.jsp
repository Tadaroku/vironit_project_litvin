<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>

    <form class="form-horizontal" action="/admin/reservations">
        <fieldset>

            <!-- Form Name -->
            <legend>Airline panel</legend>

            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/reservations/add" role="button">Add Reservations</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/reservations/delete" role="button">Delete Reservations</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/reservations/update" role="button">Update Reservations</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin" role="button">Admin Page</a></p>
            </div>
        </fieldset>
    </form>

</div>
</body>
</html>
