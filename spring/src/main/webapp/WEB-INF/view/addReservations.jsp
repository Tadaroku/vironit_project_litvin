<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container" align="center">
    <%@ include file="menu.jsp" %>
    <form class="form-horizontal" action="/admin/reservations/add" method="post">
        <fieldset>

            <!-- Form Name -->
            <h3>Add Reservations</h3>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="luggage">Luggage</label>
                <div class="col-md-4">
                    <input id="luggage" name="luggage" type="text" value="true" placeholder=""
                           class="form-control input-md"
                           required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="price">Price</label>
                <div class="col-md-4">
                    <input id="price" name="price" type="text" placeholder="" value="200.0"
                           class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="seatNumber">Seat Number</label>
                <div class="col-md-4">
                    <input id="seatNumber" name="seatNumber" type="text" placeholder="" value="20" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="ticketClass">Ticket Class</label>
                <div class="col-md-4">
                    <input id="ticketClass" name="ticketClass" type="text" value="GENERAL" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="reservationDate">Reservation Date</label>
                <div class="col-md-4">
                    <input id="reservationDate" name="reservationDate" value="2019-07-05" type="text" placeholder=""
                           class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="reservationStatus">Status</label>
                <div class="col-md-4">
                    <input id="reservationStatus" name="reservationStatus" type="text" placeholder="" value="FREE"
                           class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="flight">Flight</label>
                <div class="col-md-4">
                    <input id="flight" name="flight" value="8" type="text" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="airline">Airline</label>
                <div class="col-md-4">
                    <input id="airline" name="airline" type="text" value="5" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <form id="addReservations" method="POST" action="/admin/reservations/add">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>


        </fieldset>
    </form>
    <h2><a href="/admin/reservations">Reservations Page</a></h2>
</body>
</html>
