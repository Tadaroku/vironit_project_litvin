<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>
<form:form id="edit" action="/profile/edit" method="post" modelAttribute="user">
    <fieldset>

        <!-- Form Name -->
        <legend>Edit Profile</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
                <form:errors path="name" cssClass="error"/>
                <form:input path="name" id="name" name="name" value="${user.name}" type="text" placeholder="Enter your name"
                            pattern="[a-zA-Zа-яА-ЯёЁ]{2,}"
                            title="Name must contain at least 2 characters"
                            class="form-control input-md" required=""/>

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="surname">Surname</label>
            <div class="col-md-4">
                <form:errors path="surname" cssClass="error"/>
                <form:input path="surname" id="surname" name="surname" value="${user.surname}" type="text" placeholder="Enter your surname"
                            pattern="[a-zA-Zа-яА-ЯёЁ]{2,}"
                            title="Surname must contain at least 2 characters"
                            class="form-control input-md" required=""/>

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="phone">Phone number</label>
            <div class="col-md-4">
                <form:errors path="phone" cssClass="error"/>
                <form:input path="phone" id="phone" name="phone" value="${user.phone}" type="text" placeholder="+375 (XX) XXX-XX-XX"
                            pattern="^\+375 \((17|29|33|44|25)\) [0-9]{3}-[0-9]{2}-[0-9]{2}$"
                            title="Phone example: +375 (17|29|33|44|25) 111-11-11"
                            class="input-medium bfh-phone" required=""/>

            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="signupsubmit"></label>
            <div class="col-md-4">
                <button id="signupsubmit" name="signupsubmit" class="btn btn-success">Edit</button>
            </div>
        </div>

    </fieldset>
</form:form>
</div>
</body>
</html>
