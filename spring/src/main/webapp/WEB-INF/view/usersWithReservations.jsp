<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>

    <h2>Reservation list:</h2>
        <c:forEach var="reservation" items="${reservations}">
                <c:if test="${reservation.user.id!=null}">
        <div class="row">
            <div class="col-1">
            </div>

            <div class="col">
                <div class="media shadow p-3 mb-5 bg-white rounded">
                    <div class="media-body">
                        <h5 class="mt-0">Users with reservations</h5>


                        <p>
                            <span class="font-weight-bold">Email:</span> ${reservation.user.email}<br>
                            <span class="font-weight-bold">Name:</span> ${reservation.user.name}<br>
                            <span class="font-weight-bold">Surname:</span> ${reservation.user.surname}<br>
                            <span class="font-weight-bold">Phone:</span> ${reservation.user.phone}<br>
                            <span class="font-weight-bold">Luggage:</span> ${reservation.luggage? '+' : '-'}<br>
                            <span class="font-weight-bold">Price:</span> ${reservation.price}<br>
                            <span class="font-weight-bold">Seat:</span> ${reservation.ticketClass}<br>
                            <span class="font-weight-bold">Seat Number:</span> ${reservation.seatNumber}<br>
                            <span class="font-weight-bold">Reservation Date:</span> ${reservation.reservationDate}<br>
                            <span class="font-weight-bold">Status:</span> ${reservation.reservationStatus}<br>
                            <span class="font-weight-bold">Airline:</span> ${reservation.airline.name}<br>
                            <span class="font-weight-bold">Departure Time:</span> <fmt:formatDate value="${reservation.flight.departureTime}" pattern="dd-MM-yyyy HH:mm" /><br>
                            <span class="font-weight-bold">Arrival Time:</span> <fmt:formatDate value="${reservation.flight.arrivalTime}" pattern="dd-MM-yyyy HH:mm" /><br>
                            <span class="font-weight-bold">Airplane:</span> ${reservation.flight.airplane}<br>
                            <span class="font-weight-bold">Flight Number:</span> ${reservation.flight.flightNumber}<br>
                            <span class="font-weight-bold">Airport From:</span> ${reservation.flight.airportFrom.name}<br>
                            <span class="font-weight-bold">Location:</span> ${reservation.flight.airportFrom.location}<br>
                            <span class="font-weight-bold">Code:</span> ${reservation.flight.airportFrom.code}<br>
                            <span class="font-weight-bold">Airport To:</span> ${reservation.flight.airportTo.name}<br>
                            <span class="font-weight-bold">Location:</span> ${reservation.flight.airportTo.location}<br>
                            <span class="font-weight-bold">Code:</span> ${reservation.flight.airportTo.code}<br>

                    </div>
                    <form id="cancelReservations" method="POST" action="/admin/cancelReservation/${reservation.id}">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button id="update" value="update" name="update" class="btn btn-success">
                            Cancel
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-1">
            </div>
        </div>
            </c:if>
        </c:forEach>
</div>
</body>
</html>
