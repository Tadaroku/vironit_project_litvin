<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>

    <form class="form-horizontal" action="/admin/flights">
        <fieldset>

            <!-- Form Name -->
            <legend>Flight panel</legend>

            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/flights/add" role="button">Add Flights</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/flights/delete" role="button">Delete Flights</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/flights/update" role="button">Update Flights</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin" role="button">Admin Page</a></p>
            </div>
        </fieldset>
    </form>

</div>
</body>
</html>
