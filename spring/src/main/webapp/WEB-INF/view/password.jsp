<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>

    <div class="jumbotron text-xs-center">
        <h1 class="display-3">Your password was changed!</h1>
        <p class="lead">
            <a class="btn btn-primary btn-sm" href="${pageContext.request.contextPath}/profile" role="button">Go To Your Profile</a>
        </p>
    </div>

</div>
</body>
</html>
