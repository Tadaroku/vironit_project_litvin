<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>

    <form class="form-horizontal" action="/admin">
        <fieldset>

            <!-- Form Name -->
            <legend>Admin panel</legend>


            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="${pageContext.request.contextPath}/admin/edit/users" role="button">Edit Users</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="${pageContext.request.contextPath}/admin/airlines" role="button">Edit Airlines</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="${pageContext.request.contextPath}/admin/airports" role="button">Edit Airports</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="${pageContext.request.contextPath}/admin/flights" role="button">Edit Flights</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="${pageContext.request.contextPath}/admin/reservations" role="button">Edit Reservations</a></p>
            </div>
        </fieldset>
    </form>

</div>
</body>
</html>
