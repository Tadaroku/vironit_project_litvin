<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>

    <form class="form-horizontal" action="/admin/airports">
        <fieldset>

            <!-- Form Name -->
            <legend>Airport panel</legend>

            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/airports/add" role="button">Add Airports</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/airports/delete" role="button">Delete Airports</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/airports/update" role="button">Update Airports</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin" role="button">Admin Page</a></p>
            </div>
        </fieldset>
    </form>

</div>
</body>
</html>
