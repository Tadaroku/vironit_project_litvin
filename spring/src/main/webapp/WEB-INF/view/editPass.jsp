<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<link rel="stylesheet" href="/resources/css/error.css"/>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>
<form:form id="pass" action="/profile/pass" method="post" modelAttribute="user">
    <fieldset>

        <!-- Form Name -->
        <legend>Change Password</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                <form:errors path="password" cssClass="error"/>
                <form:input path="password" id="password" name="password" value=""  type="text" placeholder="Enter your password"
                            pattern="[a-zA-Z0-9а-яА-ЯёЁ]{4,}"
                            title="Password must contain at least 1 letter and minimum 4 characters"
                            class="form-control input-md" required=""/>

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="repeatPassword">Repeat Password</label>
            <div class="col-md-4">
                <form:errors path="confirmPassword" cssClass="error"/>
                <form:input path="confirmPassword" id="repeatPassword" name="repeatPassword" value="" type="text"
                            placeholder="Confirm your password" pattern="[a-zA-Z0-9а-яА-ЯёЁ]{4,}"
                            title="Password must contain at least 1 letter and minimum 4 characters"
                            class="form-control input-md" required=""/>
                <span id="password-message" style="color: red"></span>

            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="signupsubmit"></label>
            <div class="col-md-4">
                <button id="signupsubmit" name="signupsubmit" class="btn btn-success">Change Password</button>
            </div>
        </div>

    </fieldset>
</form:form>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="/resources/js/check.js"></script>
</html>
