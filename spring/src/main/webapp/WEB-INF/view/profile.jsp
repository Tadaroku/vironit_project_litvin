<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>
    <h2>Profile Data</h2>
    <form class="form-horizontal" method="post" action="/profile">
        <fieldset>

            <!-- Form Name -->
            <legend>Sign Up Form</legend>


            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-4">
                    <input id="name" name="name" type="text" disabled placeholder="" class="form-control input-md"
                           value="${user.name}">
                    <span class="help-block">${help_name}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="surname">Surname</label>
                <div class="col-md-4">
                    <input id="surname" name="surname" type="text" disabled placeholder="" class="form-control input-md"
                           value="${user.surname}">
                    <span class="help-block">${help_surname}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="phone">Phone</label>
                <div class="col-md-4">
                    <input id="phone" name="phone" type="text" disabled placeholder="" class="form-control input-md"
                           value="${user.phone}">
                    <span class="help-block">${help_phone}</span>
                </div>
            </div>
        </fieldset>
        </form>

        <c:forEach var="reservation" items="${reservations}">
    <h2>Reservation Info</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Luggage</th>
            <th scope="col">Price</th>
            <th scope="col">Seat</th>
            <th scope="col">Seat Number</th>
            <th scope="col">Reservation Date</th>
            <th scope="col">Status</th>
            <th scope="col">Airline</th>

        </tr>
        </thead>
        <tbody>
            <tr>
                <td>${reservation.luggage? '+' : '-'}</td>
                <td>${reservation.price}</td>
                <td>${reservation.ticketClass}</td>
                <td>${reservation.seatNumber}</td>
                <td>${reservation.reservationDate}</td>
                <td>${reservation.reservationStatus}</td>
                <td>${reservation.airline.name}</td>
            </tr>


        </tbody>
    </table>

    <%--<h2>Flight Info</h2>--%>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Departure Time</th>
            <th scope="col">Arrival Time</th>
            <th scope="col">Airport From</th>
            <th scope="col">Location From</th>
            <th scope="col">Airport To</th>
            <th scope="col">Location To</th>

        </tr>
        </thead>
        <tbody>
            <tr>
                <td><fmt:formatDate value="${reservation.flight.departureTime}" pattern="dd-MM-yyyy HH:mm"/></td>
                <td><fmt:formatDate value="${reservation.flight.departureTime}" pattern="dd-MM-yyyy HH:mm"/></td>
                <td>${reservation.flight.airportFrom.name}</td>
                <td>${reservation.flight.airportFrom.location}</td>
                <td>${reservation.flight.airportTo.name}</td>
                <td>${reservation.flight.airportTo.location}</td>
            </tr>
        </tbody>
    </table>
            <form id="updateReservations" method="POST" action="/cancelReservation/${reservation.id}">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button id="update" value="update" name="update" class="btn btn-success">
                    Cancel
                </button>
            </form>
        </c:forEach>

    <form id="logout" method="POST" action="/logout">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

        <fieldset>

            <!-- Form Name -->
            <h2>Logout</h2>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="logout"></label>
                <div class="col-md-4">
                    <button id="logoutButton" name="logout" value="1" class="btn btn-success">Logout</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
<footer class="page-footer font-small unique-color-dark pt-4">
    <div class="footer-copyright text-center py-3">© 2019 Copyright: LE Aviasales </div>
</footer>
</body>
</html>
