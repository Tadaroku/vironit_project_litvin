<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>

    <form class="form-horizontal" action="/admin/airlines">
        <fieldset>

            <!-- Form Name -->
            <legend>Airline panel</legend>

            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/airlines/add" role="button">Add Airlines</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/airlines/delete" role="button">Delete Airlines</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin/airlines/update" role="button">Update Airlines</a></p>
            </div>
            <div class="jumbotron">
                <p><a class="btn btn-lg btn-success" href="/admin" role="button">Admin Page</a></p>
            </div>
        </fieldset>
    </form>

</div>
</body>
</html>
