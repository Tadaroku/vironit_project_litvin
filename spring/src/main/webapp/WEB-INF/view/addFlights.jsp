<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container" align="center">
    <%@ include file="menu.jsp" %>
    <form class="form-horizontal" action="/admin/flights/add" method="post">
        <fieldset>

            <!-- Form Name -->
            <h3>Add Flights</h3>

            <div class="form-group">
                <label class="col-md-4 control-label" for="departureTime">Departure Time</label>
                <div class="col-md-4">
                    <input id="departureTime" name="departureTime" type="text" value="2019-11-09 10:30" placeholder="" class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="arrivalTime">Arrival Time</label>
                <div class="col-md-4">
                    <input id="arrivalTime" name="arrivalTime" type="text" value="2019-11-09 10:30" placeholder="" class="form-control input-md" required="">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="airplane">Airplane Name</label>
                <div class="col-md-4">
                    <input id="airplane" name="airplane" type="text" value="Boing" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="flightNumber">Flight Number</label>
                <div class="col-md-4">
                    <input id="flightNumber" name="flightNumber" type="text" value="220" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="airportFrom">Airport From</label>
                <div class="col-md-4">
                    <input id="airportFrom" name="airportFrom" type="text" value="2" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="airportTo">Airport To</label>
                <div class="col-md-4">
                    <input id="airportTo" name="airportTo" type="text" value="3" placeholder="" class="form-control input-md"
                           required="">
                </div>
            </div>

            <form id="addAirlines" method="POST" action="/admin/flights/add">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>


        </fieldset>
    </form>
    <h2><a href="/admin/flights">Flights Page</a></h2>
</body>
</html>
