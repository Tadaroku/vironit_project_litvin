<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item ">
                <a class="nav-link" href="${pageContext.request.contextPath}/home">Home</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="${pageContext.request.contextPath}/reservationlist">Reservation List</a>
            </li>
            <sec:authorize access="hasAuthority('ADMIN')">
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.request.contextPath}/admin">Admin page</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.request.contextPath}/admin/usersWithReservations">Users Reservations</a>
                </li>
            </sec:authorize>

            <sec:authorize access="hasAnyAuthority('ADMIN','USER')">
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.request.contextPath}/profile">User page</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.request.contextPath}/profile/edit">Edit Profile</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.request.contextPath}/profile/pass">Change Password</a>
                </li>
            </sec:authorize>
            <sec:authorize access="isAnonymous()">
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.request.contextPath}/signup">SignUp</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.request.contextPath}/login">Login</a>
                </li>
            </sec:authorize>
        </ul>
    </div>
</nav>
