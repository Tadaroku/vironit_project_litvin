<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Home</title>
    <link rel="icon" type="image/x-icon" href="resources/img/favicon.ico">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="resources/css/main.css">
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top padding-null">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" a href="${pageContext.request.contextPath}/aboutus">About Us</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/reservationlist">List Of Reservations</a></li>
                <%--<c:if test="${user==null}">--%>
                    <%--<li><a href="${pageContext.request.contextPath}/signup">SignUp</a></li>--%>
                    <%--<li><a href="${pageContext.request.contextPath}/login">LogIn</a></li>--%>
                <%--</c:if>--%>
                <sec:authorize access="isAnonymous()">
                    <li><a href="${pageContext.request.contextPath}/signup">SignUp</a></li>
                    <li><a href="${pageContext.request.contextPath}/login">LogIn</a></li>
                </sec:authorize>
                <sec:authorize access="hasAnyAuthority('ADMIN','USER')">
                    <li class="nav-item ">
                        <a class="nav-link" href="${pageContext.request.contextPath}/profile">User page</a>
                    </li>
                </sec:authorize>
                <%--<c:if test="${user!=null}">--%>
                    <%--<li class="nav-item ">--%>
                        <%--<a class="nav-link" href="${pageContext.request.contextPath}/profile">User page</a>--%>
                    <%--</li>--%>
                <%--</c:if>--%>
            </ul>
        </div>
    </div>
</div>
<div id="headerwrap">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-8 col-lg-offset-2">
                <h1>LE Aviasales</h1>
                <h2>Best Air Tickets Sale Website</h2>
            </div>
        </div>
    </div>
</div>
<div id="carousel-cars" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#testCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#testCarousel" data-slide-to="1"></li>
        <li data-target="#testCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img src="resources/img/airport.jpg" alt="...">
        </div>
        <div class="item">
            <img src="resources/img/plane1.jpg" alt="...">
        </div>
        <div class="item">
            <img src="resources/img/planes.jpg" alt="...">
        </div>
    </div>

    <a class="left carousel-control" href="#carousel-cars" role="button" data-slide="prev">
        <i class="fas fa-arrow-left glyphicon-chevron-left"></i>
    </a>
    <a class="right carousel-control" href="#carousel-cars" role="button" data-slide="next">
        <i class="fas fa-arrow-right glyphicon-chevron-right"></i>
    </a>
</div>
<%--<div id="dg">--%>
    <%--<div class="container">--%>
        <%--<div class="row centered">--%>
            <%--<h4></h4>--%>
            <%--<div class="col-lg-4">--%>

            <%--</div>--%>
            <%--<div class="col-lg-4">--%>

            <%--</div>--%>
            <%--<div class="col-lg-4">--%>

            <%--</div>--%>
        <%--</div>--%>
    <%--</div>--%>
    <%--<div class="container wb">--%>
        <%--<div class="row centered">--%>
            <%--<h1>CONTACT US</h1>--%>
        <%--</div>--%>
        <%--<div class="col-lg8 col-lg-offset-2">--%>

        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>
<div id="footer">
    <div class="container">
        <div class="row centered">
            <h2>CONTACT US</h2>
            <a href="https://vk.com/yasuoismylove"><i class="fab fa-vk"></i></a>
            <a href="https://www.instagram.com/edwardkun123/?hl=ru"><i class="fab fa-instagram"></i></a>
            <div class="footer-copyright text-center py-3">(c)2019 Copyright: LE Aviasales </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>
