<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>
    <div class="container">
        <div class="row">
            <div class=col-md-1>ID</div>
            <div class=col-md-2>Departure Time</div>
            <div class=col-md-2>Arrival Time</div>
            <div class=col-md-1>Airplane</div>
            <div class=col-md-1>Flight Number</div>
            <div class=col-md-2>Airport From</div>
            <div class=col-md-2>Airport To</div>
        </div>
    </div>

    <div class="container">
        <c:forEach items="${flight}" var="flight">
            <form class="update-user-${flight.id}" action="/admin/flights/delete" method=POST>
                <div class="row">

                    <div class=col-md-1>
                        <input id="id" class="form-control input-md"
                               name="id"
                               value="${flight.id}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="departureTime" class="form-control input-md" name="departureTime"
                               value="${flight.departureTime}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="arrivalTime" class="form-control input-md"
                               name="arrivalTime"
                               value="${flight.arrivalTime}"/>
                    </div>


                    <div class=col-md-1>
                        <input id="airplane" class="form-control input-md" name="airplane"
                               value="${flight.airplane}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="flightNumber" class="form-control input-md" name="flightNumber"
                               value="${flight.flightNumber}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="airportFromName" disabled
                               class="form-control input-md" name="airportFromName"
                               value="${flight.airportFrom.name}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="airportToName" disabled
                               class="form-control input-md" name="airportToName"
                               value="${flight.airportTo.name}"/>
                    </div>

                    <form id="deleteFlights" method="POST" action="/admin/flights/delete">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button id="delete" value="delete" name="delete" class="btn btn-danger col-md-1">
                            Delete
                        </button>
                    </form>
                </div>
            </form>
            <p></p>
        </c:forEach>
    </div>
    <h2><a href="/admin/flights">Flights Page</a></h2>

</div>
</body>
