<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="head.jsp" %>
<body>
<div class="container">
    <%@ include file="menu.jsp" %>
    <div class="container">
        <div class="row">
            <div class=col-md-1>ID</div>
            <div class=col-md-1>Luggage</div>
            <div class=col-md-1>Price</div>
            <div class=col-md-1>Seat</div>
            <div class=col-md-2>Class</div>
            <div class=col-md-2>Reservation Date</div>
            <div class=col-md-1>Status</div>
            <div class=col-md-1>Flight Id</div>
            <div class=col-md-1>Airline</div>
        </div>
    </div>

    <div class="container">
        <c:forEach items="${reservation}" var="reservation">
            <form class="update-user-${reservation.id}" action="/admin/reservations/delete" method=POST>
                <div class="row">
                    <div class=col-md-1>
                        <input id="id" class="form-control input-md"
                               name="id"
                               value="${reservation.id}"/>
                    </div>
                    <div class=col-md-1>
                        <input id="luggage" class="form-control input-md" name="luggage"
                               value="${reservation.luggage}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="price" class="form-control input-md"
                               name="price"
                               value="${reservation.price}"/>
                    </div>


                    <div class=col-md-1>
                        <input id="seatNumber" class="form-control input-md" name="seatNumber"
                               value="${reservation.seatNumber}"/>
                    </div>


                    <div class=col-md-2>
                        <input id="ticketClass" class="form-control input-md" name="ticketClass"
                               value="${reservation.ticketClass}"/>
                    </div>

                    <div class=col-md-2>
                        <input id="reservationDate" class="form-control input-md" name="reservationDate"
                               value="${reservation.reservationDate}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="reservationStatus"
                               class="form-control input-md" name="reservationStatus"
                               value="${reservation.reservationStatus}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="flightId" disabled
                               class="form-control input-md" name="flightId"
                               value="${reservation.flight.id}"/>
                    </div>

                    <div class=col-md-1>
                        <input id="airlineName" disabled
                               class="form-control input-md" name="airlineName"
                               value="${reservation.airline.name}"/>
                    </div>

                    <form id="deleteReservations" method="POST" action="/admin/reservations/delete">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button id="delete" value="delete" name="delete" class="btn btn-danger col-md-1">
                            Delete
                        </button>
                    </form>
                </div>
            </form>
            <p></p>
        </c:forEach>
    </div>
    <h2><a href="/admin/reservations">Reservation Page</a></h2>

</div>
</body>
