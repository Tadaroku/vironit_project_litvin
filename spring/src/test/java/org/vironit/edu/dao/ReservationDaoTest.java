package org.vironit.edu.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.vironit.edu.config.*;
import org.vironit.edu.entity.*;
import org.vironit.edu.entity.enums.ReservationStatus;
import org.vironit.edu.entity.enums.TicketClass;
import org.vironit.edu.service.AirlineService;
import org.vironit.edu.service.FlightService;
import org.vironit.edu.service.ReservationService;
import org.vironit.edu.service.UserService;

import java.text.ParseException;
import java.time.LocalDate;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, WebConfig.class, WebInitializer.class, SecurityConfig.class, SpringSecurityInitializer.class})
public class ReservationDaoTest {

    @Autowired
    ReservationService reservationService;

    @Autowired
    AirlineService airlineService;

    @Autowired
    UserService userService;

    @Autowired
    FlightService flightService;

        @Test
    public void save() throws ParseException {
        Reservation reservation = new Reservation();
        reservation.setSeatNumber(21);
        reservation.setLuggage(true);
        reservation.setPrice(222.0);
        reservation.setReservationDate(LocalDate.now());
        reservation.setReservationStatus(ReservationStatus.FREE);
        reservation.setTicketClass(TicketClass.ECONOM);

        User user = userService.getById(2L);
        reservation.setUser(user);

        Airline airline = airlineService.getById(2);
        reservation.setAirline(airline);

        Flight flight = flightService.getById(2);
        reservation.setFlight(flight);


        reservationService.save(reservation);
        assertNotNull(reservation);
    }

//        @Test
//    public void getById() {
//        assertNotNull(reservationService.getById(2));
//    }

    @Test
    public void getAll() {
        assertNotNull(reservationService.getAll());
    }

//        @Test
    public void update() {
        Reservation reservation = reservationService.getById(3);
        reservation.setSeatNumber(22);
        reservationService.update(reservation);
    }

//        @Test
    public void delete() {
        reservationService.delete(2);
    }
}

