package org.vironit.edu.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.vironit.edu.config.*;
import org.vironit.edu.entity.Airline;
import org.vironit.edu.service.AirlineService;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, WebConfig.class, WebInitializer.class, SecurityConfig.class, SpringSecurityInitializer.class})
public class AirlineDaoTest {

    @Autowired
    AirlineService airlineService;

//        @Test
    public void save() {
        Airline airline = new Airline();
        airline.setName("Belavia 2");
        airlineService.save(airline);
        assertNotNull(airline);
    }

    @Test
    public void getById() {
        assertNotNull(airlineService.getById(2));
    }

    @Test
    public void getAll() {
        assertNotNull(airlineService.getAll());
    }

    //    @Test
    public void update() {
        Airline airline = airlineService.getById(2);
        airline.setName("Belavia");
        airlineService.update(airline);
    }

    //    @Test
    public void delete() {
        airlineService.delete(2);
    }

}
