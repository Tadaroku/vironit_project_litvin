package org.vironit.edu.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.vironit.edu.config.*;
import org.vironit.edu.entity.Airport;
import org.vironit.edu.service.AirportService;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, WebConfig.class, WebInitializer.class, SecurityConfig.class, SpringSecurityInitializer.class})
public class AirportDaoTest {

    @Autowired
    AirportService airportService;

//        @Test
    public void save() {
        Airport airport = new Airport();
        airport.setName("Minsk");
        airport.setCode("AAA");
        airport.setLocation("Belarus");
        airportService.save(airport);
        assertNotNull(airport);
    }

    //    @Test
    public void getById() {
        assertNotNull(airportService.getById(2));
    }

    @Test
    public void getAll() {
        assertNotNull(airportService.getAll());
    }

            @Test
    public void update() {
        Airport airport = airportService.getById(2);
        airport.setName("NameUpdate");
        airportService.update(airport);
    }

    //        @Test
    public void delete() {
        airportService.delete(2);
    }

}

