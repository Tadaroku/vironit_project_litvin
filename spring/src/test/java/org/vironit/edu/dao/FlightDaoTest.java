package org.vironit.edu.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.vironit.edu.config.*;
import org.vironit.edu.entity.Airport;
import org.vironit.edu.entity.Flight;
import org.vironit.edu.service.AirportService;
import org.vironit.edu.service.FlightService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, WebConfig.class, WebInitializer.class, SecurityConfig.class, SpringSecurityInitializer.class})
public class FlightDaoTest {

    @Autowired
    FlightService flightService;

    @Autowired
    AirportService airportService;

//    @Test
    public void save() throws ParseException {
        Flight flight = new Flight();
        String pattern = "yyyy-MM-dd HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date1 = simpleDateFormat.parse("2018-09-09 14:30");
        Date date2 = simpleDateFormat.parse("2018-09-09 16:30");
        flight.setDepartureTime(date1);
        flight.setArrivalTime(date2);
        flight.setFlightNumber("220");
        flight.setAirplane("Boing");

        Airport airportFrom = airportService.getById(2);
        Airport airportTo = airportService.getById(3);

        flight.setAirportFrom(airportFrom);
        flight.setAirportTo(airportTo);


         flightService.save(flight);
        assertNotNull(flight);
    }

//    @Test
    public void getById() {
        assertNotNull(flightService.getById(2));
    }

    @Test
    public void getAll() {
        assertNotNull(flightService.getAll());
    }

//    @Test
    public void update() {
        Flight flight = flightService.getById(2);
        flight.setAirplane("Update2");
        flightService.update(flight);
    }

//    @Test
    public void delete() {
        flightService.delete(2);
    }
}
