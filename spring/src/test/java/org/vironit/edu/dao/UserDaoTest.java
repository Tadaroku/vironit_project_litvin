package org.vironit.edu.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.vironit.edu.config.*;
import org.vironit.edu.entity.User;
import org.vironit.edu.entity.enums.RoleName;
import org.vironit.edu.service.UserService;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, SecurityConfig.class, WebConfig.class, WebInitializer.class, SpringSecurityInitializer.class})
public class UserDaoTest {

    @Autowired
    UserService userService;

    @Autowired
    BCryptPasswordEncoder encoder;

//        @Test
    public void saveTest() {
        User user = new User();
        user.setName("Edward");
        user.setSurname("Litvin");
        user.setPassword(encoder.encode("zaxscd123a"));
        user.setPhone("+375 (29) 100-00-00");
        user.setEmail("edwa1@mail.ru");
        user.setRole(RoleName.USER);
        userService.save(user);
        assertNotNull(user);
    }

    @Test
    public void getByEmail() {
        assertNotNull(userService.findByEmail("edward@mail.com"));
    }

//    @Test
    public void getById() {
        assertNotNull(userService.getById(4L));
    }

    @Test
    public void getAll() {
        assertNotNull(userService.getAll());
    }

    //    @Test
    public void update() {
        User user = userService.getById(7L);
        user.setName("Edwardd");
        userService.update(user);
    }

    //    @Test
    public void delete() {
        userService.delete(7L);
    }


}
