package org.vironit.edu.fapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.fapi.model.Airline;
import org.vironit.edu.fapi.service.AirlineService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/airlines")
public class AirlineController {

    private final AirlineService airlineService;


    @Autowired
    public AirlineController(AirlineService airlineService) {
        this.airlineService = airlineService;
    }

    @GetMapping
    public List<Airline> getAllAirlines(@RequestParam(value = "page") int page) {
        return airlineService.getAllAirlines(page);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<Airline> getById(@PathVariable Integer id) {
        return airlineService.getAirlineById(id);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        airlineService.deleteAirline(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateAirline(@PathVariable Integer id, @RequestBody Airline airline) {
        return new ResponseEntity<>(airlineService.saveAirline(airline), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Airline saveAirline(@RequestBody Airline airline) {
        return airlineService.saveAirline(airline);
    }
}
