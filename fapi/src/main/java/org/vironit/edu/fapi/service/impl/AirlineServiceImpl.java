package org.vironit.edu.fapi.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.vironit.edu.fapi.model.Airline;
import org.vironit.edu.fapi.service.AirlineService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class AirlineServiceImpl implements AirlineService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public Optional<Airline> getAirlineById(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        Airline airline = restTemplate.getForObject(backendServerUrl + "/api/v1/airlines/" + id, Airline.class);
        return Optional.ofNullable(airline);
    }

    @Override
    public List<Airline> getAllAirlines(int page) {
        RestTemplate restTemplate = new RestTemplate();
        Airline[] airlines = restTemplate.getForObject(backendServerUrl + "/api/v1/airlines?page=" + page, Airline[].class);
        return airlines == null ? Collections.emptyList() : Arrays.asList(airlines);
    }

    @Override
    public Airline saveAirline(Airline airline) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl + "/api/v1/airlines", airline, Airline.class).getBody();
    }

    @Override
    public Airline deleteAirline(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendServerUrl + "/api/v1/airlines/" + id);
        return null;
    }

    @Override
    public Optional<Airline> findByName(String name) {
        RestTemplate restTemplate = new RestTemplate();
        Airline airline = restTemplate.getForObject(backendServerUrl + "/api/v1/airlines/name/" + name, Airline.class);
        return Optional.ofNullable(airline);
    }
}
