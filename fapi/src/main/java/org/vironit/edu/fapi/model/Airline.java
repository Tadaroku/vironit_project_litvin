package org.vironit.edu.fapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Airline {

    private int id;
    private String name;
    private String reservations;

    private String airports;

    public Airline() {
    }

    public Airline(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Airline(int id, String name, String reservations, String airports) {
        this.id = id;
        this.name = name;
        this.reservations = reservations;
        this.airports = airports;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservations() {
        return reservations;
    }

    public void setReservations(String reservations) {
        this.reservations = reservations;
    }

    public String getAirports() {
        return airports;
    }

    public void setAirports(String airport) {
        this.airports = airport;
    }
}
