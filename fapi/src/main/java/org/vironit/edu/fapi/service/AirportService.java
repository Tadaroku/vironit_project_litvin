package org.vironit.edu.fapi.service;

import org.vironit.edu.fapi.model.Airport;

import java.util.List;
import java.util.Optional;

public interface AirportService {

    Optional<Airport> getAirportById(Integer id);

    Airport saveAirport(Airport airport);

    Airport deleteAirport(Integer id);

    Optional<Airport> findByName(String name);

    List<Airport> getAllAirports(int page);
}
