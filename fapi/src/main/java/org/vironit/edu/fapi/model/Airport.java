package org.vironit.edu.fapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Airport {
    private int id;
    private String name;
    private String location;
    private String code;
    private String airlines;


    public Airport() {
    }

    public Airport(int id, String name, String location, String code) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.code = code;
    }

    public Airport(int id, String name, String location, String code, String airlines) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.code = code;
        this.airlines = airlines;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlines) {
        this.airlines = airlines;
    }
}
