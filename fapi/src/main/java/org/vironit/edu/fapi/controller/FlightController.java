package org.vironit.edu.fapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.fapi.model.Flight;
import org.vironit.edu.fapi.service.FlightService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/flights")
public class FlightController {

    private final FlightService flightService;


    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping
    public List<Flight> getAllFlights(@RequestParam(value = "page") int page) {
        return flightService.getAllFlights(page);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<Flight> getById(@PathVariable Integer id) {
        return flightService.getFlightById(id);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        flightService.deleteFlight(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateFlight(@PathVariable Integer id, @RequestBody Flight flight) {
        return new ResponseEntity<>(flightService.saveFlight(flight), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Flight saveFlight(@RequestBody Flight flight) {
        return flightService.saveFlight(flight);
    }
}
