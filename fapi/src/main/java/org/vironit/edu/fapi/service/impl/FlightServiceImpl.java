package org.vironit.edu.fapi.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.vironit.edu.fapi.model.Flight;
import org.vironit.edu.fapi.service.FlightService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class FlightServiceImpl implements FlightService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public Optional<Flight> getFlightById(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        Flight flight = restTemplate.getForObject(backendServerUrl + "/api/v1/flights/" + id, Flight.class);
        return Optional.ofNullable(flight);
    }

    @Override
    public List<Flight> getAllFlights(int page) {
        RestTemplate restTemplate = new RestTemplate();
        Flight[] flights = restTemplate.getForObject(backendServerUrl + "/api/v1/flights?page=" + page, Flight[].class);
        return flights == null ? Collections.emptyList() : Arrays.asList(flights);
    }

    @Override
    public Flight saveFlight(Flight flight) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl + "/api/v1/flights", flight, Flight.class).getBody();
    }

    @Override
    public Flight deleteFlight(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendServerUrl + "/api/v1/flights/" + id);
        return null;
    }
}
