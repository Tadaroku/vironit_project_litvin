package org.vironit.edu.fapi.service;

import org.vironit.edu.fapi.model.Reservation;

import java.util.List;
import java.util.Optional;

public interface ReservationService {

    Optional<Reservation> getReservationById(Integer id);

    List<Reservation> getAllReservations(int page);

    Reservation saveReservation(Reservation reservation);

    Reservation deleteReservation(Integer id);
}
