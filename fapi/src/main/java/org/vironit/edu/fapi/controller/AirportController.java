package org.vironit.edu.fapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.fapi.model.Airport;
import org.vironit.edu.fapi.service.AirportService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/airports")
public class AirportController {

    private final AirportService airportService;


    @Autowired
    public AirportController(AirportService airportService) {
        this.airportService = airportService;
    }

    @GetMapping
    public List<Airport> getAllAirports(@RequestParam(value = "page") int page) {
        return airportService.getAllAirports(page);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<Airport> getById(@PathVariable Integer id) {
        return airportService.getAirportById(id);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        airportService.deleteAirport(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateAirport(@PathVariable Integer id, @RequestBody Airport airport) {
        return new ResponseEntity<>(airportService.saveAirport(airport), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Airport saveAirport(@RequestBody Airport airport) {
        return airportService.saveAirport(airport);
    }
}
