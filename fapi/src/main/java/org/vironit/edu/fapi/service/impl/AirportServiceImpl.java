package org.vironit.edu.fapi.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.vironit.edu.fapi.model.Airport;
import org.vironit.edu.fapi.service.AirportService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class AirportServiceImpl implements AirportService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public Optional<Airport> getAirportById(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        Airport airport = restTemplate.getForObject(backendServerUrl + "/api/v1/airports/" + id, Airport.class);
        return Optional.ofNullable(airport);
    }

    @Override
    public List<Airport> getAllAirports(int page) {
        RestTemplate restTemplate = new RestTemplate();
        Airport[] airports = restTemplate.getForObject(backendServerUrl + "/api/v1/airports?page=" + page, Airport[].class);
        return airports == null ? Collections.emptyList() : Arrays.asList(airports);
    }

    @Override
    public Airport saveAirport(Airport airport) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl + "/api/v1/airports", airport, Airport.class).getBody();
    }

    @Override
    public Airport deleteAirport(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendServerUrl + "/api/v1/airports/" + id);
        return null;
    }

    @Override
    public Optional<Airport> findByName(String name) {
        RestTemplate restTemplate = new RestTemplate();
        Airport airport = restTemplate.getForObject(backendServerUrl + "/api/v1/users/name/" + name, Airport.class);
        return Optional.ofNullable(airport);
    }
}
