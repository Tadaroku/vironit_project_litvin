package org.vironit.edu.fapi.controller;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.vironit.edu.fapi.model.AuthToken;
import org.vironit.edu.fapi.model.LoginUser;
import org.vironit.edu.fapi.security.TokenProvider;

@SuppressWarnings("Duplicates")
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;

    @Qualifier("customUserDetailsService")
    @Autowired
    private UserDetailsService userDetailsService;


    @PostMapping(value = "/generate")
    public ResponseEntity<?> signin(@RequestBody LoginUser loginUser) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        try {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            final String token = jwtTokenUtil.generateToken(authentication);
            final String refreshToken = jwtTokenUtil.generateRefreshToken(authentication);
            return ResponseEntity.ok(new AuthToken(token, refreshToken));
        } catch (ExpiredJwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping(value = "/refresh")
    public ResponseEntity<?> refresh(@RequestBody String refreshToken) {

        String username = jwtTokenUtil.getUsernameFromToken(refreshToken);
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (jwtTokenUtil.validateToken(refreshToken, userDetails)) {
            final Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());

            SecurityContextHolder.getContext().setAuthentication(authentication);
            final String token = jwtTokenUtil.generateToken(authentication);
            final String newRefreshToken = jwtTokenUtil.generateRefreshToken(authentication);
            return ResponseEntity.ok(new AuthToken(token, newRefreshToken));
        }
        return null;


    }

}

