package org.vironit.edu.fapi.service;

import org.vironit.edu.fapi.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> findByEmail(String email);

    Optional<User> findByUsername(String username);

    List<User> findAll(int page);

    User save(User user);

    User deleteUser(Long id);

    Optional<User> findByPhoneNumber(String phoneNumber);

    Optional<User> getUserById(Long id);
}
