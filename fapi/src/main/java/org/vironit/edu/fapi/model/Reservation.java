package org.vironit.edu.fapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Reservation {

    private int id;

    private String reservationDate;

    private boolean luggage = true;

    private String price;

    private String ticketClass;

    private String seatNumber;

    private String reservationStatus;

    private String user;

    private String flight;

    private String airline;

    public Reservation() {
    }

    public Reservation(int id, String reservationDate, boolean luggage, String price, String ticketClass, String seatNumber, String reservationStatus) {
        this.id = id;
        this.reservationDate = reservationDate;
        this.luggage = luggage;
        this.price = price;
        this.ticketClass = ticketClass;
        this.seatNumber = seatNumber;
        this.reservationStatus = reservationStatus;
    }

    public Reservation(int id, String reservationDate, boolean luggage, String price, String ticketClass, String seatNumber, String reservationStatus, String user, String flight, String airline) {
        this.id = id;
        this.reservationDate = reservationDate;
        this.luggage = luggage;
        this.price = price;
        this.ticketClass = ticketClass;
        this.seatNumber = seatNumber;
        this.reservationStatus = reservationStatus;
        this.user = user;
        this.flight = flight;
        this.airline = airline;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTicketClass() {
        return ticketClass;
    }

    public void setTicketClass(String ticketClass) {
        this.ticketClass = ticketClass;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(String reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }
}
