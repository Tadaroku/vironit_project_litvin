package org.vironit.edu.fapi.service;

import org.vironit.edu.fapi.model.Flight;

import java.util.List;
import java.util.Optional;

public interface FlightService {

    Optional<Flight> getFlightById(Integer id);

    Flight saveFlight(Flight flight);

    Flight deleteFlight(Integer id);

    List<Flight> getAllFlights(int page);
}
