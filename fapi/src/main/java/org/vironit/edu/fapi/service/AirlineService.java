package org.vironit.edu.fapi.service;

import org.vironit.edu.fapi.model.Airline;

import java.util.List;
import java.util.Optional;

public interface AirlineService {

    Optional<Airline> getAirlineById(Integer id);

    List<Airline> getAllAirlines(int page);

    Airline saveAirline(Airline airline);

    Airline deleteAirline(Integer id);

    Optional<Airline> findByName(String name);
}
