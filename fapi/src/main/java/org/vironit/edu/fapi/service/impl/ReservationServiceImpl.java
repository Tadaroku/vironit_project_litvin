package org.vironit.edu.fapi.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.vironit.edu.fapi.model.Reservation;
import org.vironit.edu.fapi.service.ReservationService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public Optional<Reservation> getReservationById(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        Reservation reservation = restTemplate.getForObject(backendServerUrl + "/api/v1/reservations/" + id, Reservation.class);
        return Optional.ofNullable(reservation);
    }

    @Override
    public List<Reservation> getAllReservations(int page) {
        RestTemplate restTemplate = new RestTemplate();
        Reservation[] reservations = restTemplate.getForObject(backendServerUrl + "/api/v1/reservations?page=" + page, Reservation[].class);
        return reservations == null ? Collections.emptyList() : Arrays.asList(reservations);
    }

    @Override
    public Reservation saveReservation(Reservation reservation) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl + "/api/v1/reservations", reservation, Reservation.class).getBody();
    }

    @Override
    public Reservation deleteReservation(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendServerUrl + "/api/v1/reservations/" + id);
        return null;
    }
}
